describe("dependencies", function() {
    it("must be WebSockets supported", function() {
        expect(typeof(window.WebSocket)).toBe('function');
    });
    it("must be Promises supported", function(done) {
        expect(typeof(window.Promise)).toBe('function');
        try {
            var p1 = function(value) { return new Promise(
                    function(resolve, reject) {
                            resolve(value);
                        }
                     );
                 };
            p1(42)
                .then(function(result) { expect(result).toBe(42); done();})
                .catch(function(error) { console.log(error); done.fail(error) });
        } catch(e) {console.log(e); done.fail(); }
     });
    it("must be the sockjs library available", function() {
        expect(typeof(window.SockJS)).toBe('function');
     });
    it("must be the vertx.x event bus library available", function() {
        expect(typeof(window.EventBus)).toBe('function');
    });
    it("must be the modified jsonPath available", function() {
        expect(typeof(window.JSONPath)).toBe('function');
    });
    it("dataBusSettings (global variable) must be defined and contain UPPT Location and Solution/Router EventBus Address as well as logging setting", function() {
        expect(typeof(window.dataBusSettings)).toBe('object');
        expect(typeof(window.dataBusSettings.location)).toBe('string');
        expect(typeof(window.dataBusSettings.routerDomain)).toBe('string');
        expect(window.dataBusSettings.routerDomain.length).toBeGreaterThan(4);
        expect(typeof(window.dataBusSettings.routerVersion)).toBe('string');
        expect(window.dataBusSettings.routerVersion.length).toBeGreaterThan(0);
        expect(typeof(window.dataBusSettings.routerType)).toBe('string');
        expect(window.dataBusSettings.routerDomain.length).toBeGreaterThan(2);
    });
});
describe("testParameters", function() {
console.log("PARAMS: " + JSON.stringify(window.testParameters));
    it("should be test parameters available", function() {
        expect(typeof(window.testParameters)).toBe('object');
        expect(window.testParameters).not.toBe(null);
    });
    it("should contain parameters for testing login via email", function() {
        expect(typeof(window.testParameters.email)).toBe('string');
        expect(window.testParameters.email).not.toBe(null);
        expect(window.testParameters.email).toContain("@");
        expect(typeof(window.testParameters.password)).toBe('string');
        expect(window.testParameters.password).not.toBe(null);
    });
});
describe("DataBus - local operations", function() {
    it("should be loaded as global window variable", function() {
        expect(typeof(window.DataBus)).toBe('object');
    });
    it("enables setting local values and getting a confirmation", function(done) {
        expect(typeof(window.DataBus.setLocal)).toBe('function');

        window.DataBus.setLocal("test", "1", "path.to.data", "dummyValue", "uppt-databus-spec.setLocal")
            .then(function(result) {
                expect(result.domain).toBe("test");
                expect(result.version).toBe("1");
                expect(result.operation).toBe("setLocal");
                expect(result.paths[0]).toBe("path.to.data");
                expect(result.values[0]).toBe("dummyValue");
                expect(typeof(result.correlationID)).toBe("string");
                expect(result.origins[0]).toBe("uppt-databus-spec.setLocal");
                done();
            })
            .catch(done.fail);
    });

    it("changes 'set' operations to 'setLocal' if it is not connected/logged in and indicates it by the returned operation value", function(done) {
        window.DataBus.set("test", "1", "path.to.data", "dummyValue", "uppt-databus-spec.setDisconnected")
            .then(function(result) {
                expect(result.domain).toBe("test");
                expect(result.version).toBe("1");
                expect(result.operation).toBe("setLocal");
                expect(result.paths[0]).toBe("path.to.data");
                expect(result.values[0]).toBe("dummyValue");
                expect(typeof(result.correlationID)).toBe("string");
                expect(result.origins[0]).toBe("uppt-databus-spec.setDisconnected");
                done();
            })
            .catch(done.fail);
    });

    it("changes 'get' operations to 'getLocal' if it is not connected/logged in and indicates it by the returned operation value", function(done) {
            window.DataBus.get("test", "1", "$.path.to.data", "uppt-databus-spec.getDisconnected")
                .then(function(result) {
                    expect(result.domain).toBe("test");
                    expect(result.version).toBe("1");
                    expect(result.operation).toBe("getLocal");
                    expect(result.paths[0]).toBe("path.to.data");
                    expect(result.values[0]).toBe("dummyValue");
                    expect(typeof(result.correlationID)).toBe("string");
                    expect(result.origins[0]).toBe("uppt-databus-spec.setDisconnected");
                    done();
                })
                .catch(done.fail);
        });


    it("enables setting local values and retrieving them via a JSONPath (http://goessner.net/articles/JsonPath/)", function(done) {
        expect(typeof(window.DataBus.getLocal)).toBe('function');

        window.DataBus.setLocal("test", "1", "simple.path.to.data", "dummyValue", "uppt-databus-spec.setLocal");
        window.DataBus.getLocal("test", "1", "$.simple.path.to.data", "uppt-databus-spec.jspnPath")
            .then(function(result) {
                expect(result.domain).toBe("test");
                expect(result.version).toBe("1");
                expect(result.operation).toBe("getLocal");
                expect(result.jsonPath).toBe("$.simple.path.to.data");
                // watch out: path and values are in an array!
                expect(result.paths[0]).toBe("simple.path.to.data");
                expect(result.values[0]).toBe("dummyValue");
                expect(result.origins[0]).toBe("uppt-databus-spec.setLocal");
            })
            .catch(done.fail);

        window.DataBus.setLocal("test", "1", "less[\"simple\"].path['to'].array[2].thing", ["dummyValue2"], "uppt-databus-spec.setDummyValue2");
        window.DataBus.getLocal("test", "1", "$..simple.path.to.array[2].thing", "uppt-databus-spec")
            .then(function(result) {
                expect(result.domain).toBe("test");
                expect(result.version).toBe("1");
                expect(result.operation).toBe("getLocal");
                expect(result.jsonPath).toBe("$..simple.path.to.array[2].thing");
                // watch out: path and values are in an array!
                expect(result.paths[0]).toBe("less.simple.path.to.array[2].thing");
                expect(result.values[0][0]).toBe("dummyValue2");
                // TODO take care of origins despite of writing notation
                //expect(result.origins[0]).toBe("uppt-databus-spec.setDummyValue2");
                done();
            })
            .catch(done.fail);
    });
    it("enables getting CURRENT local values via absolute path subscription", function(done) {
        expect(typeof(window.DataBus.subscribe)).toBe('function');

        window.DataBus.setLocal("test", "1", "path.to.data", "dummyValue", "uppt-databus-spec.absolute.initial");
        window.DataBus.subscribe("test", "1", "path.to.data", true, function(result, subscription) {
            expect(result.domain).toBe("test");
            expect(result.version).toBe("1");
            expect(result.operation).toBe("getLocal");
            expect(result.paths[0]).toBe("path.to.data");
            expect(result.values[0]).toBe("dummyValue");
            expect(result.origins[0]).toBe("uppt-databus-spec.absolute.initial");
            expect(subscription.unsubscribe()).toBe(true);
            done();
        }, done.fail);
    });
    it("enables getting local value UPDATES via absolute path subscription", function(done) {
        var counter = 0;
        window.DataBus.subscribe("test", "1", "path.to.data", true, function(result, subscription) {
            counter = counter + 1;
            // first result is the initial value, so change it
            if(counter === 1) {
                window.DataBus.setLocal("test", "1", "path.to.data", "newDummyValue", "uppt-databus-spec.absolute.update");
            }
            else if(counter === 2) {
                expect(result.domain).toBe("test");
                expect(result.version).toBe("1");
                expect(result.operation).toBe("setLocal");
                expect(result.paths[0]).toBe("path.to.data");
                expect(result.values[0]).toBe("newDummyValue");
                expect(result.origins[0]).toBe("uppt-databus-spec.absolute.update");
                expect(typeof(result.correlationID)).toBe("string");
                expect(subscription.unsubscribe()).toBe(true);
                done();
            }
        }, done.fail);
    });
    it("enables getting CURRENT local values via path prefix subscription (path selector ends with a '.*')", function(done) {
        window.DataBus.setLocal("test", "1", "path.to.data", "dummyValue", "uppt-databus-spec.prefix");
        window.DataBus.subscribe("test", "1", "path.to.*", true, function(result, subscription) {
            expect(result.domain).toBe("test");
            expect(result.version).toBe("1");
            expect(result.operation).toBe("getLocal");
            expect(result.jsonPath).toBe("path.to.*");
            expect(result.paths[0]).toBe("path.to.data");
            expect(result.values[0]).toBe("dummyValue");
            expect(result.origins[0]).toBe("uppt-databus-spec.prefix");
            expect(subscription.unsubscribe()).toBe(true);

            done();
        }, function(error) {console.log(error);});
    });
    it("enables getting local value UPDATES via absolute path prefix subscription (path selector ends with a '.*')", function(done) {
        var counter = 0;
        window.DataBus.subscribe("test", "1", "path.to.*", true, function(result, subscription) {
            counter = counter + 1;
            // first result is the initial value, so change it
            if(counter === 1) {
                window.DataBus.setLocal("test", "1", "path.to.data", "newestDummyValue", "uppt-databus-spec.prefix.update");
            }
            else if(counter === 2) {
                expect(result.domain).toBe("test");
                expect(result.version).toBe("1");
                expect(result.operation).toBe("setLocal");
                expect(result.paths[0]).toBe("path.to.data");
                expect(result.values[0]).toBe("newestDummyValue");
                expect(result.origins[0]).toBe("uppt-databus-spec.prefix.update");
                expect(subscription.unsubscribe()).toBe(true);
                done();
            }
        }, done.fail);
    });
    it("enables getting local values UPDATES via regular expression subscription (DOES NOT support returning current value yet)", function(done) {
        var counter = 0;
        window.DataBus.subscribe("test", "1", /^path\.to/i, function(result, subscription) {
            expect(result.domain).toBe("test");
            expect(result.version).toBe("1");
            expect(result.operation).toBe("setLocal");
            expect(result.paths[0]).toBe("path.to.data");
            expect(result.values[0]).toBe("dummyValue" + counter);
            expect(result.origins[0]).toBe("uppt-databus-spec.pattern");
            expect(typeof(result.correlationID)).toBe("string");
            counter = counter + 1;
            if(counter === 10) {
                expect(subscription.unsubscribe()).toBe(true);
                done();
            }
        }, done.fail);

        for(var i=0;i<10;i++) {
            window.DataBus.setLocal("test", "1", "path.to.data", "dummyValue"+i, "uppt-databus-spec.pattern");
        }
    });

    it("enables deleting local values (that is TO SET THEM TO NULL - not 'delete'/'dereference'!)", function(done) {
        expect(typeof(window.DataBus.delLocal)).toBe('function');

        window.DataBus.delLocal("test", "1", "path.to.data", "uppt-databus-spec.delLocal")
            .then(function(result) {
                expect(result.domain).toBe("test");
                expect(result.version).toBe("1");
                expect(result.operation).toBe("delLocal");
                expect(result.paths[0]).toBe("path.to.data");
                expect(result.values[0]).toBe(null);
                expect(typeof(result.correlationID)).toBe("string");
                expect(result.origins[0]).toBe("uppt-databus-spec.delLocal");
                done();
            })
            .catch(done.fail);
    });

    it("changes 'del' operations to 'delLocal' if it is not connected/logged in and indicates it by the returned operation value", function(done) {
        window.DataBus.del("test", "1", "path.to.data", "uppt-databus-spec.delLocal")
            .then(function(result) {
                expect(result.domain).toBe("test");
                expect(result.version).toBe("1");
                expect(result.operation).toBe("delLocal");
                expect(result.paths[0]).toBe("path.to.data");
                expect(result.values[0]).toBe(null);
                expect(typeof(result.correlationID)).toBe("string");
                expect(result.origins[0]).toBe("uppt-databus-spec.delLocal");
                done();
            })
            .catch(done.fail);
    });

    it("enables accessing the settings from window.dataBusSettings via the DataBus API", function(done) {
        window.DataBus.getLocal("config", "1", "$.routerDomain", "uppt-databus-spec.config")
            .then(function(result) {
                expect(result.domain).toBe("config");
                expect(result.version).toBe("1");
                expect(result.operation).toBe("getLocal");
                expect(result.jsonPath).toBe("$.routerDomain");
                // watch out: path and values are in an array!
                expect(result.paths[0]).toBe("routerDomain");
                expect(result.values[0]).toBe("in.test");
                //expect(result.origins[0]).toBe("uppt-databus-spec.config");
                done();
            })
            .catch(done.fail);
    });
});
describe("DataBus - anonymous session", function() {
    it("should establish an anonymous session automatically", function(done) {
        window.DataBus.subscribe("session", "1", "status", true, function(result, subscription) {
            if(result.values[0] === "anonymous") {
                expect(result.domain).toBe("session");
                expect(result.version).toBe("1");
                expect(result.paths[0]).toBe("status");
                expect(result.operation).toBe("get");
                done();
            }
        }, done.fail);
    });
    it("should contain a method for preflighting a login via email", function() {
        expect(typeof(window.DataBus.preflightLoginViaEmail)).not.toBe('undefined');
    });
    it("should fail to preflight an unknown email", function(done) {
        window.DataBus.preflightLoginViaEmail("fail" + window.testParameters.email)
            .then(done.fail)
            .catch(function(error) {
                expect(error.startsWith("404")).toBe(true);
                done();
            });
    });
    it("should succeed to preflight a known email", function(done) {
        window.DataBus.preflightLoginViaEmail(window.testParameters.email)
            .then(function(result) {
                    expect(result).toBe(window.testParameters.email);
                    done();
                })
            .catch(done.fail);
    });

});




/*
describe("DataBus - sessions", function() {
jasmine.DEFAULT_TIMEOUT_INTERVAL = 50000;
    it("should contain a method for connecting", function() {
        expect(typeof(window.DataBus.connect)).not.toBe('undefined');
    });
    it("should be able to connect", function(done) {
        window.DataBus.connect(window.testParameters.email, window.testParameters.password, null, true)
            .then(function(sessionInfo) {
                expect(sessionInfo.status).toBe("authenticated");
                expect(sessionInfo.user.id).toBe(window.testParameters.email);
                expect(typeof(sessionInfo.user.roles)).toBe("object");
                done();
                })
            .catch(done.fail);
    });
    it("should have set the connection status and the session information in the local data structure", function(done) {
        window.DataBus.getLocal("session", "1", "$.", "uppt-databus-spec")
            .then(function(result) {
               expect(result.values[0].status).toBe("authenticated");
               expect(result.values[0].user.id).toBe(window.testParameters.email);
               expect(typeof(result.values[0].user.roles)).toBe("object");
               expect(typeof(result.values[0].addressDomain)).toBe("string");
               expect(typeof(result.values[0].addressVersion)).toBe("string");
               expect(typeof(result.values[0].addressType)).toBe("string");
               expect(typeof(result.values[0].source)).toBe("string");
               expect(typeof(result.values[0].traits)).toBe("object");
               expect(result.values[0].isSaveSessionTokenAllowed).toBe(true);
               done();
            })
            .catch(done.fail);
    });
    it("should have saved the session token", function() {
        expect(localStorage.getItem('sessionToken')).not.toBe('undefined');
    });
    it("should have set traits as defined by the solution service", function(done) {
        window.DataBus.getLocal("session", 1, "traits", "uppt-databus-spec")
            .then(function(result) {
                expect(typeof(result.values[0])).toBe("object");
                expect(typeof(result.values[0].someTestTrait)).toBe("boolean");
                expect(result.values[0].someTestTrait).toBe(true);
                expect(typeof(result.values[0].someTestTraitWhichShouldNotMatch)).toBe("undefined");
                done();
            })
            .catch(done.fail);
    });
    it("should be able to logout", function(done) {
        window.DataBus.subscribe("session", "1", "status", function(result, subscription) {
            if(result.values[0] === "disconnected") {
                expect(subscription.unsubscribe()).toBe(true);
                window.DataBus.getLocal("session", "1", "$.status", "uppt-databus-spec")
                    .then(function(result) {
                        expect(result.values[0]).toBe("disconnected");
                        done();
                    })
                    .catch(done.fail);
            }
        });
        window.DataBus.setLocal("session", "1", "status", "disconnecting", "uppt-databus-spec");
    });
    it("should have deleted the session token", function() {
        expect(localStorage.getItem('sessionToken')).toBe(null);
    });
    it("should be able to connect again", function(done) {
            window.DataBus.connect(window.testParameters.email, window.testParameters.password, null, true)
                .then(function(sessionInfo) {
                    expect(sessionInfo.status).toBe("authenticated");
                    expect(sessionInfo.user.id).toBe(window.testParameters.email);
                    expect(typeof(sessionInfo.user.roles)).toBe("object");
                    done();
                    })
                .catch(done.fail);
    });
    it("should be able to make the databus think it is disconnected", function(done) {
            DataBus.setLocal("session", "1", "status", "disconnected", "DataBus.disconnect")
                .then(function(result) {
                    done();
                })
                .catch(function(error) {
                    done.fail(error);
                });
    });
    it("should be able to re-connect using saved session token", function(done) {
            window.DataBus.connect()
                .then(function(session) {
                    expect(session.status).toBe("authenticated");
                    window.DataBus.getStatus()
                        .then(function(status) {
                            expect(status).toBe("authenticated");
                            done();
                        })
                        .catch(done.fail)
                })
                .catch(done.fail);
        });
});
describe("DataBus - logged in/connected", function() {
    it("should be able to get a value from a solution (service with router)", function(done) {
        window.DataBus.getRemote("test", "1", "$.some.data.value", "uppt-databus-spec.getRemoteValue")
            .then(function(result) {
                expect(result.domain).toBe("test");
                expect(result.version).toBe("1");
                expect(result.operation).toBe("getRemote");
                expect(result.jsonPath).toBe("$.some.data.value");
                expect(result.paths[0]).toBe("some.data.value");
                expect(result.values[0]).toBe(1234);
                expect(result.origins[0]).toBe("solutionService1");
                done();
            })
            .catch(done.fail);
    });

    it("should have stored the retrieved value locally as well (kind of a cache)", function(done) {
        window.DataBus.getLocal("test", "1", "$.some.data.value", "uppt-databus-spec.getValueFromCache")
            .then(function(result) {
                expect(result.domain).toBe("test");
                expect(result.version).toBe("1");
                expect(result.operation).toBe("getLocal");
                expect(result.jsonPath).toBe("$.some.data.value");
                // watch out: path and values are in an array!
                expect(result.paths[0]).toBe("some.data.value");
                expect(result.values[0]).toBe(1234);
                expect(result.origins[0]).toBe("solutionService1");
                done();
            })
            .catch(done.fail);
    });

    it("should notify observers of a remotely retrieved value (/object/array)", function(done) {
            window.DataBus.subscribe("test", "1", "some.data.value", false, function(result, subscription) {
                expect(result.domain).toBe("test");
                expect(result.version).toBe("1");
                expect(result.operation).toBe("getRemote");
                expect(result.paths[0]).toBe("some.data.value");
                expect(result.values[0]).toBe(1234);
                expect(result.origins[0]).toBe("solutionService1");
                expect(typeof(result.correlationID)).toBe("string");
                expect(subscription.unsubscribe()).toBe(true);
                done();
            }, done.fail);

            window.DataBus.getRemote("test", "1", "$.some.data.value", "uppt-databus-spec.getRemoteValue")
                .then(function(result) {
                    expect(result.domain).toBe("test");
                    expect(result.version).toBe("1");
                    expect(result.operation).toBe("getRemote");
                    expect(result.jsonPath).toBe("$.some.data.value");
                    expect(result.paths[0]).toBe("some.data.value");
                    expect(result.values[0]).toBe(1234);
                    expect(result.origins[0]).toBe("solutionService1");
                })
                .catch(done.fail);
    });

    it("should be able to get an object from a solution (service with router)", function(done) {
        window.DataBus.getRemote("test", "1", "$.some.data.object", "uppt-databus-spec.getRemoteObject")
            .then(function(result) {
                expect(result.domain).toBe("test");
                expect(result.version).toBe("1");
                expect(result.operation).toBe("getRemote");
                expect(result.jsonPath).toBe("$.some.data.object");
                expect(result.paths[0]).toBe("some.data.object");
                expect(typeof result.values[0]).toBe("object");
                expect(result.values[0].subValue).toBe(4);
                expect(result.origins[0]).toBe("solutionService1");
                done();
            })
            .catch(done.fail);
    });

    it("should have stored the retrieved object locally as well (kind of a cache)", function(done) {
        window.DataBus.getLocal("test", "1", "$.some.data.object", "uppt-databus-spec.getObjectFromCache")
            .then(function(result) {
                expect(result.domain).toBe("test");
                expect(result.version).toBe("1");
                expect(result.operation).toBe("getLocal");
                expect(result.jsonPath).toBe("$.some.data.object");
                // watch out: path and values are in an array!
                expect(result.paths[0]).toBe("some.data.object");
                expect(typeof result.values[0]).toBe("object");
                expect(result.values[0].subValue).toBe(4);
                expect(result.origins[0]).toBe("solutionService1");
                done();
            })
            .catch(done.fail);
    });

    it("should be able to get an array from a solution (service with router)", function(done) {
        window.DataBus.getRemote("test", "1", "$.some.data.array", "uppt-databus-spec.getRemoteArray")
            .then(function(result) {
                expect(result.domain).toBe("test");
                expect(result.version).toBe("1");
                expect(result.operation).toBe("getRemote");
                expect(result.jsonPath).toBe("$.some.data.array");
                expect(result.paths[0]).toBe("some.data.array");
                expect(typeof result.values[0]).toBe("object");
                expect(result.values[0].length).toBe(3);
                expect(result.values[0][0]).toBe(1);
                expect(result.values[0][1]).toBe(2);
                expect(result.values[0][2]).toBe(3);
                expect(result.origins[0]).toBe("solutionService1");
                done();
            })
            .catch(done.fail);
    });

    it("should have stored the retrieved array locally as well (kind of a cache)", function(done) {
        window.DataBus.getLocal("test", "1", "$.some.data.array", "uppt-databus-spec.getArrayFromCache")
            .then(function(result) {
                expect(result.domain).toBe("test");
                expect(result.version).toBe("1");
                expect(result.operation).toBe("getLocal");
                expect(result.jsonPath).toBe("$.some.data.array");
                expect(result.paths[0]).toBe("some.data.array");
                expect(typeof result.values[0]).toBe("object");
                expect(result.values[0].length).toBe(3);
                expect(result.values[0][0]).toBe(1);
                expect(result.values[0][1]).toBe(2);
                expect(result.values[0][2]).toBe(3);
                expect(result.origins[0]).toBe("solutionService1");
                done();
            })
            .catch(done.fail);
    });

    it("should be able to set values remotely (and have the cache updated and notify observers)", function(done) {
        window.DataBus.subscribe("test", "1", "some.data.value", false, function(result, subscription) {
            expect(result.domain).toBe("test");
            expect(result.version).toBe("1");
            expect(result.operation).toBe("setRemote");
            expect(result.paths[0]).toBe("some.data.value");
            expect(result.values[0]).toBe("dummyValue");
            expect(result.origins[0]).toBe("solutionService1");
            expect(typeof(result.correlationID)).toBe("string");
            expect(subscription.unsubscribe()).toBe(true);

            window.DataBus.getLocal("test", "1", "$.some.data.value", "uppt-databus-spec.getValueFromCache")
                .then(function(result) {
                    expect(result.domain).toBe("test");
                    expect(result.version).toBe("1");
                    expect(result.operation).toBe("getLocal");
                    expect(result.jsonPath).toBe("$.some.data.value");
                    // watch out: path and values are in an array!
                    expect(result.paths[0]).toBe("some.data.value");
                    expect(result.values[0]).toBe("dummyValue");
                    expect(result.origins[0]).toBe("uppt-databus-spec.setRemoteValue");
                    done();
                })
                .catch(done.fail);
        }, done.fail);

        window.DataBus.setRemote("test", "1", "some.data.value", "dummyValue", "uppt-databus-spec.setRemoteValue")
            .then(function(result) {
                expect(result.domain).toBe("test");
                expect(result.version).toBe("1");
                expect(result.operation).toBe("setRemote");
                expect(result.paths[0]).toBe("some.data.value");
                expect(result.values[0]).toBe("dummyValue");
                expect(typeof(result.correlationID)).toBe("string");
                //expect(result.origins[0]).toBe("uppt-databus-spec.setRemoteValue");
            })
            .catch(done.fail);
    });

    it("should be able to set objects remotely", function(done) {
        var value = {"subValue" : "newValue", "subValue2": [1,2,3]};
        window.DataBus.setRemote("test", "1", "some.data.object", value, "uppt-databus-spec.setRemoteObject")
            .then(function(result) {
                expect(result.domain).toBe("test");
                expect(result.version).toBe("1");
                expect(result.operation).toBe("setRemote");
                expect(result.paths[0]).toBe("some.data.object");
                expect(JSON.stringify(result.values[0])).toBe(JSON.stringify(value));
                expect(typeof(result.correlationID)).toBe("string");
                expect(result.origins[0]).toBe("solutionService1");
                done();
            })
            .catch(done.fail);
    });

    it("should be able to set arrays remotely", function(done) {
        var value = ["entry 1", {"value": 2} , 3];
        window.DataBus.setRemote("test", "1", "some.data.array", value, "uppt-databus-spec.setRemoteArray")
            .then(function(result) {
                expect(result.domain).toBe("test");
                expect(result.version).toBe("1");
                expect(result.operation).toBe("setRemote");
                expect(result.paths[0]).toBe("some.data.array");
                expect(JSON.stringify(result.values[0])).toBe(JSON.stringify(value));
                expect(typeof(result.correlationID)).toBe("string");
                expect(result.origins[0]).toBe("solutionService1");
                done();
            })
            .catch(done.fail);
    });

    it("enables deleting values remotely (note that locally/in the cache that value is set to null - not 'delete'/'dereference'!)", function(done) {
        expect(typeof(window.DataBus.delRemote)).toBe('function');

        window.DataBus.subscribe("test", "1", "some.data.value", false, function(result, subscription) {
            expect(result.domain).toBe("test");
            expect(result.version).toBe("1");
            expect(result.operation).toBe("delRemote");
            expect(result.paths[0]).toBe("some.data.value");
            expect(result.values[0]).toBe(null);
            expect(result.origins[0]).toBe("solutionService1");
            expect(typeof(result.correlationID)).toBe("string");
            expect(subscription.unsubscribe()).toBe(true);

            window.DataBus.getLocal("test", "1", "$.some.data.value", "uppt-databus-spec.getValueFromCache")
                .then(function(result) {
                    expect(result.domain).toBe("test");
                    expect(result.version).toBe("1");
                    expect(result.operation).toBe("getLocal");
                    expect(result.jsonPath).toBe("$.some.data.value");
                    // watch out: path and values are in an array!
                    expect(result.paths[0]).toBe("some.data.value");
                    expect(result.values[0]).toBe(null);
                    expect(result.origins[0]).toBe("uppt-databus-spec.delRemote");
                    done();
                })
                .catch(done.fail);
        }, done.fail);

        window.DataBus.delRemote("test", "1", "some.data.value", "uppt-databus-spec.delRemote")
            .then(function(result) {
                expect(result.domain).toBe("test");
                expect(result.version).toBe("1");
                expect(result.operation).toBe("delRemote");
                expect(result.paths[0]).toBe("some.data.value");
                expect(result.values[0]).toBe(null);
                expect(typeof(result.correlationID)).toBe("string");
                expect(result.origins[0]).toBe("solutionService1");
            })
            .catch(done.fail);
    });

    it("prefers returning local values upon 'get' before requesting them from the server", function(done) {
        // the set path/value does not exist on server side, so result can only be returned if local value is found and returned
        window.DataBus.setLocal("test", "1", "path.to.localOnlyValue", "dummyValueLocal", "uppt-databus-spec.prepareCache")
            .then(function() {
                window.DataBus.get("test", "1", "$.path.to.localOnlyValue", "uppt-databus-spec.getCachedValue")
                    .then(function(result) {
                        expect(result.domain).toBe("test");
                        expect(result.version).toBe("1");
                        expect(result.operation).toBe("get");
                        expect(result.jsonPath).toBe("$.path.to.localOnlyValue");
                        // watch out: path and values are in an array!
                        expect(result.paths[0]).toBe("path.to.localOnlyValue");
                        expect(result.values[0]).toBe("dummyValueLocal");
                        expect(result.origins[0]).toBe("uppt-databus-spec.prepareCache");

                        // requesting something, that is not locally available yet
                        window.DataBus.get("test", "1", "$.some.data.anotherValue", "uppt-databus-spec.getFreshValue")
                            .then(function(result) {
                                expect(result.domain).toBe("test");
                                expect(result.version).toBe("1");
                                expect(result.operation).toBe("get");
                                expect(result.jsonPath).toBe("$.some.data.anotherValue");
                                // watch out: path and values are in an array!
                                expect(result.paths[0]).toBe("some.data.anotherValue");
                                expect(result.values[0]).toBe(4321);
                                expect(result.origins[0]).toBe("solutionService1");

                                // check if it is stored locally as well
                                window.DataBus.getLocal("test", "1", "$.some.data.anotherValue", "uppt-databus-spec.getCachedValue")
                                    .then(function(result) {
                                        expect(result.domain).toBe("test");
                                        expect(result.version).toBe("1");
                                        expect(result.operation).toBe("getLocal");
                                        expect(result.jsonPath).toBe("$.some.data.anotherValue");
                                        // watch out: path and values are in an array!
                                        expect(result.paths[0]).toBe("some.data.anotherValue");
                                        expect(result.values[0]).toBe(4321);
                                        expect(result.origins[0]).toBe("solutionService1");
                                        done();
                                    })
                                    .catch(done.fail);
                            })
                            .catch(done.fail);
                    })
                    .catch(done.fail);
            })
            .catch(done.fail);
    });
    it("enables the service to push data updates and observers get notified, pushed data should have been set", function(done) {
        // TODO: why does it take so long to get the push data? Handler registration?
        window.DataBus.subscribe("testSolution", "1", "push.data.value", true, function(result, subscription) {
            expect(result.domain).toBe("testSolution");
            expect(result.version).toBe("1");
            //expect(result.operation).toBe("setPush");
            expect(result.paths[0]).toBe("push.data.value");
            expect(result.values[0]).toBe("foo");
            expect(result.origins[0]).toBe("solutionService1");
            expect(typeof(result.correlationID)).toBe("string");
            expect(subscription.unsubscribe()).toBe(true);

            done();
        }, done.fail);
    });
     it("should log out", function(done) {
        window.DataBus.subscribe("session", "1", "status", function(result, subscription) {
            if(result.values[0] === "disconnected") {
                expect(subscription.unsubscribe()).toBe(true);
                window.DataBus.getLocal("session", "1", "$.status", "uppt-databus-spec")
                    .then(function(result) {
                        expect(result.values[0]).toBe("disconnected");
                        done();
                    })
                    .catch(done.fail);
            }
        });
        window.DataBus.setLocal("session", "1", "status", "disconnecting", "uppt-databus-spec");
    });
});
*/
package de.maltebehrendt.uppt.services;

import de.maltebehrendt.uppt.annotation.*;
import de.maltebehrendt.uppt.enums.DataType;
import de.maltebehrendt.uppt.enums.Operation;
import de.maltebehrendt.uppt.events.Message;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.HashMap;

/**
 * Created by malte on 05.03.17.
 */
public class SolutionService1 extends AbstractSolution {

    // of course, in real life this must not be done state-full but saved in a shared db or sth similar
    private JsonObject testData = null;

    @Override
    public void prepare(Future<Object> prepareFuture) {
        // prepare solution setup here

        // some test data
        testData = new JsonObject()
                .put("value", 1234)
                .put("anotherValue", 4321)
                .put("array", new JsonArray().add(1).add(2).add(3))
                .put("object", new JsonObject().put("subValue", 4));
        prepareFuture.complete();
    }


    @Override
    public void shutdown(Future<Object> shutdownFuture) {
        shutdownFuture.complete();
    }

    @Router(domain = "in.test",
            version = "1",
            type = "router",
            description = "Minimal test router",
            authorities = @Authorities(
                    isAuthenticationRequired = true,
                    userRoles = {"tester"}
            )
    )
    public void router(Message message) {
        // the results from the results are in the body's "results" JsonArray
        // you can either perform further operations or just return it
        // the user id/roles are accessible as usual via message.userID()/message.userRoles()
        message.reply(message.getBodyAsJsonObject());
    }

    @Override
    public void handleSessionBecameAvailable(JsonObject sessionInfo) {
        //logger.info("Session became available event: " + sessionInfo.encode());

        JsonObject config = sessionInfo.getJsonObject("config");
        String addressDomain = config.getString("addressDomain");
        String addressVersion = config.getString("addressVersion");
        String addressType = config.getString("addressType");

        // push some data to the session/user
        send(addressDomain, addressVersion, addressType, new JsonObject()
                .put("results", new JsonArray()
                        .add(new JsonObject()
                                .put("domain","testSolution")
                                .put("version","1")
                                .put("paths", new JsonArray().add("push.data.value"))
                                .put("values",new JsonArray().add("foo"))
                                .put("origins", new JsonArray().add("solutionService1"))
                        )
                ), null);
    }

    @Override
    public void handleSessionBecameAuthenticated(JsonObject sessionInfo) {
        //logger.info("Session became authenticated event: " + sessionInfo.encode());
    }

    @Override
    public void handleSessionBecameUnavailable(JsonObject sessionInfo) {
        //logger.info("Session became unavailable event: " + sessionInfo.encode());
    }

    @Route(
            description = "Performs a get operation in any path ('*') of this domain/version",
            domain = "test",
            version = "1",
            path = "some.data.*",
            operation = Operation.GET
    )
    public void routeGet(Message message) {
        // userID/userRoles are included in the payload/body, not in the header as usual

        JsonArray pathSteps = message.getBodyAsJsonObject().getJsonArray("pathSteps");
        JsonArray pathPredicates = message.getBodyAsJsonObject().getJsonArray("pathPredicates");

        // real scenarios: walk/parse steps and predicates
        // here I know what's being requested...
        String path = null;
        Object value = null;
        if(pathSteps.size() != 3) {
            message.reply(404, "Path steps could not be matched!");
            return;
        }

        switch(pathSteps.getString(2)) {
            case "value":
                path = "some.data.value";
                value = testData.getValue("value");
                break;
            case "anotherValue":
                path = "some.data.anotherValue";
                value = testData.getValue("anotherValue");
                break;
            case "array":
                path = "some.data.array";
                value = testData.getValue("array");
                break;
            case "object":
                path = "some.data.object";
                value = testData.getValue("object");
                break;
            default:
                message.reply(404, "Path steps could not be matched!");
                return;
        }

        message.reply(new JsonObject()
                .put("paths", new JsonArray().add(path))
                .put("values", new JsonArray().add(value))
                .put("origins", new JsonArray().add("solutionService1"))
        );
    }

    @Route(
            description = "Performs a set operation in any path ('*') of this domain/version",
            domain = "test",
            version = "1",
            path = "some.data.*",
            operation = Operation.SET
    )
    public void routeSet(Message message) {
        // userID/userRoles are included in the payload/body, not in the header as usual
        if(!message.getBodyAsJsonObject().getJsonArray("userRoles").contains("tester")) {
            message.reply(401, "User is not allowed to set!");
            return;
        }

        JsonArray pathSteps = message.getBodyAsJsonObject().getJsonArray("pathSteps");
        JsonArray pathPredicates = message.getBodyAsJsonObject().getJsonArray("pathPredicates");
        Object value = message.getBodyAsJsonObject().getValue("value");
        // real scenarios: walk/parse steps and predicates
        // here I know what's being requested...

        if(pathSteps.size() != 3) {
            message.reply(404, "Path steps could not be matched!");
            return;
        }

        testData.put(pathSteps.getString(2), value);

        message.reply(new JsonObject()
                .put("paths", new JsonArray().add("some.data." + pathSteps.getString(2)))
                .put("values", new JsonArray().add(value))
                .put("origins", new JsonArray().add("solutionService1"))
        );
    }

    @Route(
            description = "Performs a del operation in any path ('*') of this domain/version",
            domain = "test",
            version = "1",
            path = "some.data.*",
            operation = Operation.DEL
    )
    public void routeDel(Message message) {
        // userID/userRoles are included in the payload/body, not in the header as usual
        if(!message.getBodyAsJsonObject().getJsonArray("userRoles").contains("tester")) {
            message.reply(401, "User is not allowed to delete!");
            return;
        }

        JsonArray pathSteps = message.getBodyAsJsonObject().getJsonArray("pathSteps");
        JsonArray pathPredicates = message.getBodyAsJsonObject().getJsonArray("pathPredicates");
        // real scenarios: walk/parse steps and predicates
        // here I know what's being requested...

        if(pathSteps.size() != 3) {
            message.reply(404, "Path steps could not be matched!");
            return;
        }

        testData.remove(pathSteps.getString(2));

        message.reply(new JsonObject()
                .put("paths", new JsonArray().add("some.data." + pathSteps.getString(2)))
                .put("values", new JsonArray().add(""))
                .put("origins", new JsonArray().add("solutionService1"))
        );
    }
}

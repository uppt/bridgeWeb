package de.maltebehrendt.uppt;

import de.maltebehrendt.uppt.junit.AbstractTest;
import de.maltebehrendt.uppt.util.EncryptionUtils;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.http.*;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import java.io.File;
import java.lang.invoke.MethodHandles;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by malte on 31.01.17.
 */
public class BridgeWebService1Test extends AbstractTest {
    private static HashMap<String, JsonObject> userAccounts = new HashMap<>();
    private Integer userCounter = 0;

    // TODO: huge technical debt: here mean attacks/invalid messages should be tested. Normal stuff via JSDataBusSpec1Test
    // TODO: traits

    @BeforeClass
    public static void setup(TestContext context) {
        setupTestVertxAndDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
    }

    @AfterClass
    public static void teardown(TestContext context) {
        tearDownTestVertxAndCleanupDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
    }


    @Test
    public void testAnonymousSessionSetupViaWebsocket(TestContext context) {
        Async requestResult = context.async(2);

        // try to connect to the unbridged address
        HttpClientOptions clientOptions = new HttpClientOptions()
                .setSsl(false)
                .setTrustAll(true);
        HttpClient client = vertx.createHttpClient(clientOptions);
        client.websocket(8080, "localhost", "/eventbus/websocket", webSocket -> {
            webSocket.frameHandler(frame -> {
                JsonObject response = null;

                if(frame.isFinal()) {
                    if(frame.isText()) {
                        response = new JsonObject(frame.textData());
                    }
                    else if(frame.isBinary()) {
                        response = new JsonObject(frame.binaryData().toString());
                    }

                    JsonObject header = response.getJsonObject("headers");
                    context.assertEquals("200", header.getString("statusCode"));
                    context.assertEquals("webUser", header.getString("origin"));
                    context.assertNotNull(header.getString("correlationID"));

                    JsonObject body = response.getJsonObject("body");

                    JsonObject user = body.getJsonObject("user");
                    context.assertNotNull(user);
                    context.assertEquals("anonymous", user.getString("id"));
                    context.assertTrue(user.getJsonArray("roles").contains("anonymous"));
                    context.assertNotNull(user.getString("sessionToken"));
                    context.assertTrue(!user.getString("sessionToken").isEmpty());

                    JsonObject config = body.getJsonObject("config");
                    context.assertNotNull(config);
                    context.assertEquals("127.0.0.1", config.getString("source"));
                    context.assertNotNull(config.getLong("startTimestamp"));
                    context.assertNotNull(config.getString("addressType"));
                    context.assertTrue(!config.getString("addressType").isEmpty());
                    context.assertNotNull(config.getString("addressDomain"));
                    context.assertTrue(!config.getString("addressDomain").isEmpty());
                    context.assertNotNull(config.getString("addressVersion"));
                    context.assertTrue(!config.getString("addressVersion").isEmpty());
                    context.assertTrue(config.getBoolean("isBackendLoggingEnabled"));
                    context.assertEquals(3, config.getInteger("backendLoggingLevel"));
                    context.assertNotNull(config.getJsonObject("traits"));
//                    context.assertEquals(1, body.getJsonObject("traits").size());
                    //                  context.assertTrue(body.getJsonObject("traits").containsKey("someTestTrait"));
                    //                context.assertTrue(body.getJsonObject("traits").getBoolean("someTestTrait"));

                    requestResult.countDown();
                }
            });

            webSocket.closeHandler(closing -> {});
            webSocket.exceptionHandler(exception -> {
                context.assertTrue(false, exception.getMessage());
            });


            JsonObject message = new JsonObject()
                    .put("type", "send")
                    .put("address", "in.sessions.1.sessionMissing")
                    .put("headers", new JsonObject())
                    .put("body", new JsonObject()
                            .put("sessionContext", new JsonObject()
                                .put("source", "127.0.0.1")
                                .put("device", new JsonObject())
                                .put("connection", new JsonObject())
                                .put("navigator", new JsonObject())
                                .put("pathName", "/")
                                .put("routerDomain", "in.test")
                                .put("routerVersion", "1")
                                .put("routerType", "router")))
                    .put("replyAddress", "abc");

            webSocket.writeFinalTextFrame(message.encode());
            requestResult.countDown();
        }, failureHandler -> {
            context.assertFalse(true, failureHandler.getMessage());
            failureHandler.printStackTrace();
        });

        requestResult.awaitSuccess();
        client.close();
    }

    // TODO: remove
    //@Test
    public void testMissingSessionAndAuthentication(TestContext context) {
        Async requestResult = context.async(3);

        vertx.executeBlocking((Future<JsonObject> future) -> {
            getNewUser(new JsonArray().add("testUser"), future);
        }, result -> {
            JsonObject newUser = result.result();
            requestResult.countDown();

            HttpClientOptions clientOptions = new HttpClientOptions()
                    .setSsl(false)
                    .setTrustAll(true);
            HttpClient client = vertx.createHttpClient(clientOptions);

            client.websocket(8080, "localhost", "/eventbus/websocket", webSocket -> {
                String sessionToken[] = new String[1];

                webSocket.frameHandler(frame -> {
                    JsonObject response = null;

                    if(frame.isFinal()) {
                        if(frame.isText()) {
                            response = new JsonObject(frame.textData());
                        }
                        else if(frame.isBinary()) {
                            response = new JsonObject(frame.binaryData().toString());
                        }

                        if("sessionAuthResponse".equals(response.getString("address"))) {
                            JsonObject header = response.getJsonObject("headers");
                            context.assertEquals("200", header.getString("statusCode"));
                            context.assertEquals("webUser", header.getString("origin"));
                            context.assertNotNull(header.getString("correlationID"));

                            JsonObject body = response.getJsonObject("body");

                            JsonObject user = body.getJsonObject("user");
                            context.assertNotNull(user);
                            context.assertEquals(newUser.getString("email"), user.getString("id"));
                            context.assertEquals(2, user.getJsonArray("roles").size());
                            context.assertTrue(user.getJsonArray("roles").contains("authenticated"));
                            context.assertTrue(user.getJsonArray("roles").contains("testUser"));
                            context.assertNotNull(user.getString("sessionToken"));
                            context.assertTrue(!user.getString("sessionToken").isEmpty());

                            JsonObject config = body.getJsonObject("config");
                            context.assertNotNull(config);
                            context.assertEquals("127.0.0.1", config.getString("source"));
                            context.assertNotNull(config.getLong("startTimestamp"));
                            context.assertNotNull(config.getString("addressType"));
                            context.assertTrue(!config.getString("addressType").isEmpty());
                            context.assertNotNull(config.getString("addressDomain"));
                            context.assertTrue(!config.getString("addressDomain").isEmpty());
                            context.assertNotNull(config.getString("addressVersion"));
                            context.assertTrue(!config.getString("addressVersion").isEmpty());
                            context.assertTrue(config.getBoolean("isBackendLoggingEnabled"));
                            context.assertEquals(3, config.getInteger("backendLoggingLevel"));
                            context.assertNotNull(config.getJsonObject("traits"));

                            requestResult.countDown();
                        }
                    }
                });

                //webSocket.closeHandler(closing -> {System.err.println("closed");});
                webSocket.exceptionHandler(exception -> {
                    exception.printStackTrace();
                });

                JsonObject message = new JsonObject()
                        .put("type", "send")
                        .put("address", "in.sessions.1.sessionAndAuthenticationMissing")
                        .put("headers", new JsonObject())
                        .put("body", new JsonObject()
                                .put("sessionContext", new JsonObject()
                                    .put("source", "127.0.0.1")
                                    .put("device", new JsonObject())
                                    .put("connection", new JsonObject())
                                    .put("navigator", new JsonObject())
                                    .put("pathName", "/")
                                    .put("routerDomain", "in.test")
                                    .put("routerVersion", "1")
                                    .put("routerType", "router"))
                                .put("authType", "email")
                                .put("authConfig", new JsonObject()
                                    .put("password", newUser.getString("password"))
                                    .put("email", newUser.getString("email")))
                        )
                        .put("replyAddress", "sessionAuthResponse");

                webSocket.writeFinalTextFrame(message.encode());
                requestResult.countDown();
            }, failureHandler -> {
                context.assertFalse(true, failureHandler.getMessage());
                failureHandler.printStackTrace();
            });
        });

        requestResult.awaitSuccess();
    }

    @Test
    public void testMissingSessionAndLaterAuthenticationViaEmail(TestContext context) {
        Async requestResult = context.async(5);

        vertx.executeBlocking((Future<JsonObject> future) -> {
            getNewUser(new JsonArray().add("testUser"), future);
        }, result -> {
            JsonObject newUser = result.result();
            requestResult.countDown();

            HttpClientOptions clientOptions = new HttpClientOptions()
                    .setSsl(false)
                    .setTrustAll(true);
            HttpClient client = vertx.createHttpClient(clientOptions);

            client.websocket(8080, "localhost", "/eventbus/websocket", webSocket -> {
                String sessionToken[] = new String[1];

                webSocket.frameHandler(frame -> {
                    JsonObject response = null;

                    if(frame.isFinal()) {
                        if(frame.isText()) {
                            response = new JsonObject(frame.textData());
                        }
                        else if(frame.isBinary()) {
                            response = new JsonObject(frame.binaryData().toString());
                        }

                        if("sessionResponse".equals(response.getString("address"))) {
                            JsonObject header = response.getJsonObject("headers");
                            context.assertEquals("200", header.getString("statusCode"));
                            context.assertEquals("webUser", header.getString("origin"));
                            context.assertNotNull(header.getString("correlationID"));

                            JsonObject body = response.getJsonObject("body");

                            JsonObject user = body.getJsonObject("user");
                            context.assertNotNull(user);
                            context.assertEquals("anonymous", user.getString("id"));
                            context.assertTrue(user.getJsonArray("roles").contains("anonymous"));
                            context.assertNotNull(user.getString("sessionToken"));
                            context.assertTrue(!user.getString("sessionToken").isEmpty());

                            JsonObject config = body.getJsonObject("config");
                            context.assertNotNull(config);
                            context.assertEquals("127.0.0.1", config.getString("source"));
                            context.assertNotNull(config.getLong("startTimestamp"));
                            context.assertNotNull(config.getString("addressType"));
                            context.assertTrue(!config.getString("addressType").isEmpty());
                            context.assertNotNull(config.getString("addressDomain"));
                            context.assertTrue(!config.getString("addressDomain").isEmpty());
                            context.assertNotNull(config.getString("addressVersion"));
                            context.assertTrue(!config.getString("addressVersion").isEmpty());
                            context.assertTrue(config.getBoolean("isBackendLoggingEnabled"));
                            context.assertEquals(3, config.getInteger("backendLoggingLevel"));
                            context.assertNotNull(config.getJsonObject("traits"));
//                    context.assertEquals(1, body.getJsonObject("traits").size());
                            //                  context.assertTrue(body.getJsonObject("traits").containsKey("someTestTrait"));
                            //                context.assertTrue(body.getJsonObject("traits").getBoolean("someTestTrait"));

                            requestResult.countDown();

                            // try login
                            JsonObject message = new JsonObject()
                                    .put("type", "send")
                                    .put("address", "in.sessions.1.authenticationMissing")
                                    .put("headers", new JsonObject().put("sessionToken", user.getString("sessionToken")))
                                    .put("body", new JsonObject()
                                            .put("source", "127.0.0.1")
                                            .put("authType", "email")
                                            .put("authConfig", new JsonObject()
                                                    .put("emailAddress", newUser.getString("emailAddress"))
                                                    .put("password", newUser.getString("password")))
                                    )
                                    .put("replyAddress", "authResponse");

                            webSocket.writeFinalTextFrame(message.encode());
                            requestResult.countDown();
                        }
                        else if("authResponse".equals(response.getString("address"))) {
                            JsonObject header = response.getJsonObject("headers");
                            context.assertEquals("200", header.getString("statusCode"));
                            context.assertEquals("webUser", header.getString("origin"));
                            context.assertNotNull(header.getString("correlationID"));

                            JsonObject body = response.getJsonObject("body");
                            context.assertNotNull(body);
                            context.assertNotNull(body.getString("id"));
                            context.assertEquals(1, body.getJsonArray("roles").size());
                            context.assertTrue(body.getJsonArray("roles").contains("testUser"));

                            requestResult.countDown();
                        }
                    }
                });

                //webSocket.closeHandler(closing -> {System.err.println("closed");});
                webSocket.exceptionHandler(exception -> {
                    exception.printStackTrace();
                });


                JsonObject message = new JsonObject()
                        .put("type", "send")
                        .put("address", "in.sessions.1.sessionMissing")
                        .put("headers", new JsonObject())
                        .put("body", new JsonObject()
                                .put("sessionContext", new JsonObject()
                                        .put("source", "127.0.0.1")
                                        .put("device", new JsonObject())
                                        .put("connection", new JsonObject())
                                        .put("navigator", new JsonObject())
                                        .put("pathName", "/")
                                        .put("routerDomain", "in.test")
                                        .put("routerVersion", "1")
                                        .put("routerType", "router")))
                        .put("replyAddress", "sessionResponse");


                webSocket.writeFinalTextFrame(message.encode());
                requestResult.countDown();
            }, failureHandler -> {
                context.assertFalse(true, failureHandler.getMessage());
                failureHandler.printStackTrace();
            });
        });

        requestResult.awaitSuccess();
    }

    @Test
    public void testMissingSessionAndAuthenticationPreflightViaEmail(TestContext context) {
        Async requestResult = context.async(5);

        vertx.executeBlocking((Future<JsonObject> future) -> {
            getNewUser(new JsonArray().add("testUser"), future);
        }, result -> {
            JsonObject newUser = result.result();
            requestResult.countDown();

            HttpClientOptions clientOptions = new HttpClientOptions()
                    .setSsl(false)
                    .setTrustAll(true);
            HttpClient client = vertx.createHttpClient(clientOptions);

            client.websocket(8080, "localhost", "/eventbus/websocket", webSocket -> {
                String sessionToken[] = new String[1];

                webSocket.frameHandler(frame -> {
                    JsonObject response = null;

                    if(frame.isFinal()) {
                        if(frame.isText()) {
                            response = new JsonObject(frame.textData());
                        }
                        else if(frame.isBinary()) {
                            response = new JsonObject(frame.binaryData().toString());
                        }

                        if("sessionResponse".equals(response.getString("address"))) {
                            JsonObject header = response.getJsonObject("headers");
                            context.assertEquals("200", header.getString("statusCode"));
                            context.assertEquals("webUser", header.getString("origin"));
                            context.assertNotNull(header.getString("correlationID"));

                            JsonObject body = response.getJsonObject("body");

                            JsonObject user = body.getJsonObject("user");
                            context.assertNotNull(user);
                            context.assertEquals("anonymous", user.getString("id"));
                            context.assertTrue(user.getJsonArray("roles").contains("anonymous"));
                            context.assertNotNull(user.getString("sessionToken"));
                            context.assertTrue(!user.getString("sessionToken").isEmpty());

                            JsonObject config = body.getJsonObject("config");
                            context.assertNotNull(config);
                            context.assertEquals("127.0.0.1", config.getString("source"));
                            context.assertNotNull(config.getLong("startTimestamp"));
                            context.assertNotNull(config.getString("addressType"));
                            context.assertTrue(!config.getString("addressType").isEmpty());
                            context.assertNotNull(config.getString("addressDomain"));
                            context.assertTrue(!config.getString("addressDomain").isEmpty());
                            context.assertNotNull(config.getString("addressVersion"));
                            context.assertTrue(!config.getString("addressVersion").isEmpty());
                            context.assertTrue(config.getBoolean("isBackendLoggingEnabled"));
                            context.assertEquals(3, config.getInteger("backendLoggingLevel"));
                            context.assertNotNull(config.getJsonObject("traits"));
//                    context.assertEquals(1, body.getJsonObject("traits").size());
                            //                  context.assertTrue(body.getJsonObject("traits").containsKey("someTestTrait"));
                            //                context.assertTrue(body.getJsonObject("traits").getBoolean("someTestTrait"));

                            requestResult.countDown();

                            // try login
                            JsonObject message = new JsonObject()
                                    .put("type", "send")
                                    .put("address", "in.sessions.1.authenticationPreflight")
                                    .put("headers", new JsonObject().put("sessionToken", user.getString("sessionToken")))
                                    .put("body", new JsonObject()
                                            .put("authType", "email")
                                            .put("authConfig", new JsonObject()
                                                    .put("emailAddress", newUser.getString("emailAddress"))
                                                    .put("password", newUser.getString("password")))
                                    )
                                    .put("replyAddress", "preflightResponse");

                            webSocket.writeFinalTextFrame(message.encode());
                            requestResult.countDown();
                        }
                        else if("preflightResponse".equals(response.getString("address"))) {
                            JsonObject header = response.getJsonObject("headers");
                            context.assertEquals("202", header.getString("statusCode"));
                            context.assertEquals("webUser", header.getString("origin"));
                            context.assertNotNull(header.getString("correlationID"));

                            JsonObject body = response.getJsonObject("body");
                            context.assertNotNull(body);

                            requestResult.countDown();
                        }
                    }
                });

                //webSocket.closeHandler(closing -> {System.err.println("closed");});
                webSocket.exceptionHandler(exception -> {
                    exception.printStackTrace();
                });


                JsonObject message = new JsonObject()
                        .put("type", "send")
                        .put("address", "in.sessions.1.sessionMissing")
                        .put("headers", new JsonObject())
                        .put("body", new JsonObject()
                                .put("sessionContext", new JsonObject()
                                        .put("source", "127.0.0.1")
                                        .put("device", new JsonObject())
                                        .put("connection", new JsonObject())
                                        .put("navigator", new JsonObject())
                                        .put("pathName", "/")
                                        .put("routerDomain", "in.test")
                                        .put("routerVersion", "1")
                                        .put("routerType", "router")))
                        .put("replyAddress", "sessionResponse");


                webSocket.writeFinalTextFrame(message.encode());
                requestResult.countDown();
            }, failureHandler -> {
                context.assertFalse(true, failureHandler.getMessage());
                failureHandler.printStackTrace();
            });
        });

        requestResult.awaitSuccess();
    }


    @Test
    public void testAuthenticationAndBridgeAccessAfterAnonymousSessionSetupViaWebsocket(TestContext context) {
        Async requestResult = context.async(8);

        // register an  address
        MessageConsumer<JsonObject> consumer = eventBus.consumer("in.bridgeWebServicetest.1.bridgedTestAuth");
        consumer.handler(new Handler<Message<JsonObject>>() {
            @Override
            public void handle(Message<JsonObject> message) {
                context.assertEquals("webUser", message.headers().get("origin"), "MessageImpl origin set");
                context.assertNotNull(message.headers().get("userID"), "User id is provided");
                context.assertEquals("[\"testUser\"]", message.headers().get("userRoles"), "User is associated with its roles");
                context.assertEquals("127.0.0.1", message.body().getString("source"), "MessageImpl body is included");
                context.assertEquals("Test123", message.body().getString("data"), "Data was received");
                requestResult.countDown();
            }
        });


        JsonObject bridgingRequest = new JsonObject()
                .put("domain", "in.bridgeWebServicetest")
                .put("version", "1")
                .put("type", "bridgedTestAuth")
                .put("isInbound", true)
                .put("isOutbound", false)
                .put("authorities", new JsonObject().put("roles", new JsonArray().add("testUser")));

        send("bridge", "1", "bridgeMissing", bridgingRequest, reply -> {
            context.assertTrue(reply.succeeded(), "Received reply for new bridge request");
            context.assertEquals(200, reply.statusCode(), "New bridge request has been processed");
            requestResult.countDown();

            vertx.executeBlocking((Future<JsonObject> future) -> {
                getNewUser(new JsonArray().add("testUser"), future);
            }, result -> {
                JsonObject newUser = result.result();
                requestResult.countDown();

                HttpClientOptions clientOptions = new HttpClientOptions()
                        .setSsl(false)
                        .setTrustAll(true);
                HttpClient client = vertx.createHttpClient(clientOptions);
                final String[] sessionToken = new String[1];

                client.websocket(8080, "localhost", "/eventbus/websocket", webSocket -> {
                    webSocket.frameHandler(frame -> {
                        JsonObject response = null;

                        if(frame.isFinal()) {
                            if(frame.isText()) {
                                response = new JsonObject(frame.textData());
                            }
                            else if(frame.isBinary()) {
                                response = new JsonObject(frame.binaryData().toString());
                            }

                            if("sessionResponse".equals(response.getString("address"))) {
                                JsonObject header = response.getJsonObject("headers");
                                context.assertEquals("200", header.getString("statusCode"));
                                context.assertEquals("webUser", header.getString("origin"));
                                context.assertNotNull(header.getString("correlationID"));

                                JsonObject body = response.getJsonObject("body");

                                JsonObject user = body.getJsonObject("user");
                                context.assertNotNull(user);
                                context.assertEquals("anonymous", user.getString("id"));
                                context.assertTrue(user.getJsonArray("roles").contains("anonymous"));
                                context.assertNotNull(user.getString("sessionToken"));
                                context.assertTrue(!user.getString("sessionToken").isEmpty());

                                JsonObject config = body.getJsonObject("config");
                                context.assertNotNull(config);
                                context.assertEquals("127.0.0.1", config.getString("source"));
                                context.assertNotNull(config.getLong("startTimestamp"));
                                context.assertNotNull(config.getString("addressType"));
                                context.assertTrue(!config.getString("addressType").isEmpty());
                                context.assertNotNull(config.getString("addressDomain"));
                                context.assertTrue(!config.getString("addressDomain").isEmpty());
                                context.assertNotNull(config.getString("addressVersion"));
                                context.assertTrue(!config.getString("addressVersion").isEmpty());
                                context.assertTrue(config.getBoolean("isBackendLoggingEnabled"));
                                context.assertEquals(3, config.getInteger("backendLoggingLevel"));
                                context.assertNotNull(config.getJsonObject("traits"));
//                    context.assertEquals(1, body.getJsonObject("traits").size());
                                //                  context.assertTrue(body.getJsonObject("traits").containsKey("someTestTrait"));
                                //                context.assertTrue(body.getJsonObject("traits").getBoolean("someTestTrait"));

                                requestResult.countDown();

                                sessionToken[0] = user.getString("sessionToken");

                                // try login
                                JsonObject message = new JsonObject()
                                        .put("type", "send")
                                        .put("address", "in.sessions.1.authenticationMissing")
                                        .put("headers", new JsonObject().put("sessionToken", sessionToken[0]))
                                        .put("body", new JsonObject()
                                                .put("source", "127.0.0.1")
                                                .put("authType", "email")
                                                .put("authConfig", new JsonObject()
                                                        .put("emailAddress", newUser.getString("emailAddress"))
                                                        .put("password", newUser.getString("password")))
                                        )
                                        .put("replyAddress", "authResponse");

                                webSocket.writeFinalTextFrame(message.encode());
                                requestResult.countDown();
                            }
                            else if("authResponse".equals(response.getString("address"))) {
                                JsonObject header = response.getJsonObject("headers");
                                context.assertEquals("200", header.getString("statusCode"));
                                context.assertEquals("webUser", header.getString("origin"));
                                context.assertNotNull(header.getString("correlationID"));

                                JsonObject body = response.getJsonObject("body");
                                context.assertNotNull(body);
                                context.assertNotNull(body.getString("id"));
                                context.assertEquals(1, body.getJsonArray("roles").size());
                                context.assertTrue(body.getJsonArray("roles").contains("testUser"));

                                requestResult.countDown();

                                // try accessing restricted bridge
                                JsonObject message = new JsonObject()
                                        .put("type", "send")
                                        .put("address", "in.bridgeWebServicetest.1.bridgedTestAuth")
                                        .put("headers", new JsonObject().put("sessionToken", sessionToken[0]))
                                        .put("body", new JsonObject()
                                                .put("source", "127.0.0.1")
                                                .put("data", "Test123")
                                        );

                                webSocket.writeFinalTextFrame(message.encode());
                                requestResult.countDown();
                            }
                        }
                    });

                    //webSocket.closeHandler(closing -> {System.err.println("closed");});
                    webSocket.exceptionHandler(exception -> {
                        context.assertTrue(false, exception.getMessage());
                        System.err.println("Exception");
                    });

                    JsonObject message = new JsonObject()
                            .put("type", "send")
                            .put("address", "in.sessions.1.sessionMissing")
                            .put("headers", new JsonObject())
                            .put("body", new JsonObject()
                                    .put("sessionContext", new JsonObject()
                                            .put("source", "127.0.0.1")
                                            .put("device", new JsonObject())
                                            .put("connection", new JsonObject())
                                            .put("navigator", new JsonObject())
                                            .put("pathName", "/")
                                            .put("routerDomain", "in.test")
                                            .put("routerVersion", "1")
                                            .put("routerType", "router")))
                            .put("replyAddress", "sessionResponse");

                    webSocket.writeFinalTextFrame(message.encode());
                    requestResult.countDown();
                }, failureHandler -> {
                    context.assertFalse(true, failureHandler.getMessage());
                    failureHandler.printStackTrace();
                });
            });
        });

        requestResult.awaitSuccess();
    }

    //@Test
    public void testSessionHandover(TestContext context) {
        Async requestResult = context.async(8);

        // register an  address
        MessageConsumer<JsonObject> consumer = eventBus.consumer("in.bridgeWebServicetest.1.bridgedTestAuth2");
        consumer.handler(new Handler<Message<JsonObject>>() {
            @Override
            public void handle(Message<JsonObject> message) {
            }
        });

        JsonObject bridgingRequest = new JsonObject()
                .put("domain", "in.bridgeWebServicetest")
                .put("version", "1")
                .put("type", "bridgedTestAuth2")
                .put("isInbound", true)
                .put("isOutbound", false)
                .put("authorities", new JsonObject().put("roles", new JsonArray().add("testUser")));

        send("bridge", "1", "bridgeMissing", bridgingRequest, reply -> {
            context.assertTrue(reply.succeeded(), "Received reply for new bridge request");
            context.assertEquals(200, reply.statusCode(), "New bridge request has been processed");
            requestResult.countDown();

            vertx.executeBlocking((Future<JsonObject> future) -> {
                getNewUser(new JsonArray().add("testUser"), future);
            }, result -> {
                JsonObject newUser = result.result();
                requestResult.countDown();

                HttpClientOptions clientOptions = new HttpClientOptions()
                        .setSsl(false)
                        .setTrustAll(true);
                HttpClient client = vertx.createHttpClient(clientOptions);

                client.websocket(8080, "localhost", "/eventbus/websocket", webSocket -> {

                    webSocket.frameHandler(frame -> {
                        JsonObject response = null;

                        if (frame.isFinal()) {
                            if (frame.isText()) {
                                response = new JsonObject(frame.textData());
                            } else if (frame.isBinary()) {
                                response = new JsonObject(frame.binaryData().toString());
                            }

                            if ("sessionAuthResponse".equals(response.getString("address"))) {
                                JsonObject header = response.getJsonObject("headers");
                                context.assertEquals("200", header.getString("statusCode"));
                                context.assertEquals("webUser", header.getString("origin"));
                                context.assertNotNull(header.getString("correlationID"));

                                JsonObject body = response.getJsonObject("body");

                                JsonObject user = body.getJsonObject("user");
                                context.assertNotNull(user);
                                context.assertEquals(newUser.getString("email"), user.getString("id"));
                                context.assertEquals(2, user.getJsonArray("roles").size());
                                context.assertTrue(user.getJsonArray("roles").contains("authenticated"));
                                context.assertTrue(user.getJsonArray("roles").contains("testUser"));
                                String sessionToken = user.getString("sessionToken");
                                context.assertNotNull(sessionToken);
                                context.assertTrue(!sessionToken.isEmpty());

                                JsonObject config = body.getJsonObject("config");
                                context.assertNotNull(config);
                                context.assertEquals("127.0.0.1", config.getString("source"));
                                context.assertNotNull(config.getLong("startTimestamp"));
                                context.assertNotNull(config.getString("addressType"));
                                context.assertTrue(!config.getString("addressType").isEmpty());
                                context.assertNotNull(config.getString("addressDomain"));
                                context.assertTrue(!config.getString("addressDomain").isEmpty());
                                context.assertNotNull(config.getString("addressVersion"));
                                context.assertTrue(!config.getString("addressVersion").isEmpty());
                                context.assertTrue(config.getBoolean("isBackendLoggingEnabled"));
                                context.assertEquals(3, config.getInteger("backendLoggingLevel"));
                                context.assertNotNull(config.getJsonObject("traits"));

                                requestResult.countDown();


                                // start a new client
                                HttpClient client2 = vertx.createHttpClient(clientOptions);

                                client2.websocket(8080, "localhost", "/eventbus/websocket", webSocket2 -> {
                                    webSocket2.frameHandler(frame2 -> {
                                        JsonObject response2 = null;

                                        if (frame2.isFinal()) {
                                            if (frame2.isText()) {
                                                response2 = new JsonObject(frame2.textData());
                                            } else if (frame2.isBinary()) {
                                                response2 = new JsonObject(frame2.binaryData().toString());
                                            }

                                            if ("sessionAuthResponse2".equals(response2.getString("address"))) {
                                                JsonObject header2 = response2.getJsonObject("headers");
                                                context.assertEquals("200", header2.getString("statusCode"));
                                                context.assertEquals("webUser", header2.getString("origin"));
                                                context.assertNotNull(header2.getString("correlationID"));

                                                JsonObject body2 = response2.getJsonObject("body");

                                                JsonObject user2 = body2.getJsonObject("user");
                                                context.assertNotNull(user2);
                                                context.assertEquals(newUser.getString("email"), user2.getString("id"));
                                                context.assertEquals(2, user2.getJsonArray("roles").size());
                                                context.assertTrue(user2.getJsonArray("roles").contains("authenticated"));
                                                context.assertTrue(user2.getJsonArray("roles").contains("testUser"));
                                                String sessionToken2 = user2.getString("sessionToken");
                                                context.assertNotNull(sessionToken2);
                                                context.assertTrue(!sessionToken2.isEmpty());
                                                context.assertNotEquals(sessionToken, sessionToken2);

                                                JsonObject config2 = body2.getJsonObject("config");
                                                context.assertNotNull(config2);
                                                context.assertEquals("127.0.0.1", config2.getString("source"));
                                                context.assertNotNull(config2.getLong("startTimestamp"));
                                                context.assertNotNull(config2.getString("addressType"));
                                                context.assertTrue(!config2.getString("addressType").isEmpty());
                                                context.assertNotNull(config2.getString("addressDomain"));
                                                context.assertTrue(!config2.getString("addressDomain").isEmpty());
                                                context.assertNotNull(config2.getString("addressVersion"));
                                                context.assertTrue(!config2.getString("addressVersion").isEmpty());
                                                context.assertTrue(config2.getBoolean("isBackendLoggingEnabled"));
                                                context.assertEquals(3, config2.getInteger("backendLoggingLevel"));
                                                context.assertNotNull(config2.getJsonObject("traits"));

                                                requestResult.countDown();

                                                // now it should not be possible to use the old token anymore
                                                JsonObject message = new JsonObject()
                                                        .put("type", "send")
                                                        .put("address", "in.bridgeWebServicetest.1.bridgedTestAuth2")
                                                        .put("headers", new JsonObject().put("sessionToken", sessionToken))
                                                        .put("body", new JsonObject()
                                                                .put("source", "127.0.0.1")
                                                                .put("data", "Test123")
                                                        )
                                                        .put("replyAddress", "bridgeReply");

                                                webSocket.writeFinalTextFrame(message.encode());
                                                requestResult.countDown();
                                            }
                                        }
                                    });
                                    webSocket2.exceptionHandler(exception -> {
                                        exception.printStackTrace();
                                    });

                                    JsonObject message = new JsonObject()
                                            .put("type", "send")
                                            .put("address", "in.sessions.1.sessionAndAuthenticationMissing")
                                            .put("headers", new JsonObject().put("sessionToken", sessionToken))
                                            .put("body", new JsonObject()
                                                    .put("sessionContext", new JsonObject()
                                                            .put("source", "127.0.0.1")
                                                            .put("device", new JsonObject())
                                                            .put("connection", new JsonObject())
                                                            .put("navigator", new JsonObject())
                                                            .put("pathName", "/")
                                                            .put("routerDomain", "in.test")
                                                            .put("routerVersion", "1")
                                                            .put("routerType", "router"))
                                            )
                                            .put("replyAddress", "sessionAuthResponse2");

                                    webSocket2.writeFinalTextFrame(message.encode());
                                    requestResult.countDown();

                                }, failure -> {
                                    failure.printStackTrace();
                                });
                            }
                            else if("bridgeReply".equals(response.getString("address"))) {
                                JsonObject header = response.getJsonObject("headers");
                                context.assertEquals("409", header.getString("statusCode"));
                                requestResult.countDown();
                            }
                        }
                    });

                    //webSocket.closeHandler(closing -> {System.err.println("closed");});
                    webSocket.exceptionHandler(exception -> {
                        exception.printStackTrace();
                    });

                    JsonObject message = new JsonObject()
                            .put("type", "send")
                            .put("address", "in.sessions.1.sessionAndAuthenticationMissing")
                            .put("headers", new JsonObject())
                            .put("body", new JsonObject()
                                    .put("sessionContext", new JsonObject()
                                            .put("source", "127.0.0.1")
                                            .put("device", new JsonObject())
                                            .put("connection", new JsonObject())
                                            .put("navigator", new JsonObject())
                                            .put("pathName", "/")
                                            .put("routerDomain", "in.test")
                                            .put("routerVersion", "1")
                                            .put("routerType", "router"))
                                    .put("authType", "email")
                                    .put("authConfig", new JsonObject()
                                            .put("password", newUser.getString("password"))
                                            .put("email", newUser.getString("email")))
                            )
                            .put("replyAddress", "sessionAuthResponse");

                    webSocket.writeFinalTextFrame(message.encode());
                    requestResult.countDown();
                }, failureHandler -> {
                    context.assertFalse(true, failureHandler.getMessage());
                    failureHandler.printStackTrace();
                });
            });
        });
        requestResult.awaitSuccess();
    }

    //@Test
    public void testInAnonymousAccessToUnbridgedAddress(TestContext context) {
        Async requestResult = context.async(2);

        // register an unbridged address
        MessageConsumer<JsonObject> consumer = eventBus.consumer("in.bridgeWebServicetest.1.unknown");
        consumer.handler(new Handler<Message<JsonObject>>() {
            @Override
            public void handle(Message<JsonObject> message) {
            }
        });

        // try to connect to the unbridged address
        HttpClientOptions clientOptions = new HttpClientOptions()
                .setSsl(false)
                .setTrustAll(true);
        HttpClient client = vertx.createHttpClient(clientOptions);
        client.websocket(8080, "localhost", "/eventbus/websocket", websocketHandler -> {
            websocketHandler.frameHandler(frame -> {
                context.assertTrue(frame.isFinal());

                JsonObject message = new JsonObject(frame.textData());
                context.assertNotNull(message.getJsonObject("headers").getString("statusCode"), "Status code is returned.");
                context.assertEquals("404", message.getJsonObject("headers").getString("statusCode"), "Status code is returned.");
                context.assertNotNull(message.getJsonObject("body").getString("message"), "Non-null error message is send.");

                requestResult.countDown();
            });

            // send a message to an unbridged address
            JsonObject message = new JsonObject()
                    .put("type", "send")
                    .put("address", "in.bridgeWebServicetest.1.unknown")
                    .put("replyAddress", "123456")
                    .put("body", new JsonObject());
            websocketHandler.writeFinalTextFrame(message.encode());
            requestResult.countDown();
        });

        requestResult.awaitSuccess();
        client.close();
    }

    /*
    @Test
    public void testInAnonymousAccessToBridgedAddress(TestContext context) {
        Async requestResult = context.async(3);

        // register an  address
        MessageConsumer<JsonObject> consumer = eventBus.consumer("in.bridgeWebServicetest.1.bridged");
        consumer.handler(new Handler<Message<JsonObject>>() {
            @Override
            public void handle(Message<JsonObject> message) {
                context.assertEquals("webUser", message.headers().get("origin"), "MessageImpl origin set");
                context.assertEquals("anonymous", message.headers().get("userID"), "User is anonymous");
                context.assertTrue("[\"anonymous\"]".equalsIgnoreCase(message.headers().get("userRoles")) || "[anonymous]".equalsIgnoreCase(message.headers().get("userRoles")), "User is not associated with roles except for anonymous");
                context.assertEquals("WSClient", message.body().getString("source"), "MessageImpl body is included");
                requestResult.countDown();
            }
        });

        JsonObject bridgingRequest = new JsonObject()
                .put("domain", "in.bridgeWebServicetest")
                .put("version", "1")
                .put("type", "bridged")
                .put("isInbound", true)
                .put("isOutbound", false)
                .put("authorities", new JsonObject());

        send("bridge", "1", "bridgeMissing", bridgingRequest, reply -> {
            context.assertTrue(reply.succeeded(), "Received reply for new bridge request");
            context.assertEquals(200, reply.statusCode(), "New bridge request has been processed");
            requestResult.countDown();

            // try to connect to the unbridged address
            HttpClientOptions clientOptions = new HttpClientOptions()
                    .setSsl(false)
                    .setTrustAll(true);
            HttpClient client = vertx.createHttpClient(clientOptions);
            client.websocket(8080, "localhost", "/eventbus/websocket", websocketHandler -> {
                websocketHandler.frameHandler(frame -> {
                    JsonObject message = new JsonObject(frame.textData());
                    //TODO check if message received
                });

                // send a message to an unbridged address
                JsonObject message = new JsonObject()
                        .put("type", "send")
                        .put("address", "in.bridgeWebServicetest.1.bridged")
                        .put("body", new JsonObject().put("source", "WSClient"));
                websocketHandler.writeFinalTextFrame(message.encode());
                requestResult.countDown();
                client.close();
            }, failureHandler -> {
                context.assertFalse(true, failureHandler.getMessage());
                failureHandler.printStackTrace();
                client.close();
            });
        });

        requestResult.awaitSuccess();
    }
    */



    /*
    @Test
    public void testInAuthenticatedRoleAccessToPublicBridgeAddress(TestContext context) {
        Async requestResult = context.async(4);

        // register an  address
        MessageConsumer<JsonObject> consumer = eventBus.consumer("in.bridgeWebServicetest.1.bridgedTest2");
        consumer.handler(new Handler<Message<JsonObject>>() {
            @Override
            public void handle(Message<JsonObject> message) {
                context.assertEquals("webUser", message.headers().get("origin"), "MessageImpl origin set");
                context.assertTrue(message.headers().get("userID").indexOf("@") > 0, "User id is provided via his email");
                context.assertEquals("[\"tester\",\"superTester\",\"authenticated\"]", message.headers().get("userRoles"), "User is associated with its roles");
                context.assertEquals("WSClient", message.body().getString("source"), "MessageImpl body is included");
                requestResult.countDown();
            }
        });


        JsonObject bridgingRequest = new JsonObject()
                .put("domain", "in.bridgeWebServicetest")
                .put("version", "1")
                .put("type", "bridgedTest2")
                .put("isInbound", true)
                .put("isOutbound", false)
                .put("authorities", new JsonObject());

        send("bridge", "1", "bridgeMissing", bridgingRequest, reply -> {
            context.assertTrue(reply.succeeded(), "Received reply for new bridge request");
            context.assertEquals(200, reply.statusCode(), "New bridge request has been processed");
            requestResult.countDown();

            vertx.executeBlocking(future -> {
                loginWithNewUser(null, future);
            }, result -> {

                HashMap<String, Object> loginMap = (HashMap<String, Object>) result.result();
                HttpClient client = (HttpClient) loginMap.get("client");
                String token = (String) loginMap.get("token");
                requestResult.countDown();

                client.websocket(8080, "localhost", "/eventbus/websocket", websocketHandler -> {
                    websocketHandler.frameHandler(frame -> {
                        JsonObject message = new JsonObject(frame.textData());
                    });
                    // send a message to the bridged address
                    JsonObject message = new JsonObject()
                            .put("type", "send")
                            .put("address", "in.bridgeWebServicetest.1.bridgedTest2")
                            .put("headers", new JsonObject().put("xSessionToken", token))
                            .put("body", new JsonObject().put("source", "WSClient"));
                    websocketHandler.writeFinalTextFrame(message.encode());
                    requestResult.countDown();
                });
            });
        });

        requestResult.awaitSuccess();
    }

    @Test
    public void testInAuthorizedRoleAccessToBridgedAddress(TestContext context) {
        Async requestResult = context.async(3);

        // register an  address
        MessageConsumer<JsonObject> consumer = eventBus.consumer("in.bridgeWebServicetest.1.bridgedTest3");
        consumer.handler(new Handler<Message<JsonObject>>() {
            @Override
            public void handle(Message<JsonObject> message) {

                context.assertEquals("webUser", message.headers().get("origin"), "MessageImpl origin set");
                context.assertTrue(message.headers().get("userID").indexOf("@") > 0, "User id is provided via his email");
                context.assertEquals("[\"tester\",\"authenticated\"]", message.headers().get("userRoles"), "User is associated with its roles");
                context.assertEquals("WSClient", message.body().getString("source"), "MessageImpl body is included");
                requestResult.countDown();
            }
        });


        JsonObject bridgingRequest = new JsonObject()
                .put("domain", "in.bridgeWebServicetest")
                .put("version", "1")
                .put("type", "bridgedTest3")
                .put("isInbound", true)
                .put("isOutbound", false)
                .put("authorities", new JsonObject().put("roles", new JsonArray().add("tester")));

        DeliveryOptions options = new DeliveryOptions()
                .addHeader("statusCode", "200")
                .addHeader("correlationID", "123456789")
                .addHeader("origin", "bridgeWebServiceTest1");

        send("bridge", "1", "bridgeMissing", bridgingRequest, reply -> {
            context.assertTrue(reply.succeeded(), "Received reply for new bridge request");
            context.assertEquals(200, reply.statusCode(), "New bridge request has been processed");
            requestResult.countDown();

            vertx.executeBlocking(future -> {
                loginWithNewUser(new JsonArray().add("tester"), future);
            }, result -> {
                HashMap<String, Object> loginMap = (HashMap<String, Object>) result.result();
                HttpClient client = (HttpClient) loginMap.get("client");
                String token = (String) loginMap.get("token");


                client.websocket(8080, "localhost", "/eventbus/websocket", websocketHandler -> {
                    websocketHandler.frameHandler(frame -> {
                        JsonObject message = new JsonObject(frame.textData());
                    });
                    // send a message to the bridged address
                    JsonObject message = new JsonObject()
                            .put("type", "send")
                            .put("address", "in.bridgeWebServicetest.1.bridgedTest3")
                            .put("headers", new JsonObject().put("xSessionToken", token))
                            .put("body", new JsonObject().put("source", "WSClient"));
                    websocketHandler.writeFinalTextFrame(message.encode());
                    requestResult.countDown();
                });
            });
        });
        requestResult.awaitSuccess();
    }

    @Test
    public void testInUnauthorizedRoleAccessToBridgedAddress(TestContext context) {
        Async requestResult = context.async(3);

        // register an  address
        MessageConsumer<JsonObject> consumer = eventBus.consumer("in.bridgeWebServicetest.1.bridgedTest4");
        consumer.handler(new Handler<Message<JsonObject>>() {
            @Override
            public void handle(Message<JsonObject> message) {

            }
        });


        JsonObject bridgingRequest = new JsonObject()
                .put("domain", "in.bridgeWebServicetest")
                .put("version", "1")
                .put("type", "bridgedTest4")
                .put("isInbound", true)
                .put("isOutbound", false)
                .put("authorities", new JsonObject().put("roles", new JsonArray().add("impossibleRoleToHave")));

        send("bridge", "1", "bridgeMissing", bridgingRequest, reply -> {
            context.assertTrue(reply.succeeded(), "Received reply for new bridge request");
            context.assertEquals(200, reply.statusCode(), "New bridge request has been processed");
            requestResult.countDown();

            vertx.executeBlocking(future -> {
                loginWithNewUser(null, future);
            }, result -> {
                HashMap<String, Object> loginMap = (HashMap<String, Object>) result.result();
                HttpClient client = (HttpClient) loginMap.get("client");
                String token = (String) loginMap.get("token");

                client.websocket(8080, "localhost", "/eventbus/websocket", websocketHandler -> {
                    websocketHandler.frameHandler(frame -> {
                        context.assertTrue(frame.isFinal());
                        JsonObject message = new JsonObject(frame.textData());
                        context.assertNotNull(message.getJsonObject("headers").getString("statusCode"), "Status code is returned.");
                        context.assertEquals("403", message.getJsonObject("headers").getString("statusCode"), "Status code is returned.");
                        context.assertNotNull(message.getJsonObject("body").getString("message"), "Non-null error message is send.");
                        requestResult.countDown();
                    });

                    // send a message to the bridged address
                    JsonObject message = new JsonObject()
                            .put("type", "send")
                            .put("address", "in.bridgeWebServicetest.1.bridgedTest4")
                            .put("replyAddress", "123456")
                            .put("headers", new JsonObject().put("xSessionToken", token))
                            .put("body", new JsonObject().put("source", "WSClient"));
                    websocketHandler.writeFinalTextFrame(message.encode());
                    requestResult.countDown();
                });
            });
        });
        requestResult.awaitSuccess();
    }*/

    //@Test
    public void testInBridgingMalformattedAddress(TestContext context) {
        Async requestResult = context.async(1);

        JsonObject bridgingRequest = new JsonObject()
                .put("domain", "bridgeWebServicetest")
                .put("version", "1")
                .put("type", "bridged")
                .put("isInbound", true)
                .put("isOutbound", false)
                .put("authorities", new JsonObject());

        send("bridge", "1", "bridgeMissing", bridgingRequest, reply -> {
            context.assertTrue(reply.succeeded(), "Received reply for new bridge request");
            context.assertEquals(400, reply.statusCode(), "New bridge request has been rejected");
            requestResult.countDown();
        });

        requestResult.awaitSuccess();
    }

    //@Test
    public void testInObsoleteBridge(TestContext context) {
        Async requestResult = context.async(3);

        JsonObject bridgingRequest = new JsonObject()
                .put("domain", "in.bridgeWebServiceobsoletetest")
                .put("version", "1")
                .put("type", "bridged")
                .put("isInbound", true)
                .put("isOutbound", false)
                .put("authorities", new JsonObject());

        send("bridge", "1", "bridgeMissing", bridgingRequest, reply -> {
                context.assertTrue(reply.succeeded(), "Received reply for new bridge request");
                context.assertEquals(200, reply.statusCode(), "New bridge request has been processed");
                requestResult.countDown();

                send("bridge", "1", "obsoleteBridge", new JsonObject()
                        .put("domain", "in.bridgeWebServiceobsoletetest")
                        .put("version", "1")
                        .put("type", "bridged"), obsoleteReply-> {

                    context.assertTrue(obsoleteReply.succeeded(), "Received reply for obsolete bridge request");
                    context.assertEquals(200, obsoleteReply.statusCode(), "Obsolete bridge request has been processed");
                    requestResult.countDown();

                    send("bridge", "1", "obsoleteBridge", new JsonObject()
                            .put("domain", "in.bridgeWebServiceobsoletetest")
                            .put("version", "1")
                            .put("type", "bridged"), duplicateObsoleteReply-> {

                        context.assertTrue(duplicateObsoleteReply.succeeded(), "Received reply for duplicate obsolete bridge request");
                        context.assertEquals(200, duplicateObsoleteReply.statusCode(), "Duplicate obsolete bridge request has been processed");
                        requestResult.countDown();
                    });
                });
            });

        requestResult.awaitSuccess();
    }

    //@Test
    public void testClientLoggingOnBackend(TestContext context) {
        Async requestResult = context.async(2);

        Boolean[] hasReceivedWarnMessage = new Boolean[1];
        hasReceivedWarnMessage[0] = false;

        // register an  address
        MessageConsumer<JsonObject> consumer = eventBus.consumer("in.logging.1.log");
        consumer.handler(new Handler<Message<JsonObject>>() {
            @Override
            public void handle(Message<JsonObject> message) {
                if(!hasReceivedWarnMessage[0] && "Test logging message".equalsIgnoreCase(message.body().getString("message"))) {
                    context.assertEquals(2, message.body().getInteger("level"));
                    hasReceivedWarnMessage[0] = true;
                    requestResult.countDown();
                }
            }
        });

        JsonObject logRequest = new JsonObject()
                .put("domain", "in.logging")
                .put("version", "1")
                .put("type", "log")
                .put("message", "Test logging message")
                .put("level", 2);

        // send multiple ones as WebService has its own consumer and message get divided among distributors
        send("in.logging", "1", "log", logRequest, reply -> {});
        send("in.logging", "1", "log", logRequest, reply -> {});
        send("in.logging", "1", "log", logRequest, reply -> {});
        requestResult.countDown();

        requestResult.awaitSuccess();
    }

    /*
    private void getSessionWithNewUser(JsonArray roles, Future<Object> future) {

    }

    private void loginWithNewUser(JsonArray roles, Future<Object> future) {
        vertx.executeBlocking(loginFuture -> {
            getNewUser(roles, loginFuture);
        }, result -> {
            JsonObject user = (JsonObject) result.result();
            HttpClientOptions clientOptions = new HttpClientOptions()
                    .setSsl(false)
                    .setTrustAll(true);
            HttpClient client = vertx.createHttpClient(clientOptions);
            HttpClientRequest request = client.post(8080,"localhost", "/email/login", response -> {
                response.bodyHandler(totalBuffer -> {
                    JsonObject responseData = totalBuffer.toJsonObject();
                    HashMap<String,Object> loginMap = new HashMap<>();
                    loginMap.put("token", responseData.getString("xSessionToken"));
                    loginMap.put("client", client);
                    future.complete(loginMap);
                });
            });
            String body = new JsonObject().put("email", user.getString("userID")).put("password", user.getString("password")).encode();
            request.putHeader(HttpHeaders.CONTENT_TYPE, "application/json").end(body);
        });
    }*/

    private void createAuthenticatedSessionWithNewUser(JsonArray roles, Handler<WebSocket> webSocketHandler) {
        String email = serviceName + userCounter++ + "@test.de";

        if(roles == null) {
            roles = new JsonArray();
        }

        JsonObject authConfig = new JsonObject()
                .put("userEmail", email)
                .put("hashFactory", "PBKDF2WithHmacSHA512")
                .put("hashSalt", "testSaltToping")
                .put("hashIterations", 10)
                .put("hashLength", 256);

        // generate a really mean random password
        int randomBoundary = 0;
        while(randomBoundary == 0)
            randomBoundary = new Random().nextInt(256);

        byte[] array = new byte[randomBoundary];
        new Random().nextBytes(array);
        String password = new String(array, Charset.forName("UTF-8"));
        String passwordHash = EncryptionUtils.hash(password,
                authConfig.getString("hashFactory"),
                authConfig.getString("hashSalt"),
                authConfig.getInteger("hashIterations"),
                authConfig.getInteger("hashLength"));
        authConfig.put("passwordHash", passwordHash);

        DeliveryOptions options = new DeliveryOptions()
                .addHeader("statusCode", "200")
                .addHeader("correlationID", "858518121")
                .addHeader("origin", "bridgeWebService1Test");

        JsonObject user = new JsonObject().put("userID", email).put("userRoles", roles).put("authType", "email").put("authConfig",authConfig);

        send("users", "1", "userRegisterRequested", user, reply -> {
            if(reply.failed()) {
                reply.cause().printStackTrace();
                return;
            }
            else if(reply.statusCode() != 200) {
                System.err.println(reply.statusCode() + ":" + reply.getMessage());
                return;
            }


        });
    }

    private void createNewUser(JsonArray roles, Future<Object> future) {
        int randomNumber = ThreadLocalRandom.current().nextInt(1, 5001);

        String email = "test" + randomNumber +"@test.de";

        JsonObject authConfig = new JsonObject()
                .put("userEmail", email)
                .put("hashFactory", "PBKDF2WithHmacSHA512")
                .put("hashSalt", "testSaltToping")
                .put("hashIterations", 10)
                .put("hashLength", 256);

        if(roles == null) {
            roles = new JsonArray().add("tester").add("superTester");
        }

        // generate a really mean random password
        int randomBoundary = 0;
        while(randomBoundary == 0)
            randomBoundary = new Random().nextInt(256);

        byte[] array = new byte[randomBoundary];
        new Random().nextBytes(array);
        String password = new String(array, Charset.forName("UTF-8"));

        String passwordHash = EncryptionUtils.hash(password,
                authConfig.getString("hashFactory"),
                authConfig.getString("hashSalt"),
                authConfig.getInteger("hashIterations"),
                authConfig.getInteger("hashLength"));
        authConfig.put("passwordHash", passwordHash);

        DeliveryOptions options = new DeliveryOptions()
                .addHeader("statusCode", "200")
                .addHeader("correlationID", "858518121")
                .addHeader("origin", "bridgeWebService1Test");

        JsonObject user = new JsonObject().put("userID", email).put("userRoles", roles).put("authType", "email").put("authConfig",authConfig);

        send("users", "1", "userRegisterRequested", user, reply -> {
            if(reply.failed()) {
                reply.cause().printStackTrace();
                future.fail(reply.cause());
            }
            else if(reply.statusCode() != 200) {
                System.err.println(reply.statusCode() + ":" + reply.getMessage());
                future.fail(reply.getMessage());
            }
            else {

                userAccounts.put("email", user);
                future.complete(reply.getBodyAsJsonObject().put("password", password).put("passwordHash", passwordHash).put("email", email));
            }
        });
    }

    private void deleteUser(String id, Future<Object> future) {

        send("users", "1", "userBecameObsolete",new JsonObject().put("id", id), delReply -> {
            if(delReply.failed() || delReply.statusCode() != 200) {
                future.fail(delReply.cause());
            }
            else {
                userAccounts.remove(id);
                future.complete(id);
            }
        });
    }


}

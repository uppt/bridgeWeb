package de.maltebehrendt.uppt;

import de.maltebehrendt.uppt.events.Impl.MessageImpl;
import de.maltebehrendt.uppt.junit.AbstractTest;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.net.JksOptions;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.lang.invoke.MethodHandles;
import java.util.Map;

/**
 * Created by malte on 04.02.17.
 */
public class PublicFilesRetrieval1Test extends AbstractTest {
    private static Vertx vertx = null;
    private static String webRootPath = null;

    @BeforeClass
    public static void setup(TestContext context) {
        Map<String, Object> map = setupTestVertxAndDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
        vertx = (Vertx) map.get("vertx");
        webRootPath = map.get("baseDirectory") + File.separator + "storage" + File.separator + "webroot";
    }

    @AfterClass
    public static void teardown(TestContext context) {
        tearDownTestVertxAndCleanupDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
        if(vertx.fileSystem().existsBlocking(webRootPath)) {
           // vertx.fileSystem().deleteRecursiveBlocking(webRootPath, true);
        }
    }

    @Test
    public void testIfPublicFilesAvailableOnStorage(TestContext context) {
        // check that the files exist on storage
        File targetDirectory = new File(webRootPath + File.separator + "noop" + File.separator + "1");
        context.assertTrue(targetDirectory.exists() && targetDirectory.isDirectory(), "Target path exists");
        context.assertEquals(1, targetDirectory.listFiles().length, "Target path contains one entry");

        File folder = targetDirectory.listFiles()[0];
        context.assertEquals("vertx", folder.getName(), "Target entry name is correct");
        context.assertEquals(4, folder.listFiles().length, "Target entry content count is correct");
    }

    @Test
    public void testIfPublicFilesAvailableViaHTTP(TestContext context) {
        Async testResult = context.async(2);

        // check that resources are available via HTTPS - or in this case a least one file :-)
        JksOptions jksOptions =  new JksOptions().setPath(configDirectory + "services/bridgeWebService/sslKeyStore.jks").setPassword("strongPassword");
        HttpClientOptions clientOptions = new HttpClientOptions()
                .setSsl(false)
                .setTrustAll(true);
        HttpClient client = vertx.createHttpClient(clientOptions);

        client.getNow(8080, "localhost", "/public/noop/1/vertx/vertx-stack.json", responseHandler -> {
            context.assertEquals(200, responseHandler.statusCode(), "Get request was successful");

            responseHandler.bodyHandler(body -> {
                String fileContent = body.toString();
                context.assertEquals(18320, fileContent.length(), "Returned file has the correct length");
                testResult.countDown();
            });

        });

        client.getNow(8080, "localhost", "/public/noop/1/vertx/vertx-stack.non.json", responseHandler -> {
            context.assertEquals(404, responseHandler.statusCode(), "Requesting non-existent files causes a 404");
            testResult.countDown();
        });

        testResult.awaitSuccess();
        client.close();
    }

    @Test
    public void testPublicFilesMissingAliasForFolder(TestContext context) {
        Async testResult = context.async(2);

        send("publicFiles", "1","missingAlias", new JsonObject()
                .put("path", "/public/noop/1/vertx/*")
                .put("alias", "/noop/1/vertx/*"), reply -> {
            context.assertTrue(reply.succeeded());
            context.assertEquals(200, reply.statusCode());
            testResult.countDown();

            // check if file available via HTTP
            JksOptions jksOptions =  new JksOptions().setPath(configDirectory + "services/bridgeWebService/sslKeyStore.jks").setPassword("strongPassword");
            HttpClientOptions clientOptions = new HttpClientOptions()
                    .setSsl(false)
                    .setTrustAll(true);
            HttpClient client = vertx.createHttpClient(clientOptions);

            client.getNow(8080, "localhost", "/noop/1/vertx/vertx-stack.json", responseHandler -> {
                context.assertEquals(200, responseHandler.statusCode(), "Get request was successful");

                responseHandler.bodyHandler(body -> {
                    String fileContent = body.toString();
                    context.assertEquals(18320, fileContent.length(), "Returned file has the correct length");
                    testResult.countDown();
                });

            });
        });
        testResult.awaitSuccess();
    }

    @Test
    public void testPublicFilesAliasForFolderFromConfig(TestContext context) {
        Async testResult = context.async(1);

        // check if file available via HTTP
        JksOptions jksOptions =  new JksOptions().setPath(configDirectory + "services/bridgeWebService/sslKeyStore.jks").setPassword("strongPassword");
        HttpClientOptions clientOptions = new HttpClientOptions()
                .setSsl(false)
                .setTrustAll(true);
        HttpClient client = vertx.createHttpClient(clientOptions);

        client.getNow(8080, "localhost", "/vertx/vertx-stack.json", responseHandler -> {
            context.assertEquals(200, responseHandler.statusCode(), "Get request was successful");

            responseHandler.bodyHandler(body -> {
                String fileContent = body.toString();
                context.assertEquals(18320, fileContent.length(), "Returned file has the correct length");
                testResult.countDown();
            });

        });
        testResult.awaitSuccess();
    }

    @Test
    public void testPublicFilesRegexAliasForFileFromConfig(TestContext context) {
        Async testResult = context.async(1);

        // check if file available via HTTP
        JksOptions jksOptions =  new JksOptions().setPath(configDirectory + "services/bridgeWebService/sslKeyStore.jks").setPassword("strongPassword");
        HttpClientOptions clientOptions = new HttpClientOptions()
                .setSsl(false)
                .setTrustAll(true);
        HttpClient client = vertx.createHttpClient(clientOptions);

        client.getNow(8080, "localhost", "/66232/vertx/vertx-stack.json", responseHandler -> {
            context.assertEquals(200, responseHandler.statusCode(), "Get request was successful");

            responseHandler.bodyHandler(body -> {
                String fileContent = body.toString();
                context.assertEquals(18320, fileContent.length(), "Returned file has the correct length");
                testResult.countDown();
            });

        });
        testResult.awaitSuccess();
    }

    @Test
    public void testPublicFilesAliasWithNotificationForFileFromConfig(TestContext context) {
        Async testResult = context.async(2);

        MessageConsumer<JsonObject> consumer = eventBus.consumer("test.1.missingAliasWithNotificationForStackFile", msg -> {
            de.maltebehrendt.uppt.events.Message message = new MessageImpl(msg);
            // startup of a service complete
            context.assertEquals(200, message.statusCode());
            JsonObject body = message.getBodyAsJsonObject();

            context.assertEquals("/noopvertx/345902/1/vertx/vertx-stack.json", body.getString("alias"));
            context.assertEquals("/public/noop/1/vertx/vertx-stack.json", body.getString("path"));
            context.assertEquals("127.0.0.1", body.getString("remoteHost"));
            context.assertNotNull(body.getJsonObject("parameters"));
            context.assertEquals("345902", body.getJsonObject("parameters").getString("track_id"));
            context.assertNotNull(body.getJsonObject("headers"));

            testResult.countDown();
        });


        // check if file available via HTTP
        JksOptions jksOptions =  new JksOptions().setPath(configDirectory + "services/bridgeWebService/sslKeyStore.jks").setPassword("strongPassword");
        HttpClientOptions clientOptions = new HttpClientOptions()
                .setSsl(false)
                .setTrustAll(true);
        HttpClient client = vertx.createHttpClient(clientOptions);
        client.getNow(8080, "localhost", "/noopvertx/345902/1/vertx/vertx-stack.json", responseHandler -> {
            context.assertEquals(200, responseHandler.statusCode(), "Get request was successful");

            responseHandler.bodyHandler(body -> {
                String fileContent = body.toString();
                context.assertEquals(18320, fileContent.length(), "Returned file has the correct length");
                testResult.countDown();
            });

        });

        testResult.awaitSuccess();
    }

    @Test
    public void testPublicFilesAliasWithClearanceForFileFromConfig(TestContext context) {
        Async testResult = context.async(2);

        MessageConsumer<JsonObject> consumer = eventBus.consumer("test.1.missingAliasWithClearanceForStackFile", msg -> {
            de.maltebehrendt.uppt.events.Message message = new MessageImpl(msg);
            // startup of a service complete
            context.assertEquals(200, message.statusCode());
            JsonObject body = message.getBodyAsJsonObject();

            context.assertEquals("/noopcorrelation/345678/vertx/vertx-stack.json", body.getString("alias"));
            context.assertEquals("/public/noop/1/vertx/vertx-stack.json", body.getString("path"));
            context.assertEquals("127.0.0.1", body.getString("remoteHost"));
            context.assertNotNull(body.getJsonObject("parameters"));
            context.assertEquals("345678", body.getJsonObject("parameters").getString("correlation_id"));
            context.assertNotNull(body.getJsonObject("headers"));

            message.reply(200);

            testResult.countDown();
        });


        // check if file available via HTTP
        JksOptions jksOptions =  new JksOptions().setPath(configDirectory + "services/bridgeWebService/sslKeyStore.jks").setPassword("strongPassword");
        HttpClientOptions clientOptions = new HttpClientOptions()
                .setSsl(false)
                .setTrustAll(true);
        HttpClient client = vertx.createHttpClient(clientOptions);
        client.getNow(8080, "localhost", "/noopcorrelation/345678/vertx/vertx-stack.json", responseHandler -> {
            context.assertEquals(200, responseHandler.statusCode(), "Get request was successful");

            responseHandler.bodyHandler(body -> {
                String fileContent = body.toString();
                context.assertEquals(18320, fileContent.length(), "Returned file has the correct length");
                testResult.countDown();
            });

        });

        testResult.awaitSuccess();
    }

    @Test
    public void testPublicFilesMissingAliasWithDynamicPathForFileFromConfig(TestContext context) {
        Async testResult = context.async(4);

        MessageConsumer<JsonObject> consumer = eventBus.consumer("test.1.missingAliasWithDynamicPathForStackFile", msg -> {
            de.maltebehrendt.uppt.events.Message message = new MessageImpl(msg);
            context.assertEquals(200, message.statusCode());
            JsonObject body = message.getBodyAsJsonObject();

            context.assertEquals("127.0.0.1", body.getString("remoteHost"));
            context.assertNotNull(body.getJsonObject("parameters"));
            context.assertNotNull(body.getJsonObject("headers"));

            if("46875".equals(body.getJsonObject("parameters").getString("dyn_id"))) {
                message.reply(200, new JsonObject().put("path", "/public/noop/1/vertx/vertx-stack.json"));
            }
            else if("5875413".equals(body.getJsonObject("parameters").getString("dyn_id"))) {
                message.reply(200, new JsonObject().put("path", "/public/noop/1/vertx/conf/logging.properties"));
            }
            else {
                context.assertTrue(false, "dyn_id unknown");
            }

            testResult.countDown();
        });


        // check if file available via HTTP
        JksOptions jksOptions =  new JksOptions().setPath(configDirectory + "services/bridgeWebService/sslKeyStore.jks").setPassword("strongPassword");
        HttpClientOptions clientOptions = new HttpClientOptions()
                .setSsl(false)
                .setTrustAll(true);
        HttpClient client = vertx.createHttpClient(clientOptions);
        client.getNow(8080, "localhost", "/noopDyn2/46875/confirm.html", responseHandler -> {
            context.assertEquals(200, responseHandler.statusCode(), "Get request was successful");

            responseHandler.bodyHandler(body -> {
                String fileContent = body.toString();
                context.assertEquals(18320, fileContent.length(), "Returned file has the correct length");
                testResult.countDown();
            });

        });

        client.getNow(8080, "localhost", "/noopDyn2/5875413/confirm.html", responseHandler -> {
            context.assertEquals(200, responseHandler.statusCode(), "Get request was successful");

            responseHandler.bodyHandler(body -> {
                String fileContent = body.toString();
                context.assertEquals(1186, fileContent.length(), "Returned file has the correct length");
                testResult.countDown();
            });

        });
        testResult.awaitSuccess();
    }

    @Test
    public void testPublicFilesMissingAliasForFile(TestContext context) {
        Async testResult = context.async(2);

        send("publicFiles", "1", "missingAlias", new JsonObject()
                .put("path", "/public/noop/1/vertx/vertx-stack.json")
                .put("alias", "/noop/1/vertx-stack.json"), reply -> {
            context.assertTrue(reply.succeeded());
            context.assertEquals(200, reply.statusCode());
            testResult.countDown();

            // check if file available via HTTP
            JksOptions jksOptions =  new JksOptions().setPath(configDirectory + "services/bridgeWebService/sslKeyStore.jks").setPassword("strongPassword");
            HttpClientOptions clientOptions = new HttpClientOptions()
                    .setSsl(false)
                    .setTrustAll(true);
            HttpClient client = vertx.createHttpClient(clientOptions);

            client.getNow(8080, "localhost", "/noop/1/vertx-stack.json", responseHandler -> {
                context.assertEquals(200, responseHandler.statusCode(), "Get request was successful");

                responseHandler.bodyHandler(body -> {
                    String fileContent = body.toString();
                    context.assertEquals(18320, fileContent.length(), "Returned file has the correct length");
                    testResult.countDown();
                });

            });
        });
        testResult.awaitSuccess();
    }

    @Test
    public void testPublicFilesMissingRegexAliasForFile(TestContext context) {
        Async testResult = context.async(2);

        send("publicFiles", "1", "missingRegexAlias", new JsonObject()
                .put("path", "/public/noop/1/vertx/vertx-stack.json")
                .put("aliasRegexPrefix", "/\\d{5}/2/")
                .put("aliasMarker", "vertx/vertx-stack.json"), reply -> {
            context.assertTrue(reply.succeeded());
            context.assertEquals(200, reply.statusCode());
            testResult.countDown();

            // check if file available via HTTP
            JksOptions jksOptions =  new JksOptions().setPath(configDirectory + "services/bridgeWebService/sslKeyStore.jks").setPassword("strongPassword");
            HttpClientOptions clientOptions = new HttpClientOptions()
                    .setSsl(false)
                    .setTrustAll(true);
            HttpClient client = vertx.createHttpClient(clientOptions);

            client.getNow(8080, "localhost", "/66232/2/vertx/vertx-stack.json", responseHandler -> {
                context.assertEquals(200, responseHandler.statusCode(), "Get request was successful");

                responseHandler.bodyHandler(body -> {
                    String fileContent = body.toString();
                    context.assertEquals(18320, fileContent.length(), "Returned file has the correct length");
                    testResult.countDown();
                });
            });
        });
        testResult.awaitSuccess();
    }

    @Test
    public void testPublicFilesMissingRegexAliasForFolder(TestContext context) {
        Async testResult = context.async(2);

        send("publicFiles", "1", "missingRegexAlias", new JsonObject()
                .put("path", "/public/noop/1/vertx/*")
                .put("aliasRegexPrefix", "/noop/\\d{5}/2/")
                .put("aliasMarker", "vertx/.*"), reply -> {
            context.assertTrue(reply.succeeded());
            context.assertEquals(200, reply.statusCode());
            testResult.countDown();

            // check if file available via HTTP
            JksOptions jksOptions =  new JksOptions().setPath(configDirectory + "services/bridgeWebService/sslKeyStore.jks").setPassword("strongPassword");
            HttpClientOptions clientOptions = new HttpClientOptions()
                    .setSsl(false)
                    .setTrustAll(true);
            HttpClient client = vertx.createHttpClient(clientOptions);

            client.getNow(8080, "localhost", "/noop/66232/2/vertx/vertx-stack.json", responseHandler -> {
                context.assertEquals(200, responseHandler.statusCode(), "Get request was successful");

                responseHandler.bodyHandler(body -> {
                    String fileContent = body.toString();
                    context.assertEquals(18320, fileContent.length(), "Returned file has the correct length");
                    testResult.countDown();
                });

            });
        });
        testResult.awaitSuccess();
    }

    @Test
    public void testPublicFilesMissingAliasWithNotificationForFolder(TestContext context) {
        Async testResult = context.async(3);

        MessageConsumer<JsonObject> consumer = eventBus.consumer("test.1.missingAliasWithNotificationForFolder", msg -> {
            de.maltebehrendt.uppt.events.Message message = new MessageImpl(msg);
            // startup of a service complete
            context.assertEquals(200, message.statusCode());

            JsonObject body = message.getBodyAsJsonObject();

            context.assertEquals("/noopTracked/645902/2/vertx/vertx-stack.json", body.getString("alias"));
            context.assertEquals("/public/noop/1/vertx/vertx-stack.json", body.getString("path"));
            context.assertEquals("127.0.0.1", body.getString("remoteHost"));
            context.assertNotNull(body.getJsonObject("parameters"));
            context.assertEquals("645902", body.getJsonObject("parameters").getString("track_id"));
            context.assertNotNull(body.getJsonObject("headers"));

            testResult.countDown();
        });

        send("publicFiles", "1", "missingAliasWithNotification", new JsonObject()
                .put("path", "/public/noop/1/vertx/*")
                .put("aliasParameterPrefix", "/noopTracked/:track_id/2/")
                .put("aliasMarker", "vertx/*")
                .put("domain", "test")
                .put("version", "1")
                .put("type", "missingAliasWithNotificationForFolder"), reply -> {
            context.assertTrue(reply.succeeded());
            context.assertEquals(200, reply.statusCode());
            testResult.countDown();

            // check if file available via HTTP
            JksOptions jksOptions =  new JksOptions().setPath(configDirectory + "services/bridgeWebService/sslKeyStore.jks").setPassword("strongPassword");
            HttpClientOptions clientOptions = new HttpClientOptions()
                    .setSsl(false)
                    .setTrustAll(true);
            HttpClient client = vertx.createHttpClient(clientOptions);
            client.getNow(8080, "localhost", "/noopTracked/645902/2/vertx/vertx-stack.json", responseHandler -> {
                context.assertEquals(200, responseHandler.statusCode(), "Get request was successful");

                responseHandler.bodyHandler(body -> {
                    String fileContent = body.toString();
                    context.assertEquals(18320, fileContent.length(), "Returned file has the correct length");
                    testResult.countDown();
                });

            });
        });

        testResult.awaitSuccess();
    }

    @Test
    public void testPublicFilesMissingAliasWithNotificationForFile(TestContext context) {
        Async testResult = context.async(3);

        MessageConsumer<JsonObject> consumer = eventBus.consumer("test.1.missingAliasWithNotificationForFile", msg -> {
            de.maltebehrendt.uppt.events.Message message = new MessageImpl(msg);
            // startup of a service complete
            context.assertEquals(200, message.statusCode());
            JsonObject body = message.getBodyAsJsonObject();

            context.assertEquals("/noopTrack/945902/2/vertx/vertx-stack.json", body.getString("alias"));
            context.assertEquals("/public/noop/1/vertx/vertx-stack.json", body.getString("path"));
            context.assertEquals("127.0.0.1", body.getString("remoteHost"));
            context.assertNotNull(body.getJsonObject("parameters"));
            context.assertEquals("945902", body.getJsonObject("parameters").getString("track_id"));
            context.assertNotNull(body.getJsonObject("headers"));

            testResult.countDown();
        });

        send("publicFiles", "1", "missingAliasWithNotification", new JsonObject()
                .put("path", "/public/noop/1/vertx/vertx-stack.json")
                .put("aliasParameterPrefix", "/noopTrack/:track_id/2/")
                .put("aliasMarker", "vertx/vertx-stack.json")
                .put("domain", "test")
                .put("version", "1")
                .put("type", "missingAliasWithNotificationForFile"), reply -> {
            context.assertTrue(reply.succeeded());
            context.assertEquals(200, reply.statusCode());
            testResult.countDown();

            // check if file available via HTTP
            JksOptions jksOptions =  new JksOptions().setPath(configDirectory + "services/bridgeWebService/sslKeyStore.jks").setPassword("strongPassword");
            HttpClientOptions clientOptions = new HttpClientOptions()
                    .setSsl(false)
                    .setTrustAll(true);
            HttpClient client = vertx.createHttpClient(clientOptions);
            client.getNow(8080, "localhost", "/noopTrack/945902/2/vertx/vertx-stack.json", responseHandler -> {
                context.assertEquals(200, responseHandler.statusCode(), "Get request was successful");

                responseHandler.bodyHandler(body -> {
                    String fileContent = body.toString();
                    context.assertEquals(18320, fileContent.length(), "Returned file has the correct length");
                    testResult.countDown();
                });

            });
        });
        testResult.awaitSuccess();
    }

    @Test
    public void testPublicFilesMissingAliasWithDynamicPath(TestContext context) {
        Async testResult = context.async(5);

        MessageConsumer<JsonObject> consumer = eventBus.consumer("test.1.missingAliasWithDynamicPathStack", msg -> {
            de.maltebehrendt.uppt.events.Message message = new MessageImpl(msg);
            context.assertEquals(200, message.statusCode());
            JsonObject body = message.getBodyAsJsonObject();

            context.assertEquals("127.0.0.1", body.getString("remoteHost"));
            context.assertNotNull(body.getJsonObject("parameters"));
            context.assertNotNull(body.getJsonObject("headers"));

            if("46875".equals(body.getJsonObject("parameters").getString("dyn_id"))) {
                message.reply(200, new JsonObject().put("path", "/public/noop/1/vertx/vertx-stack.json"));
            }
            else if("5875413".equals(body.getJsonObject("parameters").getString("dyn_id"))) {
                message.reply(200, new JsonObject().put("path", "/public/noop/1/vertx/conf/logging.properties"));
            }
            else {
                context.assertTrue(false, "dyn_id unknown");
            }

            testResult.countDown();
        });

        send("publicFiles", "1", "missingAliasWithDynamicPath", new JsonObject()
                .put("aliasParameterPrefix", "/noopDyn/:dyn_id/")
                .put("aliasMarker", "stack.json")
                .put("domain", "test")
                .put("version", "1")
                .put("type", "missingAliasWithDynamicPathStack"), reply -> {
            context.assertTrue(reply.succeeded());
            context.assertEquals(200, reply.statusCode());
            testResult.countDown();

            // check if file available via HTTP
            JksOptions jksOptions =  new JksOptions().setPath(configDirectory + "services/bridgeWebService/sslKeyStore.jks").setPassword("strongPassword");
            HttpClientOptions clientOptions = new HttpClientOptions()
                    .setSsl(false)
                    .setTrustAll(true);
            HttpClient client = vertx.createHttpClient(clientOptions);
            client.getNow(8080, "localhost", "/noopDyn/46875/stack.json", responseHandler -> {
                context.assertEquals(200, responseHandler.statusCode(), "Get request was successful");

                responseHandler.bodyHandler(body -> {
                    String fileContent = body.toString();
                    context.assertEquals(18320, fileContent.length(), "Returned file has the correct length");
                    testResult.countDown();
                });

            });

            client.getNow(8080, "localhost", "/noopDyn/5875413/stack.json", responseHandler -> {
                context.assertEquals(200, responseHandler.statusCode(), "Get request was successful");

                responseHandler.bodyHandler(body -> {
                    String fileContent = body.toString();
                    context.assertEquals(1186, fileContent.length(), "Returned file has the correct length");
                    testResult.countDown();
                });

            });
        });
        testResult.awaitSuccess();
    }

    @Test
    public void testPublicFilesMissingAliasWithClearanceForFileConfirmed(TestContext context) {
        Async testResult = context.async(3);

        MessageConsumer<JsonObject> consumer = eventBus.consumer("test.1.missingAliasWithClearanceForFileConfirmed", msg -> {
            de.maltebehrendt.uppt.events.Message message = new MessageImpl(msg);
            context.assertEquals(200, message.statusCode());
            JsonObject body = message.getBodyAsJsonObject();

            context.assertEquals("/noopClearance/98752668/vertx/vertx-stack.json", body.getString("alias"));
            context.assertEquals("/public/noop/1/vertx/vertx-stack.json", body.getString("path"));
            context.assertEquals("127.0.0.1", body.getString("remoteHost"));
            context.assertNotNull(body.getJsonObject("parameters"));
            context.assertEquals("98752668", body.getJsonObject("parameters").getString("clearance_id"));
            context.assertNotNull(body.getJsonObject("headers"));

            message.reply(200);
            testResult.countDown();
        });

        send("publicFiles", "1", "missingAliasWithClearance", new JsonObject()
                .put("path", "/public/noop/1/vertx/vertx-stack.json")
                .put("aliasParameterPrefix", "/noopClearance/:clearance_id/")
                .put("aliasMarker", "vertx/vertx-stack.json")
                .put("domain", "test")
                .put("version", "1")
                .put("type", "missingAliasWithClearanceForFileConfirmed"), reply -> {
            context.assertTrue(reply.succeeded());
            context.assertEquals(200, reply.statusCode());
            testResult.countDown();

            // check if file available via HTTP
            JksOptions jksOptions =  new JksOptions().setPath(configDirectory + "services/bridgeWebService/sslKeyStore.jks").setPassword("strongPassword");
            HttpClientOptions clientOptions = new HttpClientOptions()
                    .setSsl(false)
                    .setTrustAll(true);
            HttpClient client = vertx.createHttpClient(clientOptions);
            client.getNow(8080, "localhost", "/noopClearance/98752668/vertx/vertx-stack.json", responseHandler -> {
                context.assertEquals(200, responseHandler.statusCode(), "Get request was successful");

                responseHandler.bodyHandler(body -> {
                    String fileContent = body.toString();
                    context.assertEquals(18320, fileContent.length(), "Returned file has the correct length");
                    testResult.countDown();
                });

            });
        });
        testResult.awaitSuccess();
    }

    @Test
    public void testPublicFilesMissingAliasWithClearanceForFileDenied(TestContext context) {
        Async testResult = context.async(3);

        MessageConsumer<JsonObject> consumer = eventBus.consumer("test.1.missingAliasWithClearanceForFileDenied", msg -> {
            de.maltebehrendt.uppt.events.Message message = new MessageImpl(msg);
            context.assertEquals(200, message.statusCode());
            JsonObject body = message.getBodyAsJsonObject();

            context.assertEquals("/noopClearance2/98756425/vertx/vertx-stack.json", body.getString("alias"));
            context.assertEquals("/public/noop/1/vertx/vertx-stack.json", body.getString("path"));
            context.assertEquals("127.0.0.1", body.getString("remoteHost"));
            context.assertNotNull(body.getJsonObject("parameters"));
            context.assertEquals("98756425", body.getJsonObject("parameters").getString("clearance_id"));
            context.assertNotNull(body.getJsonObject("headers"));

            message.reply(403);
            testResult.countDown();
        });

        send("publicFiles", "1", "missingAliasWithClearance", new JsonObject()
                .put("path", "/public/noop/1/vertx/vertx-stack.json")
                .put("aliasParameterPrefix", "/noopClearance2/:clearance_id/")
                .put("aliasMarker", "vertx/vertx-stack.json")
                .put("domain", "test")
                .put("version", "1")
                .put("type", "missingAliasWithClearanceForFileDenied"), reply -> {
            context.assertTrue(reply.succeeded());
            context.assertEquals(200, reply.statusCode());
            testResult.countDown();

            // check if file available via HTTP
            JksOptions jksOptions =  new JksOptions().setPath(configDirectory + "services/bridgeWebService/sslKeyStore.jks").setPassword("strongPassword");
            HttpClientOptions clientOptions = new HttpClientOptions()
                    .setSsl(false)
                    .setTrustAll(true);
            HttpClient client = vertx.createHttpClient(clientOptions);
            client.getNow(8080, "localhost", "/noopClearance2/98756425/vertx/vertx-stack.json", responseHandler -> {
                context.assertEquals(403, responseHandler.statusCode(), "Get request was denied");
                testResult.countDown();
            });
        });
        testResult.awaitSuccess();
    }

    @Test
    public void testPublicFilesMissingAliasWithClearanceForFileNotFound(TestContext context) {
        Async testResult = context.async(3);

        MessageConsumer<JsonObject> consumer = eventBus.consumer("test.1.missingAliasWithClearanceForFileNotFound", msg -> {
            de.maltebehrendt.uppt.events.Message message = new MessageImpl(msg);
            context.assertEquals(200, message.statusCode());
            JsonObject body = message.getBodyAsJsonObject();

            context.assertEquals("/noopClearance3/456789223/vertx/vertx-stack.json", body.getString("alias"));
            context.assertEquals("/public/noop/1/vertx/vertx-stack.json", body.getString("path"));
            context.assertEquals("127.0.0.1", body.getString("remoteHost"));
            context.assertNotNull(body.getJsonObject("parameters"));
            context.assertEquals("456789223", body.getJsonObject("parameters").getString("clearance_id"));
            context.assertNotNull(body.getJsonObject("headers"));

            message.reply(404);
            testResult.countDown();
        });

        send("publicFiles", "1", "missingAliasWithClearance", new JsonObject()
                .put("path", "/public/noop/1/vertx/vertx-stack.json")
                .put("aliasParameterPrefix", "/noopClearance3/:clearance_id/")
                .put("aliasMarker", "vertx/vertx-stack.json")
                .put("domain", "test")
                .put("version", "1")
                .put("type", "missingAliasWithClearanceForFileNotFound"), reply -> {
            context.assertTrue(reply.succeeded());
            context.assertEquals(200, reply.statusCode());
            testResult.countDown();

            // check if file available via HTTP
            JksOptions jksOptions =  new JksOptions().setPath(configDirectory + "services/bridgeWebService/sslKeyStore.jks").setPassword("strongPassword");
            HttpClientOptions clientOptions = new HttpClientOptions()
                    .setSsl(false)
                    .setTrustAll(true);
            HttpClient client = vertx.createHttpClient(clientOptions);
            client.getNow(8080, "localhost", "/noopClearance3/456789223/vertx/vertx-stack.json", responseHandler -> {
                context.assertEquals(404, responseHandler.statusCode(), "Get request was denied");
                testResult.countDown();
            });
        });
        testResult.awaitSuccess();
    }

    //TODO: check conditions like overwriting and stuff
}

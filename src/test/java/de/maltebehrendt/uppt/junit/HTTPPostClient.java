package de.maltebehrendt.uppt.junit;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import javax.script.Invocable;

/**
 * Created by malte on 11.02.17.
 */
public class HTTPPostClient {
    private static Vertx vertx = null;
    private static Invocable invocable = null;

    private Logger logger = null;
    private String contentType = null;
    private HttpClient client = null;
    private HttpClientRequest request = null;

    public HTTPPostClient() {
        logger = LoggerFactory.getLogger(this.getClass());
        this.contentType = "application/json";
        this.client = vertx.createHttpClient();
    }

    public void setRequestHeader(String key, String value) {
        if(HttpHeaders.CONTENT_TYPE.equals(key)) {
            this.contentType = value;
        }
    }
    public void open(String method, String URL, boolean async) {
        request = client.post(8080,"localhost", "/email/login", response -> {
            if(response.statusCode() != 200) {
                if(invocable != null) {
                    try {
                        invocable.invokeFunction("httpOnError", response.statusCode());
                    } catch (Exception e) {
                        logger.error("HTTPPostClientForJSTest failed!", e);
                    }
                }
                return;
            }

            response.bodyHandler(totalBuffer -> {
                if(invocable != null) {
                    try {
                        invocable.invokeFunction("httpOnResponse", totalBuffer.toString());
                    } catch (Exception e) {
                        logger.error("HTTPPostClientForJSTest failed!", e);
                    }
                }
            });
        });
    }

    public void send(String body) {
        request.putHeader(HttpHeaders.CONTENT_TYPE, contentType).end(body);
    }

    public static void setInvocable(Invocable newInvocable) {
        invocable = newInvocable;
    }
    public static void setVertx(Vertx newVertx) {
        vertx = newVertx;
    }
}

package de.maltebehrendt.uppt.junit;

import de.helwich.junit.JasmineDescriber;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.apache.commons.codec.binary.Base64;
import org.junit.runner.Description;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * Created by malte on 07.02.17.
 */
public abstract class AbstractJasmineTest extends AbstractTest {
    // adopted from https://github.com/hhelwich/junit-jasmine-runner/blob/master/src/main/java/de/helwich/junit/JasmineTestRunner.java

    private Class<?> testClass = null;
    private Logger logger = null;
    private de.helwich.junit.JasmineTest jasmineTest = null;
    private ScriptEngine nashornJSEngine = null;
    private Description jasmineDescription = null;
    private JasmineReporter jasmineReporter = null;

    public void setupJasmineTestRunner(Vertx vertx, String versionPrefix, String upptLocation, String upptRouterDomain, String upptRouterVersion, String upptRouterType,  JsonObject testParameters) {
        try {
            testClass = this.getClass();
            logger = LoggerFactory.getLogger(testClass);
            jasmineTest = testClass.getAnnotation(de.helwich.junit.JasmineTest.class);
            if (jasmineTest == null) {
                throw new RuntimeException("annotation " + de.helwich.junit.JasmineTest.class.getName() + " is missing on class "
                        + testClass.getName());
            }

            ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
            nashornJSEngine = scriptEngineManager.getEngineByName("nashorn");
            if (nashornJSEngine == null) {
                throw new RuntimeException("Nashorn JS engine not available. Please use java 8.");
            }
            // set test parameters, so that they are usable within the test...
            if(testParameters == null) {
                testParameters = new JsonObject();
            }

            // load browser environment
            evalResource(nashornJSEngine, "/envjs/env.js");

            // load UPPT polyfills and quirks for making UPPT run on nashorn
            // note:  with the envjs XMLHttpRequest method really weird stuff happened. I think it were timing issues/racing conditions
            WebSocketClientForJS.setVertx(vertx);
            HTTPPostClient.setVertx(vertx);
            WebSocketClientForJS.setInvocable((Invocable) nashornJSEngine);
            HTTPPostClient.setInvocable((Invocable) nashornJSEngine);
            evalFile(nashornJSEngine, jasmineTest.testDir() + versionPrefix + "/dependencies/envjsPolyfills.js");
            evalFile(nashornJSEngine, jasmineTest.testDir() + versionPrefix + "/dependencies/promise.min.js");

            // add "normal" dependencies such as socksJS and vert.x EventBus
            evalFile(nashornJSEngine, jasmineTest.testDir() + versionPrefix + "/dependencies/sockjs-0.3.4.envjsPatched.js");
            evalFile(nashornJSEngine, jasmineTest.srcDir() + versionPrefix + "/libs/vertx-eventbus.js");

            // set global variables for the eventbus
            nashornJSEngine.eval("window.dataBusSettings = {\"location\": \"" + upptLocation + "\", \"routerDomain\": \"" + upptRouterDomain + "\", \"routerVersion\": \"" + upptRouterVersion + "\", \"routerType\": \"" + upptRouterType + "\"};");
            nashornJSEngine.eval("window.testParameters = JSON.parse('" + testParameters.encode() + "');");

            // load jasmine and the actual test files
            evalResource(nashornJSEngine, "/jasmine/jasmine.js");
            evalResource(nashornJSEngine, "/jasmine/boot.js");

            JasmineDescriber describer = (JasmineDescriber) nashornJSEngine.eval("jasmine.junitDescriber = new (Java.type(\""
                    + JasmineDescriber.class.getName() + "\")); ");
            describer.setRootName(testClass.getName());

            evalResource(nashornJSEngine, "/de/helwich/junit/describer.js");

            // test files provided in the annotation
            for (String src : jasmineTest.src()) {
                evalFile(nashornJSEngine, jasmineTest.srcDir() + "/" + src + jasmineTest.fileSuffix());
            }
            for (String test : jasmineTest.test()) {
                evalFile(nashornJSEngine, jasmineTest.testDir() + "/" + test + jasmineTest.fileSuffix());
            }

            jasmineDescription = describer.getDescription();
            describer.disable();
            jasmineReporter = (JasmineReporter) nashornJSEngine.eval("jasmine.junitReporter = new (Java.type(\""
                    + JasmineReporter.class.getName() + "\")); ");
            jasmineReporter.setDescription(jasmineDescription);
            //evalResource(nashornJSEngine, "/de/helwich/junit/reporter.js");
            evalFile(nashornJSEngine, jasmineTest.testDir() + versionPrefix +  "/dependencies/jasmineReporter.js");
            //nashornJSEngine.eval("/**  * Delegate jasmine test notifications to Java reporter.  *  * @author Hendrik Helwich  */ (function(){      var junitReporter = jasmine.junitReporter;      var reporter = {         jasmineStarted: function(options) {             //print(\"jasmineStarted \"+JSON.stringify(options));             junitReporter.jasmineStarted(options.totalSpecsDefined);         },         jasmineDone: function() {             //print(\"jasmineDone\");             junitReporter.jasmineDone();         },         suiteStarted: function(result) {             //print(\"suiteStarted \"+JSON.stringify(result));             junitReporter.suiteStarted(result.id, result.description, result.fullName, result.status);         },         suiteDone: function(result) {             //print(\"suiteDone\"+JSON.stringify(result));             junitReporter.suiteDone(result.id, result.description, result.fullName, result.status);         },         specStarted: function(result) {             //print(\"specStarted\"+JSON.stringify(result));             junitReporter.specStarted(result.id, result.description, result.fullName, result.failedExpectations);         },         specDone: function(result) {             if(result.failedExpectations) {               print(\"specDone\"+JSON.stringify(result));             }             junitReporter.specDone(result.id, result.description, result.fullName, result.failedExpectations, result.status);         }     };      var env = jasmine.getEnv();     env.addReporter(reporter);  }());");
        } catch (ScriptException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public Description getJasmineDescription() {
        return jasmineDescription;
    }

    private File projectDir() {
        File targetDir = new File(".");
        return targetDir;
    }

    private final Object evalResource(ScriptEngine nashorn, String name) {
        URL url = testClass.getResource(name);
        String src = url.toExternalForm();
        try {
            return nashorn.eval("load('" + src + "')");
        } catch (ScriptException e) {
            throw new RuntimeException(e);
        }
    }

    private final Object evalFile(ScriptEngine nashorn, String name) {
        File file = new File(projectDir(), name);
        try {
            return nashorn.eval(new FileReader(file));
        } catch (ScriptException e) {
            System.err.println("Failed file: " + name);
            throw new RuntimeException(e);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public void runJasmineTests(TestContext context, Async testResult) {
        jasmineReporter.setContext(context);
        jasmineReporter.setTestResult(testResult);

        try {
            nashornJSEngine.eval("jasmine.getEnv().execute();");
            nashornJSEngine.eval("Envjs.wait()");
        }
        catch (ScriptException e) {
            logger.error("Failed to run Jasmine tests", e);
            context.assertNotNull(e, "runJasmineTests did not throw an exception");
        }
    }

}

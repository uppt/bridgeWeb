package de.maltebehrendt.uppt;

import de.maltebehrendt.uppt.junit.AbstractTest;
import de.maltebehrendt.uppt.util.FileUtils;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.file.FileSystem;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.stream.Stream;

/**
 * Created by malte on 23.05.17.
 */
public class DevHTTPServeService1Test extends AbstractTest {

    private static FileSystem fileSystem = null;
    private static String sourceFolderPath = null;
    private static String destinationFolderPath = null;
    private static Integer bridgeWebPort = 8080;

    @BeforeClass
    public static void setup(TestContext context) {
        Async watcherReadyResult = context.async(1);

        String baseDirectory = "test" + File.separator + MethodHandles.lookup().lookupClass().getSimpleName();
        sourceFolderPath = baseDirectory + File.separator + "src" + File.separator + File.separator + "main" + File.separator + "js";
        destinationFolderPath = baseDirectory + File.separator + "storage" + File.separator + "webroot";

        Map<String, Object> map = setupTestVertxAndDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);

        Vertx vertx = (Vertx) map.get("vertx");
        fileSystem = vertx.fileSystem();
        EventBus eventBus = (EventBus) map.get("eventBus");


        // make sure certain test folders are not in the src anymore...
        if(fileSystem.existsBlocking(sourceFolderPath + File.separator  + "testService" + File.separator + "1.0" + File.separator+ "data.dup")) {
            fileSystem.deleteRecursiveBlocking(sourceFolderPath + File.separator  + "testService" + File.separator + "1.0" + File.separator+ "data.dup", true);
        }
        if(fileSystem.existsBlocking(sourceFolderPath + File.separator  + "testService" + File.separator + "1.0" + File.separator+ "data.del")) {
            fileSystem.deleteRecursiveBlocking(sourceFolderPath + File.separator  + "testService" + File.separator + "1.0" + File.separator+ "data.del", true);
        }

        vertx.setPeriodic(250, id -> {
            // wait until the initial copy is done and the watch service is up
            eventBus.send("devHTTPServeService.1.missingWatcherReadyInfo", new JsonObject(), reply -> {
                if(reply.succeeded()) {
                    JsonObject body = (JsonObject) reply.result().body();
                    if(body.getBoolean("isWatcherReady") == true) {
                        vertx.cancelTimer(id);

                        // check that the inital copy is accurate (not in own test as it leads to side effects)
                        File sourceDir = new File(sourceFolderPath);
                        File targetDir = new File(destinationFolderPath);

                        try {
                            context.assertTrue(areDirectoriesEqual(sourceDir, targetDir), "Initial folder copies are equal.");
                        } catch (Exception e) {
                            context.assertTrue(false, "Initial folder copies are equal: " + e.getMessage());
                        }
                        watcherReadyResult.countDown();
                    }
                }
            });
        });
        context.assertEquals(baseDirectory, map.get("baseDirectory"), "@BeforeClass guessed the correct base directory path");

        watcherReadyResult.awaitSuccess();
    }

    @AfterClass
    public static void teardown(TestContext context) {
        tearDownTestVertxAndCleanupDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
    }

    @Test
    public void testbridgeWebServingInitialFile(TestContext context) {
        Async testResult = context.async(1);

        // get the index.html
        HttpClientOptions clientOptions = new HttpClientOptions()
                .setTrustAll(true);
        HttpClient client = vertx.createHttpClient(clientOptions);
        client.getNow(bridgeWebPort, "localhost", "/public/testService/1.0/index.html", responseHandler -> {
            context.assertEquals(200, responseHandler.statusCode(), "Get request was successful, status code 200");

            responseHandler.bodyHandler(body -> {
                String fileContent = body.toString();
                context.assertTrue(fileContent.length() > 0, "Non-empty file was returned");
                testResult.countDown();
            });

        });

        testResult.awaitSuccess();
    }

    @Test
    public void testbridgeWebServingInitialFileViaAlias(TestContext context) {
        Async testResult = context.async(1);

        // get the index.html
        HttpClientOptions clientOptions = new HttpClientOptions()
                .setTrustAll(true);
        HttpClient client = vertx.createHttpClient(clientOptions);
        client.getNow(bridgeWebPort, "localhost", "/testService/index.html", responseHandler -> {
            context.assertEquals(200, responseHandler.statusCode(), "Get request was successful, status code 200");

            responseHandler.bodyHandler(body -> {
                String fileContent = body.toString();
                context.assertTrue(fileContent.length() > 0, "Non-empty file was returned");
                testResult.countDown();
            });

        });

        testResult.awaitSuccess();
    }

    @Test
    public void testNewFile(TestContext context) {
        Async testResult = context.async(5);

        File srcFile = new File(sourceFolderPath + File.separator + "testService" + File.separator + "1.0" + File.separator + "index.html");
        File newFile = new File(sourceFolderPath + File.separator + "testService" + File.separator + "1.0" + File.separator + "index.new.html");
        File targetFile = new File(destinationFolderPath + File.separator  + "testService" + File.separator + "1.0" + File.separator+ "index.new.html");

        MessageConsumer<JsonObject> consumer = eventBus.consumer("devHTTPServeService.1.newWatchEvent", handler -> {
            JsonObject body = handler.body();

            if ("index.new.html".equals(body.getString("fileName"))) {
                // check notification
                context.assertEquals(false, body.getBoolean("isFileDirectory"));
                context.assertEquals("testService/1.0/index.new.html", body.getString("path"));

                if("ENTRY_MODIFY".equals(body.getString("kind"))) {
                    // check that the file is in the target directory
                    context.assertTrue(targetFile.exists(), "New file was copied to target directory");
                    context.assertTrue(srcFile.length() == targetFile.length(), "Copied file has the same size");
                    testResult.countDown();

                    // check that the bridgeWeb serves the new file
                    HttpClientOptions clientOptions = new HttpClientOptions()
                            .setTrustAll(true);
                    HttpClient client = vertx.createHttpClient(clientOptions);
                    client.getNow(bridgeWebPort, "localhost", "/testService/index.new.html", responseHandler -> {
                        context.assertEquals(200, responseHandler.statusCode(), "Get request was successful, status code 200");

                        responseHandler.bodyHandler(httpBody -> {
                            String fileContent = httpBody.toString();
                            context.assertTrue(fileContent.length() == newFile.length(), "New file was served");
                            testResult.countDown();

                            fileSystem.delete(newFile.getAbsolutePath(), delResult -> {
                                context.assertTrue(delResult.succeeded(), "New file was deleted again");
                                testResult.countDown();
                            });
                        });

                    });
                }
                else if("ENTRY_DELETE".equals(body.getString("kind"))) {
                    context.assertFalse(targetFile.exists());
                    testResult.countDown();
                }
            }
        });

        fileSystem.copy(srcFile.getAbsolutePath(), newFile.getAbsolutePath(), result -> {
            context.assertTrue(result.succeeded(), "Duplicated index.html for checking new file handling/detection");
            testResult.countDown();
        });

        testResult.awaitSuccess();
        consumer.unregister();
    }


    @Test
    public void testDeletedFile(TestContext context) {
        Async testResult = context.async(5);

        File srcFile = new File(sourceFolderPath + File.separator + "testService" + File.separator + "1.0" + File.separator + "index.html");
        File newFile = new File(sourceFolderPath + File.separator + "testService" + File.separator + "1.0" + File.separator + "index.del.html");
        File targetFile = new File(destinationFolderPath + File.separator  + "testService" + File.separator + "1.0" + File.separator+ "index.del.html");

        MessageConsumer<JsonObject> consumer = eventBus.consumer("devHTTPServeService.1.newWatchEvent", handler -> {
            JsonObject body = handler.body();

            if ("index.del.html".equals(body.getString("fileName"))) {
                // check notification
                context.assertEquals(false, body.getBoolean("isFileDirectory"));
                context.assertEquals("testService/1.0/index.del.html", body.getString("path"));

                if ("ENTRY_MODIFY".equals(body.getString("kind"))) {
                    // check that the file is in the target directory
                    context.assertTrue(targetFile.exists(), "New file was copied to target directory");
                    context.assertTrue(srcFile.length() == targetFile.length(), "Copied file has the same size");
                    testResult.countDown();

                    // delete file in source
                    fileSystem.delete(newFile.getAbsolutePath(), delResult -> {
                        context.assertTrue(delResult.succeeded(), "New file was deleted again");
                        testResult.countDown();
                    });
                }
                else if("ENTRY_DELETE".equals(body.getString("kind"))) {
                    context.assertFalse(targetFile.exists(), "Target file was deleted in target directory");
                    testResult.countDown();

                    // check that the bridgeWeb does not serve the deleted file
                    HttpClientOptions clientOptions = new HttpClientOptions()
                            .setTrustAll(true);
                    HttpClient client = vertx.createHttpClient(clientOptions);
                    client.getNow(bridgeWebPort, "localhost", "/testService/index.del.html", responseHandler -> {
                        context.assertEquals(404, responseHandler.statusCode(), "Deleted file request caused a 404");
                        testResult.countDown();
                    });
                }
            }
        });

        fileSystem.copy(srcFile.getAbsolutePath(), newFile.getAbsolutePath(), result -> {
            if(result.failed()) {
                result.cause().printStackTrace();
            }

            context.assertTrue(result.succeeded(), "Duplicated index.html for checking del file handling/detection");
            testResult.countDown();
        });

        testResult.awaitSuccess();
        consumer.unregister();
    }

    @Test
    public void testNewFolder(TestContext context) {
        Async testResult = context.async(6);

        File sourceFolder = new File(sourceFolderPath + File.separator + "testService" + File.separator + "1.0" + File.separator + "data");
        File newFolder = new File(sourceFolderPath + File.separator + "testService" + File.separator + "1.0" + File.separator + "data.dup");
        File targetFolder = new File(destinationFolderPath + File.separator  + "testService" + File.separator + "1.0" + File.separator+ "data.dup");

        MessageConsumer<JsonObject> consumer = eventBus.consumer("devHTTPServeService.1.newWatchEvent", handler -> {
            JsonObject body = handler.body();

            if ("data.dup".equals(body.getString("fileName"))) {
                context.assertEquals(true, body.getBoolean("isFileDirectory"));
                context.assertEquals("testService/1.0/data.dup",body.getString("path"));

                if ("ENTRY_CREATE".equals(body.getString("kind"))) {
                    // check that the new folder is in the target directory
                    context.assertTrue(targetFolder.exists() && targetFolder.isDirectory(), "Folder has been duplicated into target folder");
                    try {
                        context.assertTrue(areDirectoriesEqual(sourceFolder, targetFolder), "Folder has been duplicated correctly");
                    } catch (Exception e) {
                        context.assertTrue(false, "Folder has been duplicated correctly");
                        return;
                    }
                    testResult.countDown();

                    // check that files are served from the duplicated folder
                    HttpClientOptions clientOptions = new HttpClientOptions()
                            .setTrustAll(true);
                    HttpClient client = vertx.createHttpClient(clientOptions);
                    client.getNow(bridgeWebPort, "localhost", "/testService/data.dup/sample_success_response.json", responseHandler -> {
                        context.assertEquals(200, responseHandler.statusCode(), "File from new folder is being served");

                        responseHandler.bodyHandler(httBbody -> {
                            String fileContent = httBbody.toString();
                            context.assertTrue(fileContent.contains("successMessage"), "Correct file from new folder was served");
                            testResult.countDown();

                            // check that modifications (e.g. new files) in the duplicated folder are being observed
                            // TODO: subsubfolders...
                            try {
                                FileWriter writer = new FileWriter(newFolder.getAbsolutePath() + File.separator + "new_response.json", true);
                                writer.write(fileContent);
                                writer.close();
                            } catch (IOException e) {
                                context.assertTrue(false, "New file has been writen into duplicated folder");
                                return;
                            }
                        });
                    });
                }
                if("ENTRY_DELETE".equals(body.getString("kind"))) {
                    context.assertFalse(targetFolder.exists(), "Duplicated folder was deleted in target directory");
                    testResult.countDown();
                }
            }
            else if("new_response.json".equals(body.getString("fileName"))) {
                context.assertEquals(false, body.getBoolean("isFileDirectory"));
                context.assertEquals("testService/1.0/data.dup/new_response.json", body.getString("path"));

                if ("ENTRY_MODIFY".equals(body.getString("kind"))) {
                    File targetFile = new File(targetFolder.getAbsolutePath() + File.separator + "new_response.json");
                    context.assertTrue(targetFile.exists(), "New file in duplicated folder has been duplicated");
                    testResult.countDown();

                    // delete folder in source directory
                    fileSystem.deleteRecursive(newFolder.getAbsolutePath(), true, delResult -> {
                        context.assertTrue(delResult.succeeded(), "Duplicated folder was deleted in source directory");
                        testResult.countDown();

                    });
                }
            }
        });

        fileSystem.copyRecursive(sourceFolder.getAbsolutePath(), newFolder.getAbsolutePath(), true, result -> {
            context.assertTrue(result.succeeded(), "Duplicated data folder for checking new folder: " + result.cause());
            testResult.countDown();
        });

        testResult.awaitSuccess();
        consumer.unregister();
    }

    @Test
    public void testDeletedFolder(TestContext context) {
        Async testResult = context.async(4);

        File sourceFolder = new File(sourceFolderPath + File.separator + "testService" + File.separator + "1.0" + File.separator + "data");
        File newFolder = new File(sourceFolderPath + File.separator + "testService" + File.separator + "1.0" + File.separator + "data.del");
        File targetFolder = new File(destinationFolderPath + File.separator  + "testService" + File.separator + "1.0" + File.separator+ "data.del");

        MessageConsumer<JsonObject> consumer = eventBus.consumer("devHTTPServeService.1.newWatchEvent", handler -> {
            JsonObject body = handler.body();

            if ("data.del".equals(body.getString("fileName"))) {
                context.assertEquals(true, body.getBoolean("isFileDirectory"));
                context.assertEquals("testService/1.0/data.del",body.getString("path"));

                if("ENTRY_CREATE".equals(body.getString("kind"))) {
                    // check that the new folder is in the target directory
                    context.assertTrue(targetFolder.exists() && targetFolder.isDirectory(), "Folder has been duplicated into target folder");
                    try {
                        context.assertTrue(areDirectoriesEqual(newFolder, targetFolder), "Folder has been duplicated correctly");
                    } catch (Exception e) {
                        context.assertTrue(false, "Folder has been duplicated correctly: " + e.getMessage());
                        return;
                    }
                    testResult.countDown();

                    // delete folder in source directory
                    context.assertTrue(FileUtils.deleteFolder(newFolder.getAbsolutePath()), "Duplicated folder was deleted in source directory");
                    //fileSystem.deleteRecursiveBlocking(newFolder.getAbsolutePath(), true);

                       // context.assertTrue(delResult.succeeded(), "Duplicated folder was deleted in source directory");
                        testResult.countDown();
                    //});
                }
                else if("ENTRY_DELETE".equals(body.getString("kind"))) {
                    context.assertFalse(targetFolder.exists(), "Duplicated folder was deleted in target directory");
                    testResult.countDown();
                }
            }

        });

        fileSystem.copyRecursive(sourceFolder.getAbsolutePath(), newFolder.getAbsolutePath(), true, result -> {
            context.assertTrue(result.succeeded(), "Duplicated data folder for checking folder deletion");
            testResult.countDown();
        });

        testResult.awaitSuccess();
        consumer.unregister();
    }

    @Test
    public void testModifiedFile(TestContext context) {
        Async testResult = context.async(4);

        File srcFile = new File(sourceFolderPath + File.separator + "testService" + File.separator + "1.0" + File.separator + "index.html");
        File newFile = new File(sourceFolderPath + File.separator + "testService" + File.separator + "1.0" + File.separator + "index.mod.html");
        File targetFile = new File(destinationFolderPath + File.separator  + "testService" + File.separator + "1.0" + File.separator+ "index.mod.html");
        Boolean markers[] = new Boolean[2];
        markers[0] = false;
        markers[1] = false;

        if(newFile.exists()) {
            newFile.delete();
        }
        if(targetFile.exists()) {
            targetFile.delete();
        }

        MessageConsumer<JsonObject> consumer = eventBus.consumer("devHTTPServeService.1.newWatchEvent", handler -> {
            JsonObject body = handler.body();

            if ("index.mod.html".equals(body.getString("fileName"))) {
                // check notification
                context.assertEquals(false, body.getBoolean("isFileDirectory"));
                context.assertEquals("testService/1.0/index.mod.html", body.getString("path"));

                if ("ENTRY_MODIFY".equals(body.getString("kind"))) {
                    // check that the file is in the target directory
                    context.assertTrue(targetFile.exists(), "New file was copied to target directory");

                    // check that the bridgeWeb serves the file
                    HttpClientOptions clientOptions = new HttpClientOptions()
                            .setTrustAll(true);
                    HttpClient client = vertx.createHttpClient(clientOptions);
                    client.getNow(bridgeWebPort, "localhost", "/testService/index.mod.html", responseHandler -> {
                        context.assertEquals(200, responseHandler.statusCode(), "Get request was successful, status code 200");

                        responseHandler.bodyHandler(htmlBody -> {
                            String fileContent = htmlBody.toString();

                            String newTitle = "<title>Shop - TITLE HAS BEEN MODIFIED HASNT IT</title>";

                            if(fileContent.contains(newTitle)) {
                                // this is the modified file
                                context.assertTrue(fileContent.contains(newTitle), "File content has been updated");
                                testResult.countDown();
                                markers[1] = true;

                                fileSystem.delete(newFile.getAbsolutePath(), delResult -> {
                                    context.assertTrue(delResult.succeeded(), "New file was deleted again");
                                    testResult.countDown();
                                });
                            }
                            else {
                                context.assertTrue(srcFile.length() == targetFile.length(), "Copied file has the same size");

                                // modify the file
                                List<String> lines = new LinkedList<String>();
                                try(Stream<String> stream = Files.lines(newFile.toPath())) {
                                    stream.forEach(line -> {
                                        if(line.contains("<title>")) {
                                            lines.add(newTitle);
                                        }
                                        else {
                                            lines.add(line);
                                        }
                                    });
                                }
                                catch (IOException e) {
                                    context.assertTrue(false, "Read mod file into memory");
                                    return;
                                }
                                try {
                                    Files.write(newFile.toPath(), lines);
                                    markers[0] = true;
                                    testResult.countDown();
                                } catch (IOException e) {
                                    context.assertTrue(false, "Wrote modified file into source directory");
                                    return;
                                }
                            }
                        });
                    });

                }
            }
        });

        fileSystem.copy(srcFile.getAbsolutePath(), newFile.getAbsolutePath(), result -> {
            context.assertTrue(result.succeeded(), "Duplicated index.html for checking file change handling/detection");
            testResult.countDown();
        });

        testResult.awaitSuccess();
        context.assertTrue(markers[0], "New file was served");
        context.assertTrue(markers[0], "Modified file was served");
    }

    private static Boolean areDirectoriesEqual(File dir1, File dir2) throws Exception {
        if(!dir1.exists()
                || !dir1.isDirectory()
                || !dir2.exists()
                || !dir2.isDirectory()) {
            System.err.println("One of the directories does not exist!");
            return false;
        }

        Files.walkFileTree(dir1.toPath(), new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attr) throws IOException {
                FileVisitResult result = super.preVisitDirectory(dir, attr);

                Path relativePath = dir1.toPath().relativize(dir);
                File otherDir = dir2.toPath().resolve(relativePath).toFile();

                if(!otherDir.exists()) {
                    throw new IOException("Folder " + relativePath + " missing!");
                }
                if(!Arrays.equals(otherDir.list(), dir.toFile().list())) {
                    System.err.println(otherDir.getAbsoluteFile().getPath() + " -> " + Arrays.toString(otherDir.list()));
                    System.err.println(dir.toFile().getAbsoluteFile().getPath() + " -> " + Arrays.toString(dir.toFile().list()));
                    throw new IOException("Folder lists are different for: " + relativePath);
                }

                return result;
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attr) throws IOException {
                FileVisitResult result = super.visitFile(file, attr);

                // only compare file size here..
                Path relativePath = dir1.toPath().relativize(file);
                File otherFile = dir2.toPath().resolve(relativePath).toFile();

                if(!otherFile.exists()) {
                    throw new IOException("File " + relativePath + " missing!");
                }
                if(otherFile.length() != attr.size()) {
                    throw new IOException("File sizes " + relativePath + " not equal!");
                }

                return result;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                super.visitFileFailed(file, exc);
                throw exc;
            }
        });

        return true;
    }
}

package de.maltebehrendt.uppt;

import de.maltebehrendt.uppt.junit.AbstractJasmineTest;
import de.maltebehrendt.uppt.util.EncryptionUtils;
import io.vertx.core.Future;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.invoke.MethodHandles;
import java.nio.charset.Charset;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by malte on 06.02.17.
 */
@de.helwich.junit.JasmineTest(
        src =  { "bridgeWeb/1.0/libs/uppt-databus" },
        test = { "bridgeWeb/1.0/libs/uppt-databus-spec" }
)
public class JSDataBusSpec1Test extends AbstractJasmineTest {
    private static String versionPrefix = "/bridgeWeb/1.0";
    private static String upptLocation = "http://localhost:8080";
    private static String upptRouterDomain = "in.test";
    private static String upptRouterVersion = "1";
    private static String upptRouterType = "router";

    @BeforeClass
    public static void setup(TestContext context) {
        setupTestVertxAndDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
    }

    @AfterClass
    public static void teardown(TestContext context) {
        tearDownTestVertxAndCleanupDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
    }

    @Test
    public void testUpptJS(TestContext context) {
        Async testResult = context.async(2);

        vertx.executeBlocking((Future<JsonObject> future) -> {
            // setup valid user account
            getNewUser(new JsonArray().add("tester"), future);
        }, createUserResult -> {
            context.assertTrue(createUserResult.succeeded(), "New test user was created");
            JsonObject newUserInfo = createUserResult.result();
            context.assertTrue(newUserInfo != null && !newUserInfo.isEmpty(), "New user information have been returned");
            testResult.countDown();

            // setup the test environment on client side (remember to set the router eventbus address!)
            setupJasmineTestRunner(vertx, versionPrefix,  upptLocation, upptRouterDomain, upptRouterVersion, upptRouterType, new JsonObject()
                    .put("email", newUserInfo.getString("emailAddress"))
                    .put("password", newUserInfo.getString("password"))
            );
            // run the Jasmine specification test
            vertx.executeBlocking(future -> {
                runJasmineTests(context, testResult);
            }, result -> {});

        });

        testResult.awaitSuccess();
    }
}

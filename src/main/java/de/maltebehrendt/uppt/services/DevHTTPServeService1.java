package de.maltebehrendt.uppt.services;

import de.maltebehrendt.uppt.annotation.Customer;
import de.maltebehrendt.uppt.annotation.Payload;
import de.maltebehrendt.uppt.annotation.Processor;
import de.maltebehrendt.uppt.enums.DataType;
import de.maltebehrendt.uppt.events.Message;
import io.vertx.core.AsyncResult;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.file.AsyncFile;
import io.vertx.core.file.FileProps;
import io.vertx.core.file.FileSystem;
import io.vertx.core.file.OpenOptions;
import io.vertx.core.json.JsonObject;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import static java.nio.file.StandardWatchEventKinds.*;

/**
 * Created by malte on 03.04.17.
 */
public class DevHTTPServeService1 extends AbstractService  {
    private Path sourcePath = null;
    private WatchService watchService = null;
    private FileSystem fileSystem = null;
    private Long timerID = null;
    private HashMap<WatchKey, Path> watchKeys = null;
    private boolean isWatcherReady = false;
    private JsonObject aliasForBridge = null;

    @Override
    public void prepare(Future<Object> prepareFuture) {
        try {
            fileSystem = vertx.fileSystem();

            String sourceFolderPath = "src" + File.separator + "main" + File.separator + "js";
            if (config().containsKey("sourceFolderPath")) {
                sourceFolderPath = config().getString("sourceFolderPath");
            }
            if (config().containsKey("aliasForBridge")) {
                aliasForBridge = config().getJsonObject("aliasForBridge");
            }

            sourcePath = Paths.get(sourceFolderPath).toAbsolutePath();
            watchKeys = new HashMap<>();

            // remove existing content
            List<Future> delFutures = new LinkedList<>();
            List<String> files = fileSystem.readDirBlocking(sourcePath.toString());
            for (String file : files) {
                Path filePath = Paths.get(file).toAbsolutePath();
                Future delFuture = Future.future();
                delFutures.add(delFuture);
                del(filePath, delFuture);
            }

            CompositeFuture.all(delFutures).setHandler(handler -> {
                prepareFuture.complete();
            });
        }
        catch(Exception e) {
            e.printStackTrace();
            prepareFuture.fail(e);
        }
    }

    @Override
    @Customer(
            domain = "publicFiles",
            version = "1",
            type = "missingAlias",
            description = "Only here to express dependency on bridge service"
    )
    public void startConsuming() {
        // prepare the alias
        if(aliasForBridge != null && !aliasForBridge.isEmpty()) {
            eventBus.publish("publicFiles.1.missingAlias", new JsonObject()
                    .put("path", aliasForBridge.getString("path"))
                    .put("alias", aliasForBridge.getString("alias"))
            );
        }

        // initial copying of files... (async, just assuming it will go through smoothly and that there are no significant side effects later on...)
        Future<Object> copyFuture = Future.future();
        copyFuture.setHandler(result -> {
            if(result.failed()) {
                logger.fatal(serviceID, serviceName, 500,"Failed to copy source files to public storage!", result.cause());
                System.exit(-1);
            }

            // start watching service and add all sub-folders
            try {
                watchService = sourcePath.getFileSystem().newWatchService();

                Files.walkFileTree(sourcePath, new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                        WatchKey watchKey = dir.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);
                        watchKeys.put(watchKey, dir);
                        return FileVisitResult.CONTINUE;
                    }
                });
            }
            catch(Exception e) {
                logger.fatal(serviceID, serviceName, 500, "Failed to start watch service for detecting file updates!", e);
                System.exit(-1);
            }

            // notify of availability
            isWatcherReady = true;
            publish("devHTTPServeService", "1", "watcherReady", new JsonObject());
            logger.info(serviceID, serviceName, 200, "DevHTTPServiceService up and running...");

            // check for changes periodically...
            timerID = vertx.setPeriodic(250, handler -> {
                try {
                    WatchKey watchKey = watchService.poll();

                    if(watchKey == null) {
                        return;
                    }

                    Path folderPath = watchKeys.get(watchKey);
                    if(folderPath == null) {
                        logger.warn(serviceID, serviceName, 500, "Experienced unknown/unregistered path event.");
                        return;
                    }


                    for(WatchEvent<?> event : watchKey.pollEvents()) {
                        WatchEvent.Kind kind = event.kind();

                        if(kind == OVERFLOW) {
                            logger.warn(serviceID, serviceName, 500, "Overflow when watching file changes in " + config().getString("sourceFolderPath"));
                            continue;
                        }
                        Path filePath = folderPath.resolve((((WatchEvent<Path>) event).context()));
                        Future<Boolean> isDirectoryFuture = Future.future();
                        isDirectoryFuture.setHandler(isDirectoryHandler -> {
                            Boolean isFileDirectory = isDirectoryHandler.result();

                            Future handlingFuture = Future.future();
                            Handler<AsyncResult> handlingResult = new Handler<AsyncResult>() {
                                @Override
                                public void handle(AsyncResult result) {
                                    if(result.succeeded()) {
                                        if(kind == ENTRY_CREATE && isFileDirectory) {
                                            try {
                                                WatchKey key = filePath.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);
                                                watchKeys.put(key, filePath);
                                            } catch (IOException e) {
                                                logger.error("Failed to add new folder to watch service: " + filePath, e);
                                                return;
                                            }
                                        }
                                        publish("devHTTPServeService", "1", "newWatchEvent", new JsonObject()
                                                .put("kind", kind.name())
                                                .put("fileName", filePath.getFileName().toString())
                                                .put("isFileDirectory", isFileDirectory)
                                                .put("path", sourcePath.relativize(filePath).toString())
                                        );
                                    }
                                    else {
                                        logger.error(serviceID, serviceName, 500, "Failed to handle watch event " + kind.name() + " for " + filePath.toString());
                                    }
                                }
                            };
                            handlingFuture.setHandler(handlingResult);

                            if(kind == ENTRY_DELETE) {
                                del(filePath, handlingFuture);
                            }
                            else if(kind == ENTRY_MODIFY){
                                // copy new file (version)
                                copy(filePath, handlingFuture);
                            }
                            else if(kind == ENTRY_CREATE && isFileDirectory) {
                                copy(filePath, handlingFuture);
                                // else file: ignore for now - adding content to a file leads to a modification event
                            }
                        });


                        if(kind == ENTRY_DELETE) {
                            Future<JsonObject> metaFuture = Future.future();
                            metaFuture.setHandler(metaHandler -> {
                                if(metaHandler.failed()) {
                                    isDirectoryFuture.complete(false);
                                    return;
                                }
                                isDirectoryFuture.complete(metaHandler.result().getBoolean("isDirectory"));
                            });
                            storage.retrieveMetaDataFromPublic(sourcePath.relativize(filePath).toString(), metaFuture);
                        }
                        else {
                            fileSystem.props(filePath.toString(), propsHandler -> {
                                if(propsHandler.failed()) {
                                    isDirectoryFuture.complete(false);
                                    return;
                                }
                                Boolean isFileDirectory = propsHandler.result().isDirectory();
                                isDirectoryFuture.complete(isFileDirectory);
                            });
                        }
                    }

                    if(!watchKey.reset()) {
                        logger.info(serviceID, serviceName, 500, "Watch key is not valid anymore (watched directory inaccessible): " + folderPath);
                        watchKeys.remove(watchKey);

                        publish("devHTTPServeService", "1", "newWatchEvent", new JsonObject()
                                .put("kind", "WATCH_DELETE")
                                .put("fileName", folderPath.getFileName().toString())
                                .put("isFileDirectory", true)
                                .put("path", sourcePath.relativize(folderPath).toString())
                        );
                    }
                }
                catch(Exception e) {
                    logger.error(serviceID, serviceName, 500, "Processing file events in folder " + config().getString("sourceFolderPath") + " caused an exception!", e);
                }
            });
        });

        // initial copy
        copy(sourcePath, copyFuture);
    }

    @Processor(
            domain = "devHTTPServeService",
            type = "missingWatcherReadyInfo",
            provides = {
                    @Payload(
                            key = "isWatcherReady",
                            type = DataType.BOOLEAN
                    )
            }
    )
    public void processMissingWatcherReadyInfo(Message message) {
        if(isWatcherReady) {
            message.reply(200, new JsonObject().put("isWatcherReady", true));
        }
        else {
            message.reply(503, new JsonObject().put("isWatcherReady", false));
        }
    }

    private void del(Path sourceFilePath, Future delFuture) {
        Path targetPath = sourcePath.relativize(sourceFilePath);
        storage.deleteFromPublic(targetPath.toString(), true, delFuture);
    }

    private void copy(Path sourceFilePath, Future future) {
        Path targetPath = sourcePath.relativize(sourceFilePath);
        FileProps fileProps = fileSystem.propsBlocking(sourceFilePath.toString());

        if(fileProps.isDirectory()) {
            List<Future> copyFutures = new LinkedList<Future>();
            List<String> files = fileSystem.readDirBlocking(sourceFilePath.toString());
            for(String file : files) {
                Path filePath = Paths.get(file).toAbsolutePath();
                Future copyFuture = Future.future();
                copyFutures.add(copyFuture);
                copy(filePath, copyFuture);
            }
            CompositeFuture.all(copyFutures).setHandler(result -> {
               if(result.failed()) {
                   future.fail(result.cause());
                   return;
               }
               future.complete();
            });
        }
        else {
            AsyncFile sourceFile = fileSystem.openBlocking(sourceFilePath.toString(), new OpenOptions().setRead(true));
            storage.persistToPublic(targetPath.toString(), true, sourceFile, future);
        }
    }

    @Override
    public void shutdown(Future<Object> shutdownFuture) {
        if(timerID != null) {
            vertx.cancelTimer(timerID);
        }

        if(watchService != null) {
            try {
                watchService.close();
            } catch (IOException e) {
                logger.warn("Failed to close watch service", e);
            }
        }
        shutdownFuture.complete();
    }
}

package de.maltebehrendt.uppt.services;

import de.maltebehrendt.uppt.annotation.Payload;
import de.maltebehrendt.uppt.annotation.Processor;
import de.maltebehrendt.uppt.cache.AsyncMessageReplyCache;
import de.maltebehrendt.uppt.enums.DataType;
import de.maltebehrendt.uppt.events.Message;
import io.vertx.core.Future;
import io.vertx.core.MultiMap;
import io.vertx.core.http.*;
import io.vertx.core.http.impl.MimeMapping;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.net.JksOptions;
import io.vertx.core.shareddata.AsyncMap;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;
import io.vertx.ext.bridge.PermittedOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.ext.web.handler.sockjs.SockJSHandlerOptions;
import io.vertx.ext.web.impl.Utils;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.DateFormat;
import java.util.*;

/**
 * Created by malte on 31.01.17.
 *s
 */
public class BridgeWebService1 extends AbstractService {

    // TODO: check if both sessionMap and webSocketSessionMap are required... (isn't write handler enough?)
    // TODO: smart caching
    // TODO: check sessionEnded -> roles anonymous/authenticated set?
    // TODO: session only interrupted if not anonymous - close otherwise!

    private SharedData sharedData = null;
    private Map<String, JsonObject> inboundWhitelist = null;
    private Map<String, JsonObject> outboundWhitelist = null;
    private Map<String, String> availableBridges = null;
    private Map<String, String> publicFilesAliasMap = null;
    private Map<String, JsonObject> clientTraitMap = null;
    private SecretKeyFactory secretKeyFactory = null;
    private byte[] saltBytes = null;
    private Boolean isBackendLoggingEnabled = false;
    private Integer backendLoggingLevel = 1;
    private Router router = null;
    private HttpServer httpServer = null;
    private Long configCheckPeriodic = null;
    private Map<String, JsonObject> sessionMap = null;
    private Map<String, JsonObject> webSocketSessionMap = null;

    private AsyncMessageReplyCache sessionAuthenticationMethodCache = null;
    private AsyncMessageReplyCache sessionStateCache = null;

    @Override
    public void prepare(Future<Object> prepareFuture) {
        sharedData = vertx.sharedData();
        inboundWhitelist = new HashMap<>();
        inboundWhitelist.put("in.logging.1.log", new JsonObject());
        inboundWhitelist.put("in.sessions.1.sessionMissing", new JsonObject());
        inboundWhitelist.put("in.sessions.1.authenticationMissing", new JsonObject().put("user", "anonymous"));
        inboundWhitelist.put("in.sessions.1.authenticationPreflight", new JsonObject().put("user", "anonymous"));
        inboundWhitelist.put("in.sessions.1.sessionEnded", new JsonObject().put("roles", new JsonArray().add("anonymous").add("authenticated")));
        outboundWhitelist = new HashMap<>();
        availableBridges = new HashMap<>();
        publicFilesAliasMap = new HashMap<>();
        clientTraitMap = new HashMap<>();
        sessionMap = new HashMap<>();
        webSocketSessionMap = new HashMap<>();
        sessionAuthenticationMethodCache = new AsyncMessageReplyCache(vertx, this);
        sessionStateCache = new AsyncMessageReplyCache(vertx, this);

        // loading configuration and set defaults, if missing
        loadConfig();

        try {
            secretKeyFactory = SecretKeyFactory.getInstance(config().getJsonObject("hashPolicy").getString("factory"));
            saltBytes = config().getJsonObject("hashPolicy").getString("salt").getBytes("UTF-8");
        } catch (NoSuchAlgorithmException e) {
            logger.fatal(serviceID, serviceName, 500, "Configured secretKeyFactory is not available: " + config().getJsonObject("hashPolicy").getString("factory"), e);
            prepareFuture.fail(e);
            return;
        } catch (UnsupportedEncodingException e) {
            logger.fatal(serviceID, serviceName, 500, "Configured sal encoding is not available", e);
            prepareFuture.fail(e);
            return;
        }

        // setup logging of client-side log messages
        isBackendLoggingEnabled = config().getBoolean("isClientSideLoggingEnabled");
        if(isBackendLoggingEnabled) {
            switch (config().getJsonObject("clientSideLogging").getString("level")) {
                case "INFO":
                case "LOG":
                    backendLoggingLevel = 4;
                    break;
                case "WARN":
                    backendLoggingLevel = 3;
                    break;
                case "ERROR":
                    backendLoggingLevel = 2;
                    break;
                case "FATAL":
                    backendLoggingLevel = 1;
                    break;
                default:
                    logger.warn(serviceID, serviceName,400, "Invalid clientSideLogging.level entry in config! Defaulting to level 1 (FATAL)");
                    backendLoggingLevel = 1;
                    break;
            }
        }

        startServer(prepareFuture);
    }

    @Override
    public void startConsuming() {
        // note: server already started in prepare! Might be dangerous!

        // apply configuration from the shared data at start and every x minutes
        // thereby ensuring that no config change was missed (racing condition at startup or missed publish message)
        applyConfigurationFromSharedData(true);
        configCheckPeriodic = vertx.setPeriodic(300000L, handler-> {
            applyConfigurationFromSharedData(true);
        });
    }

    private void startServer(Future<Object> startFuture) {
        try {
            router = Router.router(vertx);

            // CORS setup
            String allowedOriginPattern = config().getJsonObject("CORS").getString("allowedOriginPattern");
            if (allowedOriginPattern != null && !allowedOriginPattern.isEmpty()) {
                router.route().handler(CorsHandler
                        .create(allowedOriginPattern)
                        .allowedMethod(HttpMethod.GET)
                        .allowedMethod(HttpMethod.POST)
                        .allowedMethod(HttpMethod.OPTIONS)
                        .allowedHeader("Content-Type")
                        .allowedHeader("Authorization")
                        .allowedHeader("www-authenticate")
                        .allowedHeader("Access-Control-Request-Method")
                        .allowedHeader("Access-Control-Allow-Credentials")
                        .allowedHeader("Access-Control-Allow-Origin")
                        .allowedHeader("Access-Control-Allow-Headers")
                        .allowedHeader("Cookie")
                        .allowedHeader("Set-Cookie")
                        .allowedHeader("Content-Length")
                        .allowedHeader("XSessionToken")
                );
                logger.security(serviceID, 201, "Configuring CORS allowed origin pattern to allow (incl. get and post): " + allowedOriginPattern);
            }

            // SockJS/WebSocket setup
            if (config().getBoolean("isWebSocketBridgeEnabled")) {
                SockJSHandlerOptions sockJSHandlerOptions = new SockJSHandlerOptions().setHeartbeatInterval(config().getJsonObject("webSocketBridge").getInteger("heartbeatInterval"));
                SockJSHandler sockJSHandler = SockJSHandler.create(vertx, sockJSHandlerOptions);

                // Note I'm doing something very dangerous here: I DISABLE  Vert.x own SECURITY/blacklist and allow any address with the prefix "in"/"out"!
                BridgeOptions bridgeOptions = new BridgeOptions().setReplyTimeout(config().getJsonObject("webSocketBridge").getLong("replyTimeout"));
                bridgeOptions.addInboundPermitted(new PermittedOptions().setAddressRegex("in\\..+"));
                bridgeOptions.addOutboundPermitted(new PermittedOptions().setAddressRegex("out\\..+"));

                sockJSHandler.bridge(bridgeOptions, bridgeEvent -> {
                    JsonObject rawMessage = bridgeEvent.getRawMessage();
                    final String address = rawMessage != null && rawMessage.containsKey("address") ? rawMessage.getString("address") : "none";

                    // Here I'm trying to ADD SECURITY on-the-fly again...
                    switch (bridgeEvent.type()) {
                        case SEND:
                            // This event will occur when a message is attempted to be sent from the client to the server
                        case PUBLISH:
                            // This event will occur when a message is attempted to be published from the client to the server
                            if (address == null || !inboundWhitelist.containsKey(address)) {
                                // redirect to error response
                                rawMessage.put("address", "in.sessions.1.bridgingErrorOccurred")
                                        .put("body", new JsonObject()
                                                .put("error", new JsonObject()
                                                        .put("code", 404)
                                                        .put("message", "Target address is null, not existing or not whitelisted on backend")
                                                        .put("host", bridgeEvent.socket().remoteAddress().host())));
                                bridgeEvent.complete(true);
                                return;
                            }

                            JsonObject inAuthorities = inboundWhitelist.get(address);
                            if (inAuthorities == null) {
                                // address is not on whitelist
                                rawMessage.put("address", "in.sessions.1.bridgingErrorOccurred")
                                        .put("body", new JsonObject()
                                                .put("error", new JsonObject()
                                                        .put("code", 404)
                                                        .put("message", "Target address is null, not existing or not whitelisted on backend")
                                                        .put("host", bridgeEvent.socket().remoteAddress().host())));
                                bridgeEvent.complete(true);
                                return;
                            }

                            String publishWriteHandlerID = bridgeEvent.socket().writeHandlerID();
                            JsonObject publishSession = webSocketSessionMap.get(publishWriteHandlerID);
                            JsonObject publishHeaders =  rawMessage.getJsonObject("headers");
                            if(publishHeaders == null) {
                                publishHeaders = new JsonObject();
                                rawMessage.put("headers", publishHeaders);
                            }
                            String publishCorrelationID = publishHeaders.containsKey("correlationID")? publishHeaders.getString("correlationID") : UUID.randomUUID().toString();
                            publishHeaders.put("correlationID", publishCorrelationID);

                            if(address.startsWith("in.sessions.1")) {
                                // intercept message
                                Future<JsonObject> interceptionFuture = Future.future();
                                interceptionFuture.setHandler(interceptionResult -> {
                                   if(interceptionResult.failed()) {
                                       rawMessage.put("address", "in.sessions.1.bridgingErrorOccurred")
                                               .put("body", new JsonObject()
                                                       .put("error", new JsonObject()
                                                               .put("code", 500)
                                                               .put("message", interceptionResult.cause())
                                                               .put("host", bridgeEvent.socket().remoteAddress().host())));
                                       bridgeEvent.complete(true);
                                       return;
                                   }

                                   JsonObject resultObject = interceptionResult.result();
                                   Integer statusCode = resultObject.getInteger("statusCode");
                                   JsonObject result = resultObject.containsKey("result")? resultObject.getJsonObject("result") : new JsonObject();
                                   if(statusCode >= 300) {
                                        rawMessage.put("address", "in.sessions.1.bridgingErrorOccurred")
                                                .put("body", new JsonObject()
                                                        .put("error", new JsonObject()
                                                                .put("code", statusCode)
                                                                .put("message", resultObject.getString("message"))
                                                                .put("host", bridgeEvent.socket().remoteAddress().host())));
                                        bridgeEvent.complete(true);
                                        return;
                                   }

                                   JsonObject currentSession = webSocketSessionMap.get(publishWriteHandlerID);
                                   if(currentSession == null) {
                                       currentSession = publishSession;
                                   }

                                   JsonObject user = currentSession.containsKey("user")? currentSession.getJsonObject("user") : new JsonObject();
                                   JsonObject sessionConfig = currentSession.containsKey("config")? currentSession.getJsonObject("config") : new JsonObject();

                                   rawMessage.put("address", "in.sessions.1.bridgingRedirected")
                                           .put("body",  new JsonObject()
                                                   .put("statusCode", statusCode)
                                                   .put("result", result))
                                           .put("headers", new JsonObject()
                                                   .put("origin", "webUser")
                                                   .put("correlationID", publishCorrelationID)
                                            .put("userID", user.getString("id"))
                                            .put("userRoles", user.getJsonArray("roles"))
                                            .put("addressDomain", sessionConfig.getString("addressDomain"))
                                            .put("addressVersion", sessionConfig.getString("addressVersion"))
                                            .put("addressType", sessionConfig.getString("addressType")));

                                   bridgeEvent.complete(true);
                                });

                                switch(address) {
                                    case "in.sessions.1.sessionMissing":
                                        createUserSession(rawMessage, publishWriteHandlerID, bridgeEvent.socket().remoteAddress().host(), interceptionFuture);
                                        break;
                                    case "in.sessions.1.authenticationMissing":
                                        authenticateSession(rawMessage, publishWriteHandlerID, interceptionFuture);
                                        break;
                                    case "in.sessions.1.authenticationPreflight":
                                        preflightAuthenticateSession(rawMessage, interceptionFuture);
                                        break;
                                    case "in.sessions.1.sessionEnded":
                                        closeUserSession(rawMessage, publishWriteHandlerID, interceptionFuture);
                                        break;
                                    default:
                                        rawMessage.put("address", "in.sessions.1.bridgingErrorOccurred")
                                                .put("body", new JsonObject()
                                                        .put("error", new JsonObject()
                                                                .put("code", 404)
                                                                .put("message", "Unknown address: " + address)
                                                                .put("host", bridgeEvent.socket().remoteAddress().host())));
                                        bridgeEvent.complete(true);
                                        break;
                                }
                            }
                            else {
                                // manipulate message to contain all required session information (in particular userID and userRoles)
                                if (publishSession == null || publishSession.isEmpty()) {
                                    // no valid session available
                                    rawMessage.put("address", "in.sessions.1.bridgingErrorOccurred")
                                            .put("body", new JsonObject()
                                                    .put("error", new JsonObject()
                                                            .put("code", 401)
                                                            .put("message", "Must have a session for accessing " + address)
                                                            .put("host", bridgeEvent.socket().remoteAddress().host())));
                                    bridgeEvent.complete(true);
                                    return;
                                }
                                JsonObject user = publishSession.getJsonObject("user");
                                JsonObject config = publishSession.getJsonObject("config");

                                // check if user is allowed to access the address
                                if (!inAuthorities.isEmpty()) {
                                    boolean isAuthorized = false;

                                    // check if user is authorized via role
                                    if (inAuthorities.containsKey("roles")) {
                                        JsonArray roles = user.getJsonArray("roles");
                                        JsonArray authorizedRoles = inAuthorities.getJsonArray("roles");
                                        for (int i = 0; i < roles.size(); i++) {
                                            if (authorizedRoles.contains(roles.getString(i))) {
                                                isAuthorized = true;
                                                break;
                                            }
                                        }
                                    }
                                    // check via sessions
                                    if (!isAuthorized && inAuthorities.containsKey("sessions")) {
                                        JsonArray authorizedSessions = inAuthorities.getJsonArray("sessions");
                                        if (authorizedSessions.contains(user.getString("sessionTokenHash"))) {
                                            isAuthorized = true;
                                        }
                                    }
                                    // check via userID
                                    if (!isAuthorized && inAuthorities.containsKey("users")) {
                                        JsonArray authorizedUsers = inAuthorities.getJsonArray("users");
                                        if (authorizedUsers.contains(user.getString("id"))) {
                                            isAuthorized = true;
                                        }
                                    }

                                    if (!isAuthorized) {
                                        rawMessage.put("address", "in.sessions.1.bridgingErrorOccurred")
                                                .put("body", new JsonObject()
                                                        .put("error", new JsonObject()
                                                                .put("code", 403)
                                                                .put("message", "Not authorized to access " + address)
                                                                .put("host", bridgeEvent.socket().remoteAddress().host())));
                                        bridgeEvent.complete(true);
                                        return;
                                    }
                                }

                                // add user infos to message for services
                                publishHeaders.put("origin", "webUser")
                                        .put("userID", user.getString("id"))
                                        .put("userRoles", user.getJsonArray("roles").encode())
                                        .put("correlationID", publishHeaders.containsKey("correlationID")? publishHeaders.getString("correlationID") : UUID.randomUUID().toString())
                                        .put("addressDomain", config.getString("addressDomain"))
                                        .put("addressVersion", config.getString("addressVersion"))
                                        .put("addressType", config.getString("addressType"));

                                // pass own the message
                                bridgeEvent.complete(true);
                            }
                            break;
                        case RECEIVE:
                            // This event will occur when a message is attempted to be delivered from the server to the client
                            // basically only checking if address is (still) on the whitelist or a response, more fine-grained checking is done at the
                            // register event
                            String type = rawMessage.getString("type");

                            if (address == null || (!outboundWhitelist.containsKey(address) && !"rec".equals(type))) {
                                String correlationID = rawMessage.containsKey("headers") && rawMessage.getJsonObject("headers").containsKey("correlationID")? rawMessage.getJsonObject("headers").getString("correlationID") : UUID.randomUUID().toString();
                                String origin = rawMessage.containsKey("headers") && rawMessage.getJsonObject("headers").containsKey("origin")? rawMessage.getJsonObject("headers").getString("origin") : "unknown";
                                logger.error(correlationID, origin,401, "Address " + address + " is not in the whitelist!");
                                bridgeEvent.complete(false);
                                return;
                            }
                            bridgeEvent.complete(true);
                            break;
                        case REGISTER:
                            // This event will occur when a client attempts to register a handler, make sure
                            // the user is allowed to access this address...
                            if (address == null || !outboundWhitelist.containsKey(address)) {
                                rawMessage.put("address", "in.sessions.1.bridgingErrorOccurred")
                                        .put("body", new JsonObject()
                                                .put("error", new JsonObject()
                                                        .put("code", 403)
                                                        .put("message", "Not authorized to register handler for non-existing or not white-listed address  " + address)
                                                        .put("host", bridgeEvent.socket().remoteAddress().host())));
                                bridgeEvent.complete(true);
                                return;
                            }

                            String registerWriteHandlerID = bridgeEvent.socket().writeHandlerID();
                            JsonObject registerSession = webSocketSessionMap.get(registerWriteHandlerID);
                            JsonObject registerHeaders = rawMessage.containsKey("headers")? rawMessage.getJsonObject("headers") : new JsonObject();
                            String registerCorrelationID = registerHeaders.containsKey("correlationID")? registerHeaders.getString("correlationID") : UUID.randomUUID().toString();
                            registerHeaders.put("correlationID", registerCorrelationID);

                            if (registerSession == null || registerSession.isEmpty()) {
                                // no valid session available
                                rawMessage.put("address", "in.sessions.1.bridgingErrorOccurred")
                                        .put("body", new JsonObject()
                                                .put("error", new JsonObject()
                                                        .put("code", 401)
                                                        .put("message", "Must have a session for registering handler at  " + address)
                                                        .put("host", bridgeEvent.socket().remoteAddress().host())));
                                bridgeEvent.complete(true);
                                return;
                            }

                            JsonObject user = registerSession.getJsonObject("user");
                            JsonObject config = registerSession.getJsonObject("config");
                            String sessionAddress = config.getString("addressDomain") + "." + config.getString("addressVersion") + "." + config.getString("addressType");


                            JsonObject outAuthorities = outboundWhitelist.get(address);
                            if(address.equals(sessionAddress) && outAuthorities.containsKey("sessions") && outAuthorities.getJsonArray("sessions").contains(user.getString("sessionTokenHash"))) {
                                bridgeEvent.complete(true);
                                return;

                                // TODO: what about notifying router/userAuth?
                            }
                            if (!outAuthorities.isEmpty()) {
                                boolean isAuthorized = false;

                                // check if user is authorized via role
                                if (outAuthorities.containsKey("roles")) {
                                    JsonArray roles = user.getJsonArray("roles");
                                    JsonArray authorizedRoles = outAuthorities.getJsonArray("roles");
                                    for (int i = 0; i < roles.size(); i++) {
                                        if (authorizedRoles.contains(roles.getString(i))) {
                                            isAuthorized = true;
                                            break;
                                        }
                                    }
                                }
                                // check via sessions
                                if (!isAuthorized && outAuthorities.containsKey("sessions")) {
                                    JsonArray authorizedSessions = outAuthorities.getJsonArray("sessions");
                                    if (authorizedSessions.contains(user.getString("sessionTokenHash"))) {
                                        isAuthorized = true;
                                    }
                                }
                                // check via userID
                                if (!isAuthorized && outAuthorities.containsKey("users")) {
                                    JsonArray authorizedUsers = outAuthorities.getJsonArray("users");
                                    if (authorizedUsers.contains(user.getString("id"))) {
                                        isAuthorized = true;
                                    }
                                }

                                if (!isAuthorized) {
                                    rawMessage.put("address", "in.sessions.1.bridgingErrorOccurred")
                                            .put("body", new JsonObject()
                                                    .put("error", new JsonObject()
                                                            .put("code", 403)
                                                            .put("message", "Not authorized to register handler at " + address)
                                                            .put("host", bridgeEvent.socket().remoteAddress().host())));
                                    bridgeEvent.complete(true);
                                    return;
                                }

                                bridgeEvent.complete(true);
                            }
                            break;
                        case UNREGISTER:
                            // This event will occur when a client attempts to unregister a handler
                            bridgeEvent.complete(true);
                            break;
                        case SOCKET_CREATED:
                            bridgeEvent.complete(true);
                            break;
                        case SOCKET_CLOSED:
                            String writeHandlerID = bridgeEvent.socket().writeHandlerID();
                            // get session info and remove it, if available
                            JsonObject closeSession = webSocketSessionMap.remove(writeHandlerID);

                            if(closeSession == null || closeSession.isEmpty()) {
                                // session was closed properly before
                                bridgeEvent.complete(true);
                                return;
                            }
                            else {
                                JsonObject closeUser = closeSession.getJsonObject("user");
                                JsonObject closeConfig = closeSession.getJsonObject("config");
                                String addressDomain = closeConfig.getString("addressDomain");
                                String addressVersion = closeConfig.getString("addressVersion");
                                String addressType = closeConfig.getString("addressType");

                                // notify userAuth
                                send("userAuth", "1", "sessionInterrupted", new JsonObject()
                                                .put("sessionTokenHash", closeUser.getString("sessionTokenHash"))
                                        , interruptionReply -> {
                                            if(interruptionReply.failed()) {
                                                logger.error(serviceID, serviceName, interruptionReply.statusCode(), "Failed to mark session by " + closeUser.getString("id") + " as interrupted!", interruptionReply.cause());
                                            }
                                        });

                                // remove bridge
                                Future<JsonObject> bridgeFuture = Future.future();
                                bridgeFuture.setHandler(bridgeResult -> {
                                    if(bridgeResult.failed()) {
                                        logger.error(serviceID, closeUser.getString("id"), 500, "Failed to remove bridge " + addressDomain + "." + addressVersion + "." + addressType + " after session interruption", bridgeResult.cause());
                                    }
                                    else {
                                        JsonObject bridgeResponse = bridgeResult.result();
                                        if (bridgeResponse == null || bridgeResponse.getInteger("statusCode") != 200) {
                                            logger.error(serviceID, closeUser.getString("id"), bridgeResponse.getInteger("statusCode"), "Failed to to remove bridge " + addressDomain + "." + addressVersion + "." + addressType + " after session interruption: " + bridgeResponse);
                                        }
                                    }
                                    bridgeEvent.complete(true);
                                });
                                removeBridge(addressDomain, addressVersion, addressType, bridgeFuture);
                            }
                            break;
                    }
                });

                // serve websocket connections from the eventbus path
                router.route("/eventbus/*").handler(sockJSHandler);
            }

            HttpServerOptions httpServerOptions = new HttpServerOptions();

            // serve public files from the webroot-folder
            if (config().getBoolean("isPublicStorageHTTPServerEnabled")) {
                DateFormat dateTimeFormatter = Utils.createRFC1123DateTimeFormatter();

                router.route("/public/*").handler(routingContext -> {
                    HttpServerRequest request = routingContext.request();
                    HttpMethod httpMethod = request.method();
                    HttpServerResponse response = request.response();

                    if (httpMethod != HttpMethod.GET && httpMethod != HttpMethod.HEAD) {
                        // ignore - is read only
                        routingContext.fail(405);
                        return;
                    }
                    String normalizedPath = Utils.removeDots(Utils.urlDecode(routingContext.normalisedPath(), false));
                    if (normalizedPath == null) {
                        logger.warn(serviceID, serviceName, 400, "Invalid path requesting public storage: " + routingContext.normalisedPath());
                        routingContext.fail(400);
                        return;
                    }

                    String path = Utils.pathOffset(normalizedPath, routingContext);
                    // TODO: serve some kind of index file or allow for directory listing if path == "/"
                    // TODO: use cache for file props, files, ...

                    // get file properties (also checks if file exists)
                    Future<JsonObject> metaDataFuture = Future.future();
                    metaDataFuture.setHandler(metaDataHandler -> {
                        if (metaDataHandler.failed()) {
                            routingContext.fail(404);
                            return;
                        }
                        JsonObject metaData = metaDataHandler.result();

                        // don't allow serving directories
                        if (metaData.getBoolean("isDirectory")) {
                            routingContext.fail(403);
                            return;
                        }

                        MultiMap headers = response.headers();
                        headers.set("date", dateTimeFormatter.format(new Date()));

                        // set caching headers
                        if (config().getJsonObject("publicStorageHTTPServer").getBoolean("isCachingEnabled")) {
                            headers.set("cache-control", "public, max-age=" + config().getJsonObject("publicStorageHTTPServer").getJsonObject("caching").getLong("maxAgeInSeconds"));
                            headers.set("last-modified", dateTimeFormatter.format(metaData.getLong("lastModifiedTime")));
                            if (config().getJsonObject("publicStorageHTTPServer").getJsonObject("caching").getBoolean("isVaryHeaderEnabled") && request.headers().contains("accept-encoding")) {
                                headers.set("vary", "accept-encoding");
                            }
                        }

                        if (httpMethod == HttpMethod.HEAD) {
                            response.end();
                        } else {
                            // guess content type
                            String contentType = MimeMapping.getMimeTypeForFilename(metaData.getString("name"));

                            if (contentType != null) {
                                if (contentType.startsWith("text")) {
                                    response.putHeader("Content-Type", contentType + ";charset=" + Charset.defaultCharset().name());
                                } else {
                                    response.putHeader("Content-Type", contentType);
                                }
                            }

                            // set the response size
                            response.putHeader(HttpHeaders.CONTENT_LENGTH, metaData.getLong("size").toString());

                            Future<Void> retrieveFuture = Future.future();
                            retrieveFuture.setHandler(retrieveHandler -> {
                                if (retrieveHandler.failed()) {
                                    routingContext.fail(500);
                                    logger.error(serviceID, serviceName, 500, "Failed to send public file " + path, retrieveHandler.cause());
                                    return;
                                }
                                response.end();
                            });
                            storage.retrieveFromPublic(path, response, retrieveFuture);
                        }
                    });
                    storage.retrieveMetaDataFromPublic(path, metaDataFuture);
                });

                if (config().getJsonObject("publicStorageHTTPServer").getBoolean("isHTTP2Enabled")) {
                    if (!config().getBoolean("isSSLEnabled")) {
                        logger.fatal(serviceID, serviceName, 400, "Can not enable HTTP2 H2 without enabled SSL! Falling back to HTTP 1.1 only...");
                    }
                    httpServerOptions.setUseAlpn(true);
                    httpServerOptions.setIdleTimeout(config().getJsonObject("publicStorageHTTPServer").getInteger("idleTimeoutInSeconds"));
                }
            }

            // setup SSL
            if (config().getBoolean("isSSLEnabled")) {
                httpServerOptions
                        .setKeyStoreOptions(new JksOptions()
                                .setPath(config().getJsonObject("ssl").getString("keystorePath"))
                                .setPassword(config().getJsonObject("ssl").getString("keystorePassword")))
                        .setSsl(true)
                        .setClientAuth(ClientAuth.NONE);
            } else {
                httpServerOptions.setSsl(false);
                logger.security(serviceID, 400, "SSL is disabled. THIS IS PROBABLY A SEVERE RISK!");
            }

            // listen for configuration changes and minimize the time between receiving updates and first configuration application
            try {
                // register a processor for getting config changes before start consuming, so that they are available as early as possible
                JsonObject configChangedProcessorRecord = new JsonObject()
                        .put("description", "Listens to configuration change notifications from other web bridge instances")
                        .put("domain", serviceName)
                        .put("version", "1")
                        .put("type", "configHasChanged")
                        .put("category", "Processor")
                        .put("provides", new JsonArray())
                        .put("requires", new JsonArray()
                                .add(new JsonObject()
                                        .put("key", "subject")
                                        .put("type", "STRING")
                                        .put("description", "Subject/domain affected by the config change"))
                                .add(new JsonObject()
                                        .put("key", "operation")
                                        .put("type", "STRING")
                                        .put("description", "Type of the change. E.g. 'add', 'delete'"))
                                .add(new JsonObject()
                                        .put("key", "configuration")
                                        .put("type", "JSONObject")
                                        .put("description", "The configuration/parameters required to apply the configuration change"))
                        );


                Method method = this.getClass().getMethod("processConfigHasChanged", Message.class);
                registerProcessor(configChangedProcessorRecord, method, this);
            } catch (NoSuchMethodException e) {
                logger.error(serviceID, serviceName, 500, "Method processConfigHasChanged not found!", e);
                startFuture.fail("processConfigHasChanged method not found!");
                return;
            } catch (Exception e) {
                logger.error(serviceID, serviceName, 500, "Exception when preparing BridgeWebService!", e);
                startFuture.fail("Exception when preparing BridgeWebService!");
                return;
            }
            try {
                // register a processor for getting config changes before start consuming, so that they are available as early as possible
                JsonObject updateProcessorRecord = new JsonObject()
                        .put("description", "Listens to session authentication changes (e.g. blacklist) from other web bridge instances")
                        .put("domain", serviceName)
                        .put("version", "1")
                        .put("type", "authenticationWasUpdated")
                        .put("category", "Processor")
                        .put("provides", new JsonArray())
                        .put("requires", new JsonArray()
                                .add(new JsonObject()
                                        .put("key", "subject")
                                        .put("type", "STRING")
                                        .put("description", "Subject/domain affected by the update, e.g. session"))
                                .add(new JsonObject()
                                        .put("key", "operation")
                                        .put("type", "STRING")
                                        .put("description", "Type of the change. E.g. 'blacklist'"))
                                .add(new JsonObject()
                                        .put("key", "configuration")
                                        .put("type", "JSONObject")
                                        .put("description", "The configuration/parameters required to apply the update, e.g. sessionToken"))
                        );


                Method method = this.getClass().getMethod("processAuthenticationUpdate", Message.class);
                registerProcessor(updateProcessorRecord, method, this);
            } catch (NoSuchMethodException e) {
                logger.error(serviceID, serviceName, 500,"Method processConfigHasChanged not found!", e);
                startFuture.fail("processConfigHasChanged method not found!");
                return;
            } catch (Exception e) {
                logger.error("Exception when preparing BridgeWebService!", e);
                startFuture.fail("Exception when preparing BridgeWebService!");
                return;
            }

            // apply the configuration from the shared data
            applyConfigurationFromSharedData(false);

            // start Server
            Integer port = config().getInteger("port");
            httpServer = vertx.createHttpServer(httpServerOptions).requestHandler(router::accept).listen(port, result -> {
                if (result.failed()) {
                    logger.fatal(serviceID, serviceName, 500,"Failed to start HTTP Server!", result.cause());
                    startFuture.fail(result.cause());
                } else {
                    logger.info(serviceID, serviceName, 201, "HTTP server started on port " + port);
                    startFuture.complete();
                }
            });


        }
        catch (Exception e) {
            e.printStackTrace();
            startFuture.fail(e);
        }
    }

    private void createUserSession(JsonObject rawMessage, String writeHandlerID, String sessionSource, Future<JsonObject> future) {
        try {
            // check if the connection already has a session. This can happen in case of client-side race conditions. However, this is not a perfect solution.
            // e.g. if auth is provided by script so quickly after JS initialization that the previous automatic anonymous session setup failed
            JsonObject body = rawMessage.getJsonObject("body");
            if(body == null || body.isEmpty()) {
                future.complete(new JsonObject().put("statusCode", 400).put("message", "Missing message body"));
                return;
            }

            if(writeHandlerID != null && !writeHandlerID.isEmpty()) {
                JsonObject session = webSocketSessionMap.get(writeHandlerID);

                if(session != null && session.containsKey("user") && session.getJsonObject("user").containsKey("id")) {
                    String userID = session.getJsonObject("user").getString("id");
                    if("anonymous".equals(userID) && body.containsKey("authType") && body.containsKey("authConfig")) {
                        authenticateSession(rawMessage, writeHandlerID, future);
                    }
                    else {
                        // can neither upgrade anonymous session (no auth info provided) nor switch user (if user not anonymous)
                        future.complete(new JsonObject().put("statusCode", 200).put("result", session));
                    }
                    return;
                }
            }

            Long timestamp = System.currentTimeMillis();

            // ensure that there is a valid correlation ID
            JsonObject headers = rawMessage.getJsonObject("headers");
            if(headers == null) {
                headers = new JsonObject();
                rawMessage.put("headers", headers);
            }
            String correlationID = headers.getString("correlationID") != null? headers.getString("correlationID") : UUID.randomUUID().toString();
            headers.put("correlationID", correlationID);

            // create a new session token and a hash of it
            String sessionToken = Base64.getEncoder().encodeToString((UUID.randomUUID().toString() + sessionSource + timestamp).getBytes());
            JsonObject sessionTokenHashConfig = getHashKeyWithHashConfig(sessionToken);
            String sessionTokenHash = sessionTokenHashConfig.getString("hash");
            sessionTokenHashConfig.remove("hash");

            // check and prepare the context and config data
            JsonObject sessionContext = body.getJsonObject("sessionContext");
            if(sessionContext == null || sessionContext.isEmpty()) {
                future.complete(new JsonObject().put("statusCode", 400).put("message", "Missing required parameter/payload: non-empty sessionContext"));
                return;
            }

            String pathName = sessionContext.getString("pathName");
            String routerDomain = sessionContext.getString("routerDomain");
            String routerVersion = sessionContext.getString("routerVersion");
            String routerType = sessionContext.getString("routerType");
            JsonObject navigator = sessionContext.getJsonObject("navigator");
            JsonObject device = sessionContext.getJsonObject("device");
            JsonObject connection = sessionContext.getJsonObject("connection");

            if (pathName == null || pathName.isEmpty()) {
                future.complete(new JsonObject().put("statusCode", 400).put("message", "Missing required parameter/payload: non-empty pathName"));
                return;
            }
            if (routerDomain == null || routerDomain.isEmpty()) {
                future.complete(new JsonObject().put("statusCode", 400).put("message", "Missing required parameter/payload: non-empty routerDomain"));
                return;
            }
            if (routerVersion == null || routerVersion.isEmpty()) {
                future.complete(new JsonObject().put("statusCode", 400).put("message", "Missing required parameter/payload: non-empty routerVersion"));
                return;
            }
            if (routerType == null || routerType.isEmpty()) {
                future.complete(new JsonObject().put("statusCode", 400).put("message", "Missing required parameter/payload: non-empty routerType"));
                return;
            }
            if (navigator == null) {
                future.complete(new JsonObject().put("statusCode", 400).put("message", "Missing required parameter/payload: navigator"));
                return;
            }
            if (device == null) {
                future.complete(new JsonObject().put("statusCode", 400).put("message", "Missing required parameter/payload: device"));
                return;
            }
            if (connection == null) {
                future.complete(new JsonObject().put("statusCode", 400).put("message", "Missing required parameter/payload: connection"));
                return;
            }


            JsonObject session = new JsonObject();
            JsonObject user = new JsonObject();
            JsonObject sessionConfig = new JsonObject()
                    .put("addressDomain", "out.sessions")
                    .put("addressVersion", "1")
                    .put("addressType", sessionTokenHash)
                    .put("source", sessionSource)
                    .put("startTimestamp", timestamp)
                    .put("isBackendLoggingEnabled", isBackendLoggingEnabled)
                    .put("backendLoggingLevel", backendLoggingLevel);
            session.put("user", user)
                    .put("config", sessionConfig)
                    .put("context", sessionContext)
                    .put("sessionTokenHash", sessionTokenHash)
                    .put("writeHandlerID", writeHandlerID);

            // set traits (later version: adjust traits after auth)
            JsonObject traits = new JsonObject();
            if (config().getBoolean("areServicesAllowedToSetClientTraitConfigs")) {
                clientTraitMap.forEach((key, value) -> {
                    String navigatorUserAgent = navigator != null && navigator.containsKey("userAgent") ? navigator.getString("userAgent") : "";
                    String deviceOsCpu = device != null && device.containsKey("oscpu") ? device.getString("oscpu") : "";
                    String connectionType = connection != null && connection.containsKey("type") ? connection.getString("type") : "";

                    String router = routerDomain + "." + routerVersion + "." + routerType;
                    if ((".*".equals(router) || router.matches(value.getString("routerRegex")))
                            && (".*".equals(value.getString("navigatorUserAgentRegex")) || navigatorUserAgent.matches(value.getString("navigatorUserAgentRegex")))
                            && (".*".equals(value.getString("deviceOsCpuRegex")) || deviceOsCpu.matches(value.getString("deviceOsCpuRegex")))
                            && (".*".equals(value.getString("connectionTypeRegex")) || connectionType.matches(value.getString("connectionTypeRegex")))
                            ) {
                        traits.put(value.getString("traitKey"), value.getBoolean("traitValue"));
                    }
                });
            }
            sessionConfig.put("traits", traits);

            // after session has been created, a bridge is created and the session saved to this bridge instance
            Future<JsonObject> bridgeFuture = Future.future();
            bridgeFuture.setHandler(bridgeResult -> {
                try {
                    if (bridgeResult.failed()) {
                        logger.error(correlationID, sessionSource, 500, "Failed to create bridge for user session", bridgeResult.cause());
                        future.complete(new JsonObject().put("statusCode", 500).put("message", "Unknown error occurred"));

                        // end created session again...
                        send("userAuth", "1", "sessionEnded", new JsonObject()
                                .put("sessionTokenHash", sessionTokenHash), endReply -> {
                            if(endReply.failed()) {
                                logger.error(correlationID, sessionSource, 500, "Failed to end session " + sessionTokenHash + " after bridging error!", endReply.cause());
                            }
                        });
                        return;
                    }

                    JsonObject bridgeInfo = bridgeResult.result();
                    Integer bridgeStatusCode = bridgeInfo.getInteger("statusCode");
                    if (bridgeStatusCode != 200) {
                        logger.error(correlationID, sessionSource, 500,  "Failed to create bridge for user session. " + bridgeStatusCode + ": " + bridgeInfo.getString("message"));
                        future.complete(new JsonObject().put("statusCode", 500).put("message", "Unknown error occurred"));

                        // end created session again...
                        send("userAuth", "1", "sessionEnded", new JsonObject()
                                .put("sessionTokenHash", sessionTokenHash), endReply -> {
                            if(endReply.failed()) {
                                logger.error(correlationID, sessionSource, 500, "Failed to end session " + sessionTokenHash + " after bridging error!", endReply.cause());
                            }
                        });
                        return;
                    }

                    sessionMap.put(user.getString("sessionTokenHash"), session);
                    webSocketSessionMap.put(writeHandlerID, session);

                    future.complete(new JsonObject().put("statusCode", 200).put("result", session));
                } catch (Exception e) {
                    logger.error(correlationID, sessionSource, 500, "Error when creating session bridge", e);
                    future.complete(new JsonObject().put("statusCode", 500).put("message", "Unknown error occurred"));
                }
            });

            // create anonymous session in any case first
            send("userAuth", "1", "sessionRegistrationMissing", new JsonObject()
                            .put("sessionTokenHash", sessionTokenHash)
                            .put("sessionTokenHashConfig", sessionTokenHashConfig)
                            .put("sessionContext", sessionContext)
                            .put("sessionConfig", sessionConfig)
                    , sessionReply -> {
                        if (sessionReply.failed()) {
                            logger.error(correlationID, sessionSource, 500, "Failed to create session", sessionReply.cause());
                            future.complete(new JsonObject().put("statusCode", 500).put("message", "Unknown error occurred"));
                            return;
                        }
                        if (sessionReply.statusCode() != 200) {
                            logger.error(correlationID, sessionSource, sessionReply.statusCode(), "Failed to create session: " + sessionReply.getMessage());
                            future.complete(new JsonObject().put("statusCode", sessionReply.statusCode()).put("message", sessionReply.getMessage()));
                            return;
                        }

                        JsonObject result = sessionReply.getBodyAsJsonObject();
                        user.put("sessionToken", sessionToken)
                                .put("sessionTokenHash", sessionTokenHash)
                                .put("id", result.getString("userID"))
                                .put("roles", result.getJsonArray("userRoles").add("anonymous"));

                        // check if authorization is possible
                        String authType = body.getString("authType");
                        if(authType != null && "sessionToken".equals(authType)) {
                            // a previous session token is available - use it
                            JsonObject authConfig = body.getJsonObject("authConfig");
                            if(authConfig == null || authConfig.isEmpty()) {
                                // return the anonymous session
                                addBridge(sessionConfig.getString("addressDomain"), sessionConfig.getString("addressVersion"), sessionConfig.getString("addressType"), false, true, new JsonObject().put("sessions", new JsonArray().add(sessionTokenHash)), bridgeFuture);
                                return;
                            }
                            String previousSessionToken = authConfig.getString("sessionToken");
                            if(previousSessionToken == null || previousSessionToken.isEmpty()) {
                                // return the anonymous session
                                addBridge(sessionConfig.getString("addressDomain"), sessionConfig.getString("addressVersion"), sessionConfig.getString("addressType"), false, true, new JsonObject().put("sessions", new JsonArray().add(sessionTokenHash)), bridgeFuture);
                                return;
                            }
                            // in a later version this should make sure the same hashing mechanism is being used...
                            String previousSessionTokenHash = getHashKey(previousSessionToken);

                            // authenticate session
                            send("userAuth", "1", "sessionAuthenticationMissing", new JsonObject()
                                            .put("sessionTokenHash", sessionTokenHash)
                                            .put("authMethodConfig", new JsonObject()
                                                    .put("type", "sessionToken")
                                                    .put("sessionTokenHash", previousSessionTokenHash))
                                    , authReply -> {
                                        if(authReply.failed() || authReply.statusCode() != 200) {
                                            // return the anonymous session
                                            addBridge(sessionConfig.getString("addressDomain"), sessionConfig.getString("addressVersion"), sessionConfig.getString("addressType"), false, true, new JsonObject().put("sessions", new JsonArray().add(sessionTokenHash)), bridgeFuture);
                                            return;
                                        }

                                        JsonObject authResult = authReply.getBodyAsJsonObject();
                                        user.put("id", authResult.getString("userID"))
                                                .put("roles", authResult.getJsonArray("userRoles"));
                                        addBridge(sessionConfig.getString("addressDomain"), sessionConfig.getString("addressVersion"), sessionConfig.getString("addressType"), false, true, new JsonObject().put("sessions", new JsonArray().add(sessionTokenHash)), bridgeFuture);
                                    });
                        }
                        else {
                            // return the anonymous session
                            addBridge(sessionConfig.getString("addressDomain"), sessionConfig.getString("addressVersion"), sessionConfig.getString("addressType"), false, true, new JsonObject().put("sessions", new JsonArray().add(sessionTokenHash)), bridgeFuture);
                        }
                    });
        }
        catch(Exception e) {
            logger.error(serviceID, sessionSource, 500, "Unknown error occurred when starting session", e);
            future.complete(new JsonObject().put("statusCode", 500).put("message", "Unknown error occurred"));
            return;
        }
    }

    private void closeUserSession(JsonObject rawMessage, String writeHandlerID, Future<JsonObject> future) {
        JsonObject headers = rawMessage.getJsonObject("headers");
        if(headers == null) {
            headers = new JsonObject();
            rawMessage.put("headers", headers);
        }
        String correlationID = headers.containsKey("correlationID")? headers.getString("correlationID") : UUID.randomUUID().toString();
        headers.put("correlationID", correlationID);

        if(writeHandlerID == null || writeHandlerID.isEmpty()) {
            future.fail("Must provide non-empty writeHandlerID!");
            return;
        }

        JsonObject session = webSocketSessionMap.remove(writeHandlerID);
        if(session == null || session.isEmpty()) {
            logger.warn(correlationID, 404, "Tried to close session which does not exist!");
            future.complete(new JsonObject().put("statusCode", 404).put("message", "Session not found"));
            return;
        }

        JsonObject user = session.getJsonObject("user");
        JsonObject config = session.getJsonObject("config");
        String addressDomain = config.getString("addressDomain");
        String addressVersion = config.getString("addressVersion");
        String addressType = config.getString("addressType");

        // notify userAuth
        send("userAuth", "1", "sessionEnded", correlationID, new JsonObject()
                        .put("sessionTokenHash", user.getString("sessionTokenHash"))
            , endReply -> {
                if(endReply.failed()) {
                    logger.error(correlationID, user.getString("id"), endReply.statusCode(), "Failed to mark session as closed!", endReply.cause());
                }

                // remove bridge
                Future<JsonObject> bridgeFuture = Future.future();
                bridgeFuture.setHandler(bridgeResult -> {
                    if(bridgeResult.failed()) {
                        logger.error(correlationID, user.getString("id"), 500, "Failed to remove bridge " + addressDomain + "." + addressVersion + "." + addressType + " after session interruption", bridgeResult.cause());
                    }
                    else {
                        JsonObject bridgeResponse = bridgeResult.result();
                        if (bridgeResponse == null || bridgeResponse.getInteger("statusCode") != 200) {
                            logger.error(correlationID, user.getString("id"), bridgeResponse.getInteger("statusCode"), "Failed to to remove bridge " + addressDomain + "." + addressVersion + "." + addressType + " after session interruption: " + bridgeResponse);
                        }
                    }

                    future.complete(new JsonObject().put("statusCode", 200));
                });
                removeBridge(addressDomain, addressVersion, addressType, bridgeFuture);
            });
    }


    private void preflightAuthenticateSession(JsonObject rawMessage, Future<JsonObject> future) {
        try {
            JsonObject headers = rawMessage.getJsonObject("headers");
            if(headers == null) {
                headers = new JsonObject();
                rawMessage.put("headers", headers);
            }
            String correlationID = headers.containsKey("correlationID")? headers.getString("correlationID") : UUID.randomUUID().toString();
            headers.put("correlationID", correlationID);

            JsonObject body = rawMessage.getJsonObject("body");
            if(body == null || body.isEmpty()) {
                future.complete(new JsonObject().put("statusCode", 400).put("message", "Missing message body"));
                return;
            }

            String authType = body.getString("authType");
            if(authType == null || authType.isEmpty()) {
                future.complete(new JsonObject().put("statusCode", 400).put("message", "Must provide non-empty authType like 'email' or 'sessionToken'!"));
                return;
            }

            JsonObject authConfig = body.getJsonObject("authConfig");
            if(authConfig == null || authConfig.isEmpty()) {
                future.complete(new JsonObject().put("statusCode", 400).put("message", "Must provide non-empty authConfig!"));
                return;
            }

            if("email".equals(authType)) {
                String emailAddress = authConfig.getString("emailAddress");
                if (emailAddress == null || emailAddress.isEmpty()) {
                    future.complete(new JsonObject().put("statusCode", 400).put("message", "Must provide non-empty authConfig.emailAddress!"));
                    return;
                }
                // verify account existence via getting password hash config
                // this exposes valid email addresses and might needs to be addressed later
                Future<Message> authMethodFuture = Future.future();
                authMethodFuture.setHandler(configReply -> {
                    if(configReply.failed()) {
                        future.complete(new JsonObject().put("statusCode", 404).put("message", "Failed to preflight session authentication. Email address probably unknown/not linked to a valid account!"));
                        logger.error(correlationID, emailAddress, 404, "Failed to get auth method config from userAuth during session authentication preflight!", configReply.cause());
                        return;
                    }

                    future.complete(new JsonObject().put("statusCode", 202).put("message", "Preflight session authentication successful"));
                    // TODO: request/cache current application state
                });
                sessionAuthenticationMethodCache.get(emailAddress, "userAuth", "1", "authMethodConfigMissing", serviceName, correlationID, new JsonObject()
                        .put("authMethodIdentifier", new JsonObject()
                                .put("type", "email")
                                .put("emailAddress", emailAddress)), 30000l, authMethodFuture);
            }
            else {
                future.complete(new JsonObject().put("statusCode", 400).put("message", "authType " + authType + " unknown/not supported!"));
                return;
            }
        }
        catch(Exception e) {
            logger.security(serviceID, "Unknown error occurred when prelighting authenticating session", e);
            future.complete(new JsonObject().put("statusCode", 500).put("message", "Unknown error occurred"));
            return;
        }
    }

    private void authenticateSession(JsonObject rawMessage, String writeHandlerID, Future<JsonObject> future) {
        try {
            JsonObject headers = rawMessage.getJsonObject("headers");
            if(headers == null) {
                headers = new JsonObject();
                rawMessage.put("headers", headers);
            }
            String correlationID = headers.containsKey("correlationID")? headers.getString("correlationID") : UUID.randomUUID().toString();
            headers.put("correlationID", correlationID);

            JsonObject body = rawMessage.getJsonObject("body");
            if(body == null || body.isEmpty()) {
                future.complete(new JsonObject().put("statusCode", 400).put("message", "Missing message body"));
                return;
            }

            if(writeHandlerID == null || writeHandlerID.isEmpty()) {
                future.fail("Must provide non-empty writeHandlerID!");
                return;
            }

            JsonObject session = webSocketSessionMap.get(writeHandlerID);
            if(session == null || session.isEmpty()) {
                logger.warn(correlationID, 404, "Tried to authenticate session which does not exist!");
                future.complete(new JsonObject().put("statusCode", 404).put("message", "Session not found"));
                return;
            }

            String currentUserID = session.getJsonObject("user").getString("id");
            if(!"anonymous".equals(currentUserID)) {
                future.complete(new JsonObject().put("statusCode", 200).put("result", new JsonObject().put("userID", session.getJsonObject("user").getString("id")).put("userRoles", session.getJsonObject("user").getJsonArray("roles"))));
                return;
            }

            String authType = body.getString("authType");
            if(authType == null || authType.isEmpty()) {
                future.complete(new JsonObject().put("statusCode", 400).put("message", "Must provide non-empty authType like 'email' or 'sessionToken'!"));
                return;
            }

            JsonObject authConfig = body.getJsonObject("authConfig");
            if(authConfig == null || authConfig.isEmpty()) {
                future.complete(new JsonObject().put("statusCode", 400).put("message", "Must provide non-empty authConfig!"));
                return;
            }

            if("email".equals(authType)) {
                String emailAddress = authConfig.getString("emailAddress");
                if(emailAddress == null || emailAddress.isEmpty()) {
                    future.complete(new JsonObject().put("statusCode", 400).put("message", "Must provide non-empty authConfig.emailAddress!"));
                    return;
                }
                String password = authConfig.getString("password");
                if(password == null || password.isEmpty()) {
                    future.complete(new JsonObject().put("statusCode", 400).put("message", "Must provide non-empty authConfig.password!"));
                    return;
                }

                // get password hash config
                Future<Message> authMethodFuture = Future.future();
                authMethodFuture.setHandler(configReply -> {
                    if(configReply.failed()) {
                        future.complete(new JsonObject().put("statusCode", 500).put("message", "Failed to get auth method config from userAuth!"));
                        logger.error(correlationID, emailAddress, 500, "Failed to get auth method config from userAuth!", configReply.cause());
                        return;
                    }

                    JsonObject hashConfig = configReply.result().getBodyAsJsonObject().getJsonObject("authMethodConfig");
                    String passwordHash = getHash(password, hashConfig);


                    send("userAuth", "1", "sessionAuthenticationMissing", new JsonObject()
                                    .put("sessionTokenHash", session.getString("sessionTokenHash"))
                                    .put("authMethodConfig", hashConfig
                                            .put("type", "email")
                                            .put("emailAddress", emailAddress)
                                            .put("passwordHash", passwordHash))
                            , authReply -> {
                                if(authReply.failed()) {
                                    future.complete(new JsonObject().put("statusCode", 500).put("message", "Authenticating session failed!"));
                                    logger.warn(correlationID, currentUserID, 500, "Failed to authenticated anonymous session!", authReply.cause());
                                    return;
                                }
                                if(authReply.statusCode() != 200) {
                                    future.complete(new JsonObject().put("statusCode", authReply.statusCode()).put("message", "Authenticating session failed: " + authReply.getMessage()));
                                    logger.warn(correlationID, currentUserID,  authReply.statusCode(), "Failed to authenticated anonymous session: " + authReply.getMessage());
                                    return;
                                }

                                JsonObject authResult = authReply.getBodyAsJsonObject();
                                String userID = authResult.getString("userID");
                                JsonArray userRoles = authResult.getJsonArray("userRoles");

                                // update session info
                                session.getJsonObject("user")
                                        .put("id", userID)
                                        .put("roles", userRoles);

                                future.complete(new JsonObject().put("statusCode", 200).put("result", new JsonObject().put("id", userID)
                                        .put("roles", userRoles)));
                            });
                });
                sessionAuthenticationMethodCache.get(emailAddress, "userAuth", "1", "authMethodConfigMissing", serviceName, correlationID, new JsonObject()
                        .put("authMethodIdentifier", new JsonObject()
                                .put("type", "email")
                                .put("emailAddress", emailAddress)), 30000l, authMethodFuture);
            }
            else if("sessionToken".equals(authType)) {
                String previousSessionToken = authConfig.getString("sessionToken");
                if(previousSessionToken == null || previousSessionToken.isEmpty()) {
                    future.complete(new JsonObject().put("statusCode", 400).put("message", "Must provide non-empty authConfig.sessionToken!"));
                    return;
                }
                // in a later version this should make sure the same hashing mechanism is being used...
                String previousSessionTokenHash = getHashKey(previousSessionToken);

                // authenticate session
                send("userAuth", "1", "sessionAuthenticationMissing", correlationID, new JsonObject()
                                .put("sessionTokenHash", session.getString("sessionTokenHash"))
                                .put("authMethodConfig", new JsonObject()
                                        .put("type", "sessionToken")
                                        .put("sessionTokenHash", previousSessionTokenHash))
                        , authReply -> {
                            if(authReply.failed()) {
                                future.complete(new JsonObject().put("statusCode", 500).put("message", "Authenticating session failed!"));
                                logger.warn(correlationID, currentUserID, 500, "Failed to authenticated anonymous session!", authReply.cause());
                                return;
                            }
                            if(authReply.statusCode() != 200) {
                                future.complete(new JsonObject().put("statusCode", authReply.statusCode()).put("message", "Authenticating session failed: " + authReply.getMessage()));
                                logger.warn(correlationID, currentUserID,  authReply.statusCode(), "Failed to authenticated anonymous session: " + authReply.getMessage());
                                return;
                            }

                            JsonObject authResult = authReply.getBodyAsJsonObject();
                            String userID = authResult.getString("userID");
                            JsonArray userRoles = authResult.getJsonArray("userRoles");

                            // update session info
                            session.getJsonObject("user")
                                .put("id", userID)
                                .put("roles", userRoles);

                            future.complete(new JsonObject().put("statusCode", 200).put("result", new JsonObject().put("id", userID)
                                    .put("roles", userRoles)));
                        });
            }
            else {
                future.complete(new JsonObject().put("statusCode", 404).put("message", "authType " + authType + " unknown/not supported!"));
                return;
            }
        }
        catch(Exception e) {
            logger.security(serviceID, "Unknown error occurred when authenticating user", e);
            future.complete(new JsonObject().put("statusCode", 500).put("message", "Unknown error occurred"));
            return;
        }
    }

    private String getHashKey(String identifier) {
        try {
            PBEKeySpec pbeKeySpec = new PBEKeySpec(identifier.toCharArray(), saltBytes, config().getJsonObject("hashPolicy").getInteger("iterations"), config().getJsonObject("hashPolicy").getInteger("length"));
            SecretKey key = secretKeyFactory.generateSecret(pbeKeySpec);
            byte[] res = key.getEncoded( );
            // remove characters not supported by ArangoDB
            return java.util.Base64.getEncoder().encodeToString(res).replace("/", "_");
        }
        catch (InvalidKeySpecException e) {
            logger.error(serviceID, serviceName, 500, "Failed to compute hash key", e);
            return null;
        }
    }


    private String getHash(String identifier, JsonObject hashConfig) {
        try {
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(hashConfig.getString("hashFactory"));
            PBEKeySpec pbeKeySpec = new PBEKeySpec(identifier.toCharArray(), hashConfig.getString("hashSalt").getBytes("UTF-8"), hashConfig.getInteger("hashIterations"), hashConfig.getInteger("hashLength"));
            SecretKey key = secretKeyFactory.generateSecret(pbeKeySpec);
            byte[] res = key.getEncoded( );
            // remove characters not supported by ArangoDB
            return java.util.Base64.getEncoder().encodeToString(res);
        }
        catch (InvalidKeySpecException e) {
            logger.error(serviceID, serviceName, 500, "Failed to compute hash key", e);
            return null;
        } catch (NoSuchAlgorithmException e) {
            logger.error(serviceID, serviceName, 500, "Failed to compute hash key", e);
            return null;
        } catch (UnsupportedEncodingException e) {
            logger.error(serviceID, serviceName, 500, "Failed to compute hash key", e);
            return null;
        }
    }

    private JsonObject getHashWithHashConfig(String identifier) {
        try {
            JsonObject result = new JsonObject()
                    .put("hashFactory", config().getJsonObject("hashPolicy").getString("factory"))
                    .put("hashSalt", config().getJsonObject("hashPolicy").getString("salt"))
                    .put("hashIterations", config().getJsonObject("hashPolicy").getInteger("iterations"))
                    .put("hashLength",  config().getJsonObject("hashPolicy").getInteger("length"));

            PBEKeySpec pbeKeySpec = new PBEKeySpec(identifier.toCharArray(), saltBytes, result.getInteger("hashIterations"), result.getInteger("hashLength"));
            SecretKey key = secretKeyFactory.generateSecret(pbeKeySpec);
            byte[] res = key.getEncoded( );
            return result.put("hash", java.util.Base64.getEncoder().encodeToString(res));
        }
        catch (InvalidKeySpecException e) {
            logger.error(serviceID, serviceName, 500, "Failed to compute hash key", e);
            return null;
        }
    }

    private JsonObject getHashKeyWithHashConfig(String identifier) {
        JsonObject result = getHashWithHashConfig(identifier);
        String hash = result.getString("hash");
        if(hash != null) {
            result.put("hash",hash.replace("/", "_"));
        }
        return result;
    }

    // is registered manually so it's available before start consuming
    public void processConfigHasChanged(Message message) {
        JsonObject body = message.getBodyAsJsonObject();
        String correlationID = message.correlationID();
        // note as the updates are send via publish, also own updates will arrive...
        applyConfigurationUpdate(body, correlationID);
    }

    private void applyConfigurationFromSharedData(Boolean reportMismatches) {
        // TODO: if reportMismatches, then sync and log inconsistencies!

        List<JsonObject> configToApplyList = new LinkedList<>();
        configToApplyList.add(new JsonObject()
            .put("mapName", serviceName + "_availableBridges")
            .put("subject", "bridge")
        );

        if(config().containsKey("isPublicStorageHTTPServerEnabled") && config().getBoolean("isPublicStorageHTTPServerEnabled")) {
            configToApplyList.add(new JsonObject()
                    .put("mapName", serviceName + "_publicFilesAliases")
                    .put("subject", "publicFilesAlias")
            );
            configToApplyList.add(new JsonObject()
                    .put("mapName", serviceName + "_publicFilesAliasesWithNotification")
                    .put("subject", "publicFilesAliasWithNotification")
            );
            configToApplyList.add(new JsonObject()
                    .put("mapName", serviceName + "_publicFilesRegexAliases")
                    .put("subject", "publicFilesRegexAlias")
            );
        }
        if(config().containsKey("areServicesAllowedToSetClientTraitConfigs") && config().getBoolean("areServicesAllowedToSetClientTraitConfigs")) {
            configToApplyList.add(new JsonObject()
                    .put("mapName", serviceName + "_clientTraits")
                    .put("subject", "clientTrait")
            );
        }


        if(vertx.isClustered()) {
            for(JsonObject configToApply : configToApplyList) {
                sharedData.<String, JsonObject>getClusterWideMap(configToApply.getString("mapName"), mapResult -> {
                    if (mapResult.failed()) {
                        logger.fatal(serviceID, serviceName, 500, "Failed to obtain map " + configToApply.getString("mapName"), mapResult.cause());
                        return;
                    }
                    AsyncMap<String, JsonObject> map = mapResult.result();
                    map.values(valueHandler -> {
                        if(valueHandler.failed()) {
                            logger.fatal(serviceID, serviceName, 500, "Failed to obtain map values from " + configToApply.getString("mapName"), valueHandler.cause());
                            return;
                        }
                        List<JsonObject> values = valueHandler.result();
                        for(JsonObject configuration : values) {
                            applyConfigurationUpdate(new JsonObject()
                                    .put("subject", configToApply.getString("subject"))
                                    .put("operation", "add")
                                    .put("configuration", configuration), "Unknown CID");
                        }
                    });
                });
            }
        }
        else {
            for(JsonObject configToApply : configToApplyList) {
                LocalMap<String, JsonObject> map = sharedData.getLocalMap(configToApply.getString("mapName"));
                for(String key : map.keySet()) {
                    JsonObject configuration = map.get(key);
                    applyConfigurationUpdate(new JsonObject()
                            .put("subject", configToApply.getString("subject"))
                            .put("operation", "add")
                            .put("configuration", configuration), "Unknown CID");
                }
            }
        }
    }

    private void applyConfigurationUpdate(JsonObject updateConfiguration, String correlationID) {
        String subject = updateConfiguration.getString("subject");
        String operation = updateConfiguration.getString("operation");
        JsonObject config = updateConfiguration.getJsonObject("configuration");

        // TODO: implement overwrite if config different than stored - currently it only checks if there is an entry with the same key

        Future<JsonObject> updateFuture = Future.future();
        updateFuture.setHandler(updateHandler -> {
            if(updateHandler.failed()) {
                logger.fatal(correlationID, serviceName, 500, "Failed to update configuration: " + updateConfiguration.encodePrettily(), updateHandler.cause());
                return;
            }
            JsonObject result = updateHandler.result();
            if(result.getInteger("statusCode") != 200) {
                logger.fatal(correlationID, serviceName, 500, "Failed to update configuration: " + updateConfiguration.encodePrettily() + ". Code: " + result.getInteger("statusCode") + ". Message: " + result.getString("message"));
                return;
            }
        });

        if("bridge".equals(subject)) {
            if("add".equals(operation)) {
                addBridge(config.getString("domain"), config.getString("version"), config.getString("type"), config.getBoolean("isInbound"), config.getBoolean("isOutbound"), config.getJsonObject("authorities"), updateFuture);
            }
            else if("delete".equals(operation)) {
                removeBridge(config.getString("domain"), config.getString("version"), config.getString("type"), updateFuture);
            }
            else {
                logger.error(correlationID, serviceName, 404, "Received a configuration update with an unsupported subject/operation combination: " + updateConfiguration.encodePrettily());
            }
        }
        else if("publicFilesAliasWithNotification".equals(subject)) {
            if(config().containsKey("isPublicStorageHTTPServerEnabled") && config().getBoolean("isPublicStorageHTTPServerEnabled")) {
                if ("add".equals(operation)) {
                    String aliasParameterPrefix = config.getString("aliasParameterPrefix");
                    String aliasMarker = config.getString("aliasMarker");
                    String fullAlias = aliasParameterPrefix + aliasMarker;

                    addPublicFilesAliasWithNotification(config.getString("path"), aliasParameterPrefix, aliasMarker, fullAlias, config.getString("domain"), config.getString("version"), config.getString("type"), updateFuture);
                } else {
                    logger.error(correlationID, serviceName, 500, "Received a configuration update with an unsupported subject/operation combination: " + updateConfiguration.encodePrettily());
                }
            }
        }
        else if("publicFilesAliasWithClearance".equals(subject)) {
            if(config().containsKey("isPublicStorageHTTPServerEnabled") && config().getBoolean("isPublicStorageHTTPServerEnabled")) {
                if ("add".equals(operation)) {
                    String aliasParameterPrefix = config.getString("aliasParameterPrefix");
                    String aliasMarker = config.getString("aliasMarker");
                    String fullAlias = aliasParameterPrefix + aliasMarker;

                    addPublicFilesAliasWithClearance(config.getString("path"), aliasParameterPrefix, aliasMarker, fullAlias, config.getString("domain"), config.getString("version"), config.getString("type"), updateFuture);
                } else {
                    logger.error(correlationID, serviceName, 500, "Received a configuration update with an unsupported subject/operation combination: " + updateConfiguration.encodePrettily());
                }
            }
        }
        else if("publicFilesAliasWithDynamicPath".equals(subject)) {
            if(config().containsKey("isPublicStorageHTTPServerEnabled") && config().getBoolean("isPublicStorageHTTPServerEnabled")) {
                if ("add".equals(operation)) {
                    String aliasParameterPrefix = config.getString("aliasParameterPrefix");
                    String aliasMarker = config.getString("aliasMarker");
                    String fullAlias = aliasParameterPrefix + aliasMarker;

                    addPublicFilesAliasWithDynamicPath(aliasParameterPrefix, aliasMarker, fullAlias, config.getString("domain"), config.getString("version"), config.getString("type"), updateFuture);
                } else {
                    logger.error(correlationID, serviceName, 500, "Received a configuration update with an unsupported subject/operation combination: " + updateConfiguration.encodePrettily());
                }
            }
        }
        else if("publicFilesAlias".equals(subject)) {
            if(config().containsKey("isPublicStorageHTTPServerEnabled") && config().getBoolean("isPublicStorageHTTPServerEnabled")) {
                if ("add".equals(operation)) {
                    addPublicFilesAlias(config.getString("path"), config.getString("alias"), updateFuture);
                } else {
                    logger.error(correlationID, serviceName, 500,  "Received a configuration update with an unsupported subject/operation combination: " + updateConfiguration.encodePrettily());
                }
            }
        }
        else if("publicFilesRegexAlias".equals(subject)) {
            if(config().containsKey("isPublicStorageHTTPServerEnabled") && config().getBoolean("isPublicStorageHTTPServerEnabled")) {
                if ("add".equals(operation)) {
                    String aliasRegex = config.getString("aliasRegexPrefix");
                    String aliasMarker = config.getString("aliasMarker");
                    String fullAliasRegex = aliasRegex + aliasMarker;
                    addPublicFilesRegexAlias(config.getString("path"), aliasRegex, aliasMarker, fullAliasRegex, updateFuture);
                } else {
                    logger.error(correlationID, serviceName, 500, "Received a configuration update with an unsupported subject/operation combination: " + updateConfiguration.encodePrettily());
                }
            }
        }
        else if("clientTrait".equals(subject)) {
            if(config().containsKey("areServicesAllowedToSetClientTraitConfigs") && config().getBoolean("areServicesAllowedToSetClientTraitConfigs")) {
                if ("add".equals(operation)) {
                    addClientTrait(config, updateFuture);
                } else {
                    logger.error(correlationID, serviceName, 500, "Received a configuration update with an unsupported subject/operation combination: " + updateConfiguration.encodePrettily());
                }
            }
        }
        else {
            logger.error(correlationID, serviceName, 500, "Received an unsupported configuration update: " + updateConfiguration.encodePrettily());
        }
    }

    public void processAuthenticationUpdate(Message message) {
        JsonObject update = message.getBodyAsJsonObject();

        String subject = update.getString("subject");
        String operation = update.getString("operation");
        JsonObject config = update.getJsonObject("configuration");

        if("session".equals(subject)) {
            if("blacklist".equals(operation)) {
                String sessionToken = config.getString("sessionToken");
                JsonObject oldSession = sessionMap.remove(sessionToken);
                if(oldSession != null) {
                    String sessionAddressType = oldSession.getJsonObject("config").getString("addressType");
                    if (sessionAddressType != null) {
                        Future<JsonObject> removeFuture = Future.future();
                        removeFuture.setHandler(removeResult -> {
                            if (removeResult.failed()) {
                                logger.error("Failed to remove session bridge", removeResult.cause());
                            }
                        });
                        removeBridge(config.getString("addressDomain"), config.getString("addressVersion"), sessionAddressType, removeFuture);
                    }
                    webSocketSessionMap.remove(sessionToken);
                }
            }
            else {
                logger.error(message.correlationID(), message.origin(), 500, "Received a  session authentication update with an unsupported subject/operation combination: " + update.encodePrettily());
            }
        }
        else {
            logger.error(message.correlationID(), message.origin(), 500, "Received an invalid session authentication update: " + update.encodePrettily());
        }
    }

    @Processor(
            domain = "bridge",
            version = "1",
            type = "bridgeMissing",
            requires = {
                    @Payload(
                            key = "domain",
                            type = DataType.STRING,
                            description = "Domain of the bridge address"
                    ),
                    @Payload(
                            key = "version",
                            type = DataType.STRING,
                            description = "Version of the bridge address"
                    ),
                    @Payload(
                            key = "type",
                            type = DataType.STRING,
                            description = "Type of the bridge address"
                    ),
                    @Payload(
                            key = "isInbound",
                            type = DataType.BOOLEAN,
                            description = "Bridging direction, messages from outside into the system are allowed"
                    ),
                    @Payload(
                            key = "isOutbound",
                            type = DataType.BOOLEAN,
                            description = "Bridging direction, messages from system to the outside are allowed"
                    ),
                    @Payload(
                            key = "authorities",
                            type = DataType.JSONObject,
                            description = "Contains roles, users or sessions arrays for specifying allowed access. Empty for allowing unauthorized access."
                    )
            }
    )
    public void processBridgeMissing(Message message) {
        JsonObject bridgingRequest= message.getBodyAsJsonObject();
        String domain = bridgingRequest.getString("domain");
        String version = bridgingRequest.getString("version");
        String type = bridgingRequest.getString("type");
        Boolean isInbound = bridgingRequest.getBoolean("isInbound");
        Boolean isOutbound = bridgingRequest.getBoolean("isOutbound");
        JsonObject authorities = bridgingRequest.getJsonObject("authorities");

        Future<JsonObject> addBridgeFuture = Future.future();
        addBridgeFuture.setHandler(addHandler -> {
            if(addHandler.failed()) {
                logger.error(message.correlationID(), message.origin(), 500, "Failed to created bridge "
                                + bridgingRequest.getString("domain") + "."
                                + bridgingRequest.getString("version") + "."
                                + bridgingRequest.getString("type")
                                + " in direction inbound " + bridgingRequest.getBoolean("isInbound") + " and outbound " + bridgingRequest.getBoolean("isOutbound") + " for authorities: "
                                + bridgingRequest.getJsonObject("authorities").toString(), addHandler.cause());
                message.fail("Unknown error occurred!");
            }
            JsonObject result = addHandler.result();
            if(result.getInteger("statusCode") != 200) {
                logger.error(message.correlationID(), message.origin(), result.getInteger("statusCode") ,"Failed to created bridge "
                        + bridgingRequest.getString("domain") + "."
                        + bridgingRequest.getString("version") + "."
                        + bridgingRequest.getString("type")
                        + " in direction inbound " + bridgingRequest.getBoolean("isInbound") + " and outbound " + bridgingRequest.getBoolean("isOutbound") + " for authorities: "
                        + bridgingRequest.getJsonObject("authorities").toString()
                        + ". Message: " + result.getString("message"));
                message.fail(result.getInteger("statusCode"), result.getString("message"));
                return;
            }

            if(vertx.isClustered()) {
                sharedData.<String, JsonObject>getClusterWideMap(serviceName + "_availableBridges", mapResult -> {
                    if (mapResult.failed()) {
                        logger.fatal(message.correlationID(), serviceName, 500, "Failed to obtain map of available bridges", mapResult.cause());
                        message.fail("Unknown error occurred!");
                        return;
                    }
                    AsyncMap<String, JsonObject> map = mapResult.result();
                    map.put(domain + "." + version + "." + type, bridgingRequest, putResult -> {
                        if (putResult.failed()) {
                            logger.fatal(message.correlationID() , serviceName, 500, "Failed to persist map of available bridges", mapResult.cause());
                            message.fail("Unknown error occurred!");
                            return;
                        }

                        publish(serviceName, "1", "configHasChanged", new JsonObject()
                                .put("subject", "bridge")
                                .put("operation", "add")
                                .put("configuration", bridgingRequest)
                        );

                        logger.info(message.correlationID(), message.origin(), 201, "Created bridge "
                                + bridgingRequest.getString("domain") + "."
                                + bridgingRequest.getString("version") + "."
                                + bridgingRequest.getString("type")
                                + " in direction inbound " + bridgingRequest.getBoolean("isInbound") + " and outbound " + bridgingRequest.getBoolean("isOutbound") + " for authorities: "
                                + bridgingRequest.getJsonObject("authorities").toString()
                        );

                        message.reply(200);
                    });
                });
            }
            else {
                LocalMap<String, JsonObject> map = sharedData.getLocalMap(serviceName + "_availableBridges");
                map.put(domain + "." + version + "." + type, bridgingRequest);

                publish(serviceName, "1", "configHasChanged", new JsonObject()
                        .put("subject", "bridge")
                        .put("operation", "add")
                        .put("configuration", bridgingRequest)
                );

                logger.info(message.correlationID(), message.origin(), 201, "Created bridge "
                        + bridgingRequest.getString("domain") + "."
                        + bridgingRequest.getString("version") + "."
                        + bridgingRequest.getString("type")
                        + " in direction inbound " + bridgingRequest.getBoolean("isInbound") + " and outbound " + bridgingRequest.getBoolean("isOutbound") + " for authorities: "
                        + bridgingRequest.getJsonObject("authorities").toString()
                );

                message.reply(200);
            }
        });
        addBridge(domain, version, type, isInbound, isOutbound, authorities, addBridgeFuture);
    }

    private void addBridge(String domain, String version, String type, Boolean isInbound, Boolean isOutbound, JsonObject authorities, Future<JsonObject> future) {
        if (authorities != null && !authorities.isEmpty() && (!authorities.containsKey("roles") && !authorities.containsKey("sessions") && !authorities.containsKey("users"))) {
            future.complete(new JsonObject()
                    .put("statusCode", 400)
                    .put("message", "Specify access rights via roles, sessions or userID array in authorities!"));
            return;
        }
        if (authorities == null) {
            authorities = new JsonObject().put("roles", new JsonArray("authenticated"));
        }

        if (domain == null || domain.isEmpty()) {
            future.complete(new JsonObject()
                    .put("statusCode", 400)
                    .put("message", "Address (domain) missing!"));
            return;
        }
        if (version == null || version.isEmpty()) {
            future.complete(new JsonObject()
                    .put("statusCode", 400)
                    .put("message", "Version missing!"));
            return;
        }
        if (type == null || type.isEmpty()) {
            future.complete(new JsonObject()
                    .put("statusCode", 400)
                    .put("message", "Type missing!"));
            return;
        }
        String address = domain + "." + version + "." + type;

        if (availableBridges.containsKey(address)) {
            // bridge is already available
            future.complete(new JsonObject()
                    .put("statusCode", 200));
            return;
        }


        if (isInbound) {
            if (!address.startsWith("in.")) {
                future.complete(new JsonObject()
                        .put("statusCode", 400)
                        .put("message", "Address (domain) must start with 'in.'!"));
                return;
            }
            inboundWhitelist.put(address, authorities);
            availableBridges.put(address, address);
        }
        if (isOutbound) {
            if (!address.startsWith("out.")) {
                future.complete(new JsonObject()
                        .put("statusCode", 400)
                        .put("message", "Address (domain) must start with 'out.'!"));
                return;
            }
            outboundWhitelist.put(address, authorities);
            availableBridges.put(address, address);
        }

        future.complete(new JsonObject()
                .put("statusCode", 200));
    }

    @Processor(
            domain = "bridge",
            version = "1",
            type = "bridgeObsolete",
            description = "Removes a bridge between EventBus and outside world",
            requires = {
                    @Payload(key = "domain", type = DataType.STRING, description = "Address domain"),
                    @Payload(key = "version", type = DataType.STRING, description = "Address version"),
                    @Payload(key = "type", type = DataType.STRING, description = "Address type"),
            }
    )
    public void processBridgeObsolete(Message message) {
        JsonObject bridgingRequest= message.getBodyAsJsonObject();
        String domain = bridgingRequest.getString("domain");
        String version = bridgingRequest.getString("version");
        String type = bridgingRequest.getString("type");

        Future<JsonObject> removeFuture = Future.future();
        removeFuture.setHandler(removeHandler -> {
           if(removeHandler.failed()) {
               message.fail(500, "Unknown error occurred!");
               return;
           }
            JsonObject result = removeHandler.result();
            if(result.getInteger("statusCode") != 200) {
                logger.error(message.correlationID(), message.origin(), result.getInteger("statusCode"), "Failed to remove bridge "
                        + bridgingRequest.getString("domain") + "."
                        + bridgingRequest.getString("version") + "."
                        + bridgingRequest.getString("type")
                        + ". Message: " + result.getString("message"));
                message.fail(result.getInteger("statusCode"), result.getString("message"));
                return;
            }

            if(vertx.isClustered()) {
                sharedData.<String, JsonObject>getClusterWideMap(serviceName + "_availableBridges", mapResult -> {
                    if (mapResult.failed()) {
                        logger.fatal(message.correlationID(), serviceName, 500, "Failed to obtain map of available bridges", mapResult.cause());
                        message.fail("Unknown error occurred!");
                        return;
                    }
                    AsyncMap<String, JsonObject> map = mapResult.result();
                    map.remove(domain + "." + version + "." + type, delResult -> {
                        if (delResult.failed()) {
                            logger.fatal(message.correlationID(), serviceName, 500, "Failed to persist map of available bridges", mapResult.cause());
                            message.fail("Unknown error occurred!");
                            return;
                        }

                        publish(serviceName, "1", "configHasChanged", new JsonObject()
                                .put("subject", "bridge")
                                .put("operation", "delete")
                                .put("configuration", bridgingRequest)
                        );

                        logger.info( message.correlationID(), message.origin(), 200, "Removed bridge "
                                + bridgingRequest.getString("domain") + "."
                                + bridgingRequest.getString("version") + "."
                                + bridgingRequest.getString("type"));

                        message.reply(200);
                    });
                });
            }
            else {
                LocalMap<String, JsonObject> map = sharedData.getLocalMap(serviceName + "_availableBridges");
                map.remove(domain + "." + version + "." + type);

                publish(serviceName, "1", "configHasChanged", new JsonObject()
                        .put("subject", "bridge")
                        .put("operation", "delete")
                        .put("configuration", bridgingRequest)
                );

                logger.info(message.correlationID(), message.origin(), 200, "Removed bridge "
                        + bridgingRequest.getString("domain") + "."
                        + bridgingRequest.getString("version") + "."
                        + bridgingRequest.getString("type"));

                message.reply(200);
            }
        });
        removeBridge(domain, version, type, removeFuture);
    }

    private void removeBridge(String domain, String version, String type, Future<JsonObject> future) {
        if(domain == null || domain.isEmpty()) {
            future.complete(new JsonObject()
                .put("statusCode", 400)
                .put("message", "Domain missing")
            );
            return;
        }
        if(version == null || version.isEmpty()) {
            future.complete(new JsonObject()
                    .put("statusCode", 400)
                    .put("message", "Version missing")
            );
            return;
        }
        if(type == null || type.isEmpty()) {
            future.complete(new JsonObject()
                    .put("statusCode", 400)
                    .put("message", "Type missing")
            );
            return;
        }
        String address = domain + "." + version + "." + type;

        if(availableBridges.containsKey(address)) {
            if(inboundWhitelist.containsKey(address)) {
                inboundWhitelist.remove(address);
            }
            if(outboundWhitelist.containsKey(address)) {
                outboundWhitelist.remove(address);
            }
            availableBridges.remove(address);
            future.complete(new JsonObject()
                    .put("statusCode", 200)
            );
        }
        else {
            future.complete(new JsonObject()
                    .put("statusCode", 200)
                    .put("message", "Bridge not set up/unknown!")
            );
        }
    }

    @Processor(
            domain = "in.logging",
            version = "1",
            type = "log",
            description = "Forwards a client-side log message to the backend log handler",
            enablingConditionKey = "isClientSideLoggingEnabled",
            requires = {
                    @Payload(key = "message", type = DataType.STRING, description = "Message to be logged"),
                    @Payload(key = "level", type = DataType.INTEGER, description = "Message log level (4 = LOG, 4=INFO, 3=WARN, 2=ERROR, 1=FATAL)")
            }
    )
    public void processInLoggingLog(Message message) {
        try {
            JsonObject messageBody = message.getBodyAsJsonObject();
            Integer level = messageBody.getInteger("level");

            if (level == null || backendLoggingLevel < level) {
                // not to be processed/logged
                return;
            }

            String logMessage = messageBody.getString("message");
            if (logMessage == null || logMessage.isEmpty()) {
                return;
            }

            // TODO: include router information

            switch (level) {
                case 1:
                    logger.fatal(message.correlationID(), message.userID(), message.statusCode(), logMessage);
                    break;
                case 2:
                    logger.error(message.correlationID(), message.userID(), message.statusCode(), logMessage);
                    break;
                case 3:
                    logger.warn(message.correlationID(), message.userID(), message.statusCode(), logMessage);
                    break;
                case 4:
                    logger.info(message.correlationID(), message.userID(), message.statusCode(), logMessage);
                    break;
            }
        }
        catch(Exception e) {
            logger.error(message.correlationID(), message.userID(), 500, "Exception when handling client-side log request", e);
        }
    }

    @Processor(
            domain = "in.sessions",
            version = "1",
            type = "bridgingRedirected",
            description = "Provides client with an response to a redirected (in bridge handler) message",
            requires = {
                    @Payload(key = "statusCode", type = DataType.INTEGER, description = "Status code"),
                    @Payload(key = "result", type = DataType.JSONObject, description = "JsonObject with result")
            }
    )
    public void processInSessionsBridgingRedirected(Message message) {
        JsonObject body = message.getBodyAsJsonObject();
        Integer statusCode = body.getInteger("statusCode");
        JsonObject result = body.getJsonObject("result");

        message.reply(statusCode, result);
    }

    @Processor(
            domain = "in.sessions",
            version = "1",
            type = "bridgingErrorOccurred",
            description = "Provides client with an detailed error message in case of errors such as ACL violations or attempts to access non-existent addresses",
            requires = {
                    @Payload(key = "error", type = DataType.JSONObject, description = "JsonObject with error details, i.e. fields status/error 'code', 'message', and 'host'")
            }
    )
    public void processInSessionsBridgingErrorOccured(Message message) {
        JsonObject error = message.getBodyAsJsonObject().getJsonObject("error");
        Integer errorCode = error.getInteger("code");
        String errorMessage = error.getString("message");
        String host = error.getString("host");

        if(errorCode == null) {
            errorCode = 500;
        }
        if(errorMessage == null || errorMessage.isEmpty()) {
            errorMessage = "No error message available";
        }

        message.reply(errorCode, errorMessage);

        String correlationID = message.correlationID();
        if(correlationID == null || correlationID.isEmpty()) {
            correlationID = "Unknown correlation ID";
        }


        String userID = message.userID();
        if(userID != null && !userID.isEmpty() && "anonymous".equalsIgnoreCase(userID)) {
            logger.warn(correlationID, userID , errorCode, "Bridging error: " + errorMessage);
        }
        else if(host != null && !host.isEmpty()) {
            logger.warn(correlationID, host, errorCode, "Bridging error: " + errorMessage);
        }
    }

    @Processor(
            domain = "sessions",
            version = "1",
            type = "clientTraitConfigMissing",
            description = "Allows services to add client-trait configuration values to the session setups. Clients will receive them during session setup. In contrast, user-specific configurations should be set/provided by the router(s) after a session setup.",
            enablingConditionKey = "areServicesAllowedToSetClientTraitConfigs",
            requires = {
                    @Payload(key = "routerRegex", type = DataType.STRING, description = "Regex matching router clients. Note that this requires matching the entire router address string, i.e. domain + '.' + version + '.' + type. Traits are only send to clients of matching routers. If no matching is required use '.*'"),
                    @Payload(key = "navigatorUserAgentRegex", type = DataType.STRING, description = "Regex matching the client's reported user agent. Traits are only send to matching clients. If no matching is required use '.*'"),
                    @Payload(key = "deviceOsCpuRegex", type = DataType.STRING, description = "Regex matching the client's reported os cpu. Traits are only send to matching clients. If no matching is required use '.*'"),
                    @Payload(key = "connectionTypeRegex", type = DataType.STRING, description = "Regex matching the client's reported connection type. Traits are only send to matching clients. If no matching is required use '.*'"),
                    @Payload(key = "traitKey", type = DataType.STRING, description = "Name/key of the config entry"),
                    @Payload(key = "traitValue", type = DataType.BOOLEAN, description = "Value of the config entry")
            }
    )
    public void processSessionsClientTraitConfigMissing(Message message) {
        JsonObject messageBody = message.getBodyAsJsonObject();

        JsonObject config = new JsonObject()
                .put("routerRegex", messageBody.getString("routerRegex"))
                .put("navigatorUserAgentRegex", messageBody.getString("navigatorUserAgentRegex"))
                .put("deviceOsCpuRegex", messageBody.getString("deviceOsCpuRegex"))
                .put("connectionTypeRegex", messageBody.getString("connectionTypeRegex"))
                .put("traitKey", messageBody.getString("traitKey"))
                .put("traitValue", messageBody.getBoolean("traitValue"));

        Future<JsonObject> addFuture = Future.future();
        addFuture.setHandler(addHandler -> {
           if(addHandler.failed()) {
               logger.error(message.correlationID(), message.origin(), 500, "Failed to add client trait!", addHandler.cause());
               message.fail("Unknown error occurred!");
               return;
           }
           JsonObject result = addHandler.result();
           if(result.getInteger("statusCode") != 200) {
               logger.error(message.correlationID(), message.origin(), result.getInteger("statusCode"), "Failed to add client trait: " + result.getString("message"));
               message.fail(result.getInteger("statusCode"), result.getString("message"));
               return;
           }

            if(vertx.isClustered()) {
                sharedData.<String, JsonObject>getClusterWideMap(serviceName + "_clientTraits", mapResult -> {
                    if (mapResult.failed()) {
                        logger.fatal(message.correlationID(), message.origin(), 500, "Failed to obtain map of client traits", mapResult.cause());
                        message.fail("Unknown error occurred!");
                        return;
                    }
                    AsyncMap<String, JsonObject> map = mapResult.result();
                    map.put(config.getString("traitKey")
                            + config.getString("routerRegex")
                            + config.getString("navigatorUserAgentRegex")
                            + config.getString("deviceOsCpuRegex")
                            + config.getString("connectionTypeRegex"), config, addResult -> {
                        if (addResult.failed()) {
                            logger.fatal(message.correlationID(), message.origin(), 500,  "Failed to persist map of client traits", mapResult.cause());
                            message.fail("Unknown error occurred!");
                            return;
                        }

                        publish(serviceName, "1", "configHasChanged", new JsonObject()
                                .put("subject", "clientTrait")
                                .put("operation", "add")
                                .put("configuration", config)
                        );

                        logger.info(message.correlationID(), message.origin(), 201, "Added client trait " + config.getString("traitKey"));
                        message.reply(200);
                    });
                });
            }
            else {
                LocalMap<String, JsonObject> map = sharedData.getLocalMap(serviceName + "_clientTraits");
                map.put(config.getString("traitKey")
                                + config.getString("routerRegex")
                                + config.getString("navigatorUserAgentRegex")
                                + config.getString("deviceOsCpuRegex")
                                + config.getString("connectionTypeRegex"), config);

                publish(serviceName, "1", "configHasChanged", new JsonObject()
                        .put("subject", "clientTrait")
                        .put("operation", "add")
                        .put("configuration", config)
                );

                logger.info(message.correlationID(), message.origin(), 201, "Added client trait " + config.getString("traitKey"));
                message.reply(200);
            }
        });
        addClientTrait(config, addFuture);
    }

    private void addClientTrait(JsonObject config, Future<JsonObject> future) {
        String traitKey = config.getString("traitKey");
        Boolean traitValue = config.getBoolean("traitValue");
        String routerRegex = config.getString("routerRegex");
        String navigatorUserAgentRegex = config.getString("navigatorUserAgentRegex");
        String deviceOsCpuRegex = config.getString("deviceOsCpuRegex");
        String connectionTypeRegex = config.getString("connectionTypeRegex");

        if(traitKey == null || traitKey.isEmpty()) {
            future.complete(new JsonObject().put("statusCode", 400).put("message", "Must provide a non-empty 'traitKey'"));
            return;
        }
        if(traitValue == null) {
            future.complete(new JsonObject().put("statusCode", 400).put("message", "Must provide a non-null 'traitValue'"));
            return;
        }
        if((routerRegex == null || routerRegex.isEmpty())
                && (navigatorUserAgentRegex == null || navigatorUserAgentRegex.isEmpty())
                && (deviceOsCpuRegex == null || deviceOsCpuRegex.isEmpty())
                && (connectionTypeRegex == null || connectionTypeRegex.isEmpty())) {
            future.complete(new JsonObject().put("statusCode", 400).put("message", "Must provide one non-empty regex, e.g. routerRegex, navigatorUserAgentRegex, deviceOsCpuRegex, connectionTypeRegex"));
            return;
        }

        clientTraitMap.put(traitKey + routerRegex + navigatorUserAgentRegex + deviceOsCpuRegex + connectionTypeRegex, config);

        future.complete(new JsonObject().put("statusCode", 200));
    }

    @Processor(
            domain = "publicFiles",
            version = "1",
            type = "missingAlias",
            description = "Setups an alias route/path to a public resource location (alternative path for accessing via HTTPs)",
            enablingConditionKey = "isPublicStorageHTTPServerEnabled",
            requires = {
                    @Payload(key = "path", type = DataType.STRING, description = "Path without regex of the original public resource. Must be non-empty and start with '/public/'"),
                    @Payload(key = "alias", type = DataType.STRING, description = "Alias path without regex which is to lead to the original public resource. Must be not-empty, start with '/' and must not start with '/public/', '/eventbus/', or '/restricted/'")
            }
    )
    public void processPublicFilesMissingAlias(Message message) {
        JsonObject body = message.getBodyAsJsonObject();
        String path = body.getString("path");
        String alias = body.getString("alias");

        Future<JsonObject> addFuture = Future.future();
        addFuture.setHandler(addHandler -> {
           if(addHandler.failed()) {
               logger.error(message.correlationID(), message.origin(), 500, "Failed to add alias " + alias + " for path " + path + ": " + addHandler.cause());
               message.fail(500, "Unknown error occurred!");
               return;
           }
           JsonObject result = addHandler.result();
           if(result.getInteger("statusCode") != 200) {
               logger.error(message.correlationID(), message.origin(), 500, "Failed to add alias " + alias + " for path " + path + ": " + result.getString("message"));
               message.fail(result.getInteger("statusCode"), result.getString("message"));
               return;
           }

            if(vertx.isClustered()) {
                sharedData.<String, JsonObject>getClusterWideMap(serviceName + "_publicFilesAliases", mapResult -> {
                    if (mapResult.failed()) {
                        logger.fatal(message.correlationID(), message.origin(), 500, "Failed to obtain map of public files aliases", mapResult.cause());
                        message.fail("Unknown error occurred!");
                        return;
                    }
                    AsyncMap<String, JsonObject> map = mapResult.result();
                    map.put(alias, body, putResult -> {
                        if (putResult.failed()) {
                            logger.fatal(message.correlationID(), message.origin(), 500, "Failed to persist map of public files aliases", mapResult.cause());
                            message.fail("Unknown error occurred!");
                            return;
                        }

                        publish(serviceName, "1", "configHasChanged", new JsonObject()
                                .put("subject", "publicFilesAlias")
                                .put("operation", "add")
                                .put("configuration", body)
                        );

                        logger.info(message.correlationID(), message.origin(), 200, "Added alias " + alias + " for path " + path);

                        message.reply(200);
                    });
                });
            }
            else {
                LocalMap<String, JsonObject> map = sharedData.getLocalMap(serviceName + "_publicFilesAliases");
                map.put(alias, body);

                publish(serviceName, "1", "configHasChanged", new JsonObject()
                        .put("subject", "publicFilesAlias")
                        .put("operation", "add")
                        .put("configuration", body)
                );

                logger.info(message.correlationID(), message.origin(), 200, "Added alias " + alias + " for path " + path);
                message.reply(200);
            }
        });
        addPublicFilesAlias(path, alias, addFuture);
    }

    private void addPublicFilesAlias(String path, String alias, Future<JsonObject> future) {
        if(path.isEmpty() || !path.startsWith("/public/")) {
            future.complete(new JsonObject()
                .put("statusCode", 400)
                .put("message", "Invalid path: Must be non-empty and start with '/public/'"));
            return;
        }
        if(alias.isEmpty() || "/".equals(alias) || !alias.startsWith("/") || alias.startsWith("/public/") || alias.startsWith("/eventbus/") || alias.startsWith("/users/")) {
            future.complete(new JsonObject()
                    .put("statusCode", 400)
                    .put("message", "Invalid path: Must be not-empty, start with '/' and must not start with '/public/', '/eventbus/', or '/users/'"));
            return;
        }

        if(publicFilesAliasMap.containsKey(alias)) {
            // alias already configured
            future.complete(new JsonObject().put("statusCode", 200));
            return;
        }
        publicFilesAliasMap.put(alias, path);

        final String pathPlaceholder = path.endsWith("/*")?  path.substring(0, path.length() - 1) : path;
        final Integer aliasPlaceholderLength = alias.endsWith("/*")? alias.length() - 1 : alias.length();

        router.route(alias).handler(routingContext -> {
            String aliasPath = routingContext.normalisedPath();
            String reroutePath = pathPlaceholder + aliasPath.substring(aliasPlaceholderLength, aliasPath.length());
            routingContext.reroute(reroutePath);
        });

        future.complete(new JsonObject().put("statusCode", 200));
    }

    @Processor(
            domain = "publicFiles",
            version = "1",
            type = "missingAliasWithDynamicPath",
            description = "Setups an alias route/path to a public resource location (alternative path for accessing via HTTPs) and request the path from another processor. The path request includes alias, path, remoteHost, JsonObject parameters with all parameter values, JsonObject headers with all received HTTP headers",
            enablingConditionKey = "isPublicStorageHTTPServerEnabled",
            requires = {
                    @Payload(key = "aliasParameterPrefix", type = DataType.STRING, description = "First part of the alias path without regex but with parameter definitions. Must be not-empty, contain a parameter specification (e.g. ':productid'), start with '/' and must not start with '/public/', '/eventbus/', or '/restricted/'"),
                    @Payload(key = "aliasMarker", type = DataType.STRING, description = "Alias path after the alias with parameters. Used for identifying the end of the parametrized part and thus allowing for identifying the original public resource. Must be not-empty and contain at least one marker which is not only '/*' or '*' E.g. '/something/*' or 'filename.suffix'"),
                    @Payload(key = "domain", type = DataType.STRING, description = "Domain of the clearance processor"),
                    @Payload(key = "version", type = DataType.STRING, description = "Version of the clearance processor"),
                    @Payload(key = "type", type = DataType.STRING, description = "Type of the clearance processor")
            }
    )
    public void processPublicFilesMissingAliasWithDynamicPath(Message message) {
        JsonObject body = message.getBodyAsJsonObject();
        String aliasParameterPrefix = body.getString("aliasParameterPrefix");
        String aliasMarker = body.getString("aliasMarker");
        String fullAlias = aliasParameterPrefix + aliasMarker;
        String processorDomain = body.getString("domain");
        String processorVersion = body.getString("version");
        String processorType = body.getString("type");

        Future<JsonObject> addFuture = Future.future();
        addFuture.setHandler(addHandler -> {
            if(addHandler.failed()) {
                logger.error(message.correlationID(), message.origin(), 500, "Failed to add alias " + fullAlias + " with dynamic path via " + processorDomain + "." + processorVersion + "." + processorType + ": " + addHandler.cause());
                message.fail(500, "Unknown error occurred!");
                return;
            }
            JsonObject result = addHandler.result();
            if(result.getInteger("statusCode") != 200) {
                logger.error(message.correlationID(), message.origin(), result.getInteger("statusCode"), "Failed to add alias " + fullAlias +  " with dynamic path via " + processorDomain + "." + processorVersion + "." + processorType +  ": " + result.getString("message"));
                message.fail(result.getInteger("statusCode"), result.getString("message"));
                return;
            }

            body.put("isDynamicPath", true);

            if(vertx.isClustered()) {
                sharedData.<String, JsonObject>getClusterWideMap(serviceName + "_publicFilesAliasesWithDynamicPath", mapResult -> {
                    if (mapResult.failed()) {
                        logger.fatal(message.correlationID(), message.origin(), 500, "Failed to obtain map of public files aliases with dynamic path", mapResult.cause());
                        message.fail("Unknown error occurred!");
                        return;
                    }
                    AsyncMap<String, JsonObject> map = mapResult.result();
                    map.put(fullAlias, body, putResult -> {
                        if (putResult.failed()) {
                            logger.fatal(message.correlationID(), message.origin(), 500, "Failed to persist map of public files aliases with dynamic path", mapResult.cause());
                            message.fail("Unknown error occurred!");
                            return;
                        }

                        publish(serviceName, "1", "configHasChanged", new JsonObject()
                                .put("subject", "publicFilesAliasWithDynamicPath")
                                .put("operation", "add")
                                .put("configuration", body)
                        );

                        logger.info(message.correlationID(), message.origin(), 201, "Added alias " + fullAlias +  " with dynamic path via " + processorDomain + "." + processorVersion + "." + processorType);
                        message.reply(200);
                    });
                });
            }
            else {
                LocalMap<String, JsonObject> map = sharedData.getLocalMap(serviceName + "_publicFilesAliasesWithDynamicPath");
                map.put(fullAlias, body);



                publish(serviceName, "1", "configHasChanged", new JsonObject()
                        .put("subject", "publicFilesAliasWithDynamicPath")
                        .put("operation", "add")
                        .put("configuration", body)
                );

                logger.info(message.correlationID(), message.origin(), 201, "Added alias " + fullAlias + " with dynamic path via " + processorDomain + "." + processorVersion + "." + processorType);

                message.reply(200);
            }
        });
        addPublicFilesAliasWithDynamicPath(aliasParameterPrefix, aliasMarker, fullAlias, processorDomain, processorVersion, processorType, addFuture);
    }

    private void addPublicFilesAliasWithDynamicPath(String aliasParameterPrefix, String aliasMarker, String fullAlias, String processorDomain, String processorVersion, String processorType, Future<JsonObject> future) {
        if(aliasParameterPrefix.isEmpty() || !aliasParameterPrefix.contains(":") || !aliasParameterPrefix.startsWith("/") || aliasParameterPrefix.startsWith("/public/") || aliasParameterPrefix.startsWith("/users/") || aliasParameterPrefix.startsWith("/eventbus/")) {
            future.complete(new JsonObject()
                    .put("statusCode", 400)
                    .put("message", "Invalid aliasParameterPrefix: Must be not-empty, contain a parameter specification (e.g. ':productid'), start with '/' and must not start with '/public/', '/eventbus/, or '/restricted/'"));
            return;
        }
        if(aliasMarker.isEmpty() || (aliasMarker.endsWith("/.*") && aliasMarker.length() == 3) || (aliasMarker.endsWith(".*") && aliasMarker.length() == 2)) {
            future.complete(new JsonObject()
                    .put("statusCode", 400)
                    .put("message", "Invalid aliasMarker: Must be not-empty and contain at least one marker which is not '/*' or '*'"));

            return;
        }

        if(publicFilesAliasMap.containsKey(fullAlias)) {
            // alias already configured
            future.complete(new JsonObject().put("statusCode", 200));
            return;
        }
        publicFilesAliasMap.put(fullAlias, processorDomain + "." + processorType + "." + processorVersion);

        router.route(fullAlias).handler(routingContext -> {
            String aliasPath = routingContext.normalisedPath();

            // compile the data for the clearance
            HttpServerRequest request = routingContext.request();
            JsonObject pathRequestData = new JsonObject()
                    .put("alias", aliasPath)
                    .put("remoteHost", request.remoteAddress().host())
                    .put("parameters", new JsonObject())
                    .put("headers", new JsonObject());

            request.headers().forEach(entry -> {
                pathRequestData.getJsonObject("headers").put(entry.getKey(), entry.getValue());
            });
            request.params().forEach(entry -> {
                pathRequestData.getJsonObject("parameters").put(entry.getKey(), entry.getValue());
            });

            String correlationID = UUID.randomUUID().toString();
            send(processorDomain, processorVersion, processorType, correlationID, pathRequestData, reply -> {
                if(reply.failed()) {
                    routingContext.fail(500);
                    logger.error(correlationID, 500,"Failed to process aliased request with dynamic path. Alias path: " + aliasPath + " Remote host: " + request.remoteAddress().host(), reply.cause());
                    return;
                }
                if(reply.statusCode() != 200) {
                    routingContext.fail(reply.statusCode());
                    logger.warn(correlationID, reply.statusCode(), "Aliased request with dynamic path was denied/failed. Status code: " + reply.statusCode() + " Message: " + reply.getMessage()  + " Alias path: " + aliasPath + " Remote host: " + request.remoteAddress().host());
                    return;
                }

                String path = reply.getBodyAsJsonObject().getString("path");

                if(path.isEmpty() || !path.startsWith("/public/")) {
                    routingContext.fail(500);
                    logger.error(reply.correlationID(), 500, "Failed to process aliased request with dynamic path. Processor returned invalid path " + path + " Alias path: " + aliasPath + " Remote host: " + request.remoteAddress().host());
                    reply.reply(400, "Invalid path. Must start with '/public/' and be non-empty");
                    return;
                }

                routingContext.reroute(path);
            });
        });

        future.complete(new JsonObject().put("statusCode", 200));
    }

    @Processor(
            domain = "publicFiles",
            version = "1",
            type = "missingAliasWithClearance",
            description = "Setups an alias route/path to a public resource location (alternative path for accessing via HTTPs) but awaits clearance from the provided processor before providing the requested resource. The clearance request includes alias, path, remoteHost, JsonObject parameters with all parameter values, JsonObject headers with all received HTTP headers",
            enablingConditionKey = "isPublicStorageHTTPServerEnabled",
            requires = {
                    @Payload(key = "path", type = DataType.STRING, description = "Path without regex of the original public resource. Must be non-empty and start with '/public/'"),
                    @Payload(key = "aliasParameterPrefix", type = DataType.STRING, description = "First part of the alias path without regex but with parameter definitions. Must be not-empty, contain a parameter specification (e.g. ':productid'), start with '/' and must not start with '/public/', '/eventbus/', or '/restricted/'"),
                    @Payload(key = "aliasMarker", type = DataType.STRING, description = "Alias path after the alias with parameters. Used for identifying the end of the parametrized part and thus allowing for identifying the original public resource. Must be not-empty and contain at least one marker which is not only '/*' or '*' E.g. '/something/*' or 'filename.suffix'"),
                    @Payload(key = "domain", type = DataType.STRING, description = "Domain of the clearance processor"),
                    @Payload(key = "version", type = DataType.STRING, description = "Version of the clearance processor"),
                    @Payload(key = "type", type = DataType.STRING, description = "Type of the clearance processor")
            }
    )
    public void processPublicFilesMissingAliasWithClearance(Message message) {
        JsonObject body = message.getBodyAsJsonObject();
        String path = body.getString("path");
        String aliasParameterPrefix = body.getString("aliasParameterPrefix");
        String aliasMarker = body.getString("aliasMarker");
        String fullAlias = aliasParameterPrefix + aliasMarker;
        String processorDomain = body.getString("domain");
        String processorVersion = body.getString("version");
        String processorType = body.getString("type");

        Future<JsonObject> addFuture = Future.future();
        addFuture.setHandler(addHandler -> {
            if(addHandler.failed()) {
                logger.error(message.correlationID(), message.origin(), 500, "Failed to add alias " + fullAlias + " for path " + path + " with clearance from " + processorDomain + "." + processorVersion + "." + processorType + ": " + addHandler.cause());
                message.fail(500, "Unknown error occurred!");
                return;
            }
            JsonObject result = addHandler.result();
            if(result.getInteger("statusCode") != 200) {
                logger.error(message.correlationID(), message.origin(), result.getInteger("statusCode"), "Failed to add alias " + fullAlias + " for path " + path + " with clearance from " + processorDomain + "." + processorVersion + "." + processorType +  ": " + result.getString("message"));
                message.fail(result.getInteger("statusCode"), result.getString("message"));
                return;
            }

            body.put("isClearanceRequired", true);

            if(vertx.isClustered()) {
                sharedData.<String, JsonObject>getClusterWideMap(serviceName + "_publicFilesAliasesWithClearance", mapResult -> {
                    if (mapResult.failed()) {
                        logger.fatal(message.correlationID(), message.origin(), 500, "Failed to obtain map of public files aliases with clearance", mapResult.cause());
                        message.fail("Unknown error occurred!");
                        return;
                    }
                    AsyncMap<String, JsonObject> map = mapResult.result();
                    map.put(fullAlias, body, putResult -> {
                        if (putResult.failed()) {
                            logger.fatal(message.correlationID(), message.origin(), 500, "Failed to persist map of public files aliases with clearance", mapResult.cause());
                            message.fail("Unknown error occurred!");
                            return;
                        }

                        publish(serviceName, "1", "configHasChanged", new JsonObject()
                                .put("subject", "publicFilesAliasWithClearance")
                                .put("operation", "add")
                                .put("configuration", body)
                        );

                        logger.info(message.correlationID(), message.origin(), 201, "Added alias " + fullAlias + " for path " + path + " with clearance to " + processorDomain + "." + processorVersion + "." + processorType);
                        message.reply(200);
                    });
                });
            }
            else {
                LocalMap<String, JsonObject> map = sharedData.getLocalMap(serviceName + "_publicFilesAliasesWithClearance");
                map.put(fullAlias, body);

                publish(serviceName, "1", "configHasChanged", new JsonObject()
                        .put("subject", "publicFilesAliasWithClearance")
                        .put("operation", "add")
                        .put("configuration", body)
                );

                logger.info(message.correlationID(), message.origin(), 201, "Added alias " + fullAlias + " for path " + path + " with notifications to " + processorDomain + "." + processorVersion + "." + processorType);
                message.reply(200);
            }
        });
        addPublicFilesAliasWithClearance(path, aliasParameterPrefix, aliasMarker, fullAlias, processorDomain, processorVersion, processorType, addFuture);
    }

    private void addPublicFilesAliasWithClearance(String path, String aliasParameterPrefix, String aliasMarker, String fullAlias, String processorDomain, String processorVersion, String processorType, Future<JsonObject> future) {
        if(path.isEmpty() || !path.startsWith("/public/")) {
            future.complete(new JsonObject()
                    .put("statusCode", 400)
                    .put("message", "Invalid path: Must be non-empty and start with '/public/'"));
            return;
        }
        if(aliasParameterPrefix.isEmpty() || !aliasParameterPrefix.contains(":") || !aliasParameterPrefix.startsWith("/") || aliasParameterPrefix.startsWith("/public/") || aliasParameterPrefix.startsWith("/users/") || aliasParameterPrefix.startsWith("/eventbus/")) {
            future.complete(new JsonObject()
                    .put("statusCode", 400)
                    .put("message", "Invalid aliasParameterPrefix: Must be not-empty, contain a parameter specification (e.g. ':productid'), start with '/' and must not start with '/public/', '/eventbus/, or '/restricted/'"));
            return;
        }
        if(aliasMarker.isEmpty() || (aliasMarker.endsWith("/.*") && aliasMarker.length() == 3) || (aliasMarker.endsWith(".*") && aliasMarker.length() == 2)) {
            future.complete(new JsonObject()
                    .put("statusCode", 400)
                    .put("message", "Invalid aliasMarker: Must be not-empty and contain at least one marker which is not '/*' or '*'"));

            return;
        }

        if(publicFilesAliasMap.containsKey(fullAlias)) {
            // alias already configured
            future.complete(new JsonObject().put("statusCode", 200));
            return;
        }
        publicFilesAliasMap.put(fullAlias, path);

        final String pathPlaceholder = path.endsWith("/*")?  path.substring(0, path.length() - 1) : path;
        final String aliasMarkerPlaceholder = aliasMarker.endsWith("*")?  aliasMarker.substring(0, aliasMarker.length() - 1) : aliasMarker;

        router.route(fullAlias).handler(routingContext -> {
            String aliasPath = routingContext.normalisedPath();
            String reroutePath = pathPlaceholder + aliasPath.substring(aliasPath.indexOf(aliasMarkerPlaceholder) + aliasMarkerPlaceholder.length());


            // compile the data for the clearance
            HttpServerRequest request = routingContext.request();
            JsonObject clearanceData = new JsonObject()
                    .put("alias", aliasPath)
                    .put("path", reroutePath)
                    .put("remoteHost", request.remoteAddress().host())
                    .put("parameters", new JsonObject())
                    .put("headers", new JsonObject());

            request.headers().forEach(entry -> {
                clearanceData.getJsonObject("headers").put(entry.getKey(), entry.getValue());
            });
            request.params().forEach(entry -> {
                clearanceData.getJsonObject("parameters").put(entry.getKey(), entry.getValue());
            });

            String correlationID = UUID.randomUUID().toString();
            send(processorDomain, processorVersion, processorType, correlationID, clearanceData, reply -> {
                if(reply.failed()) {
                    routingContext.fail(500);
                    logger.error(correlationID, 500, "Failed to process aliased request with clearance. Alias path: " + aliasPath + " Remote host: " + request.remoteAddress().host(), reply.cause());
                    return;
                }
                if(reply.statusCode() != 200) {
                    routingContext.fail(reply.statusCode());
                    logger.warn(correlationID, reply.statusCode(), "Aliased request with clearance was denied/failed. Status code: " + reply.statusCode() + " Message: " + reply.getMessage()  + " Alias path: " + aliasPath + " Remote host: " + request.remoteAddress().host());
                    return;
                }
                routingContext.reroute(reroutePath);
            });
        });

        future.complete(new JsonObject().put("statusCode", 200));
    }


    @Processor(
            domain = "publicFiles",
            version = "1",
            type = "missingAliasWithNotification",
            description = "Setups an alias route/path to a public resource location (alternative path for accessing via HTTPs and notifies the provided address via publish. The notification includes alias, path, remoteHost, JsonObject parameters with all parameter values, JsonObject headers with all received HTTP headers",
            enablingConditionKey = "isPublicStorageHTTPServerEnabled",
            requires = {
                    @Payload(key = "path", type = DataType.STRING, description = "Path without regex of the original public resource. Must be non-empty and start with '/public/'"),
                    @Payload(key = "aliasParameterPrefix", type = DataType.STRING, description = "First part of the alias path without regex but with parameter definitions. Must be not-empty, contain a parameter specification (e.g. ':productid'), start with '/' and must not start with '/public/', '/eventbus/', or '/restricted/'"),
                    @Payload(key = "aliasMarker", type = DataType.STRING, description = "Alias path after the alias with parameters. Used for identifying the end of the parametrized part and thus allowing for identifying the original public resource. Must be not-empty and contain at least one marker which is not only '/*' or '*' E.g. '/something/*' or 'filename.suffix'"),
                    @Payload(key = "domain", type = DataType.STRING, description = "Domain the notification is to be send to"),
                    @Payload(key = "version", type = DataType.STRING, description = "Version the notification is to be send to"),
                    @Payload(key = "type", type = DataType.STRING, description = "Type the notification is to be send to")
            }
    )
    public void processPublicFilesMissingAliasWithNotification(Message message) {
        JsonObject body = message.getBodyAsJsonObject();
        String path = body.getString("path");
        String aliasParameterPrefix = body.getString("aliasParameterPrefix");
        String aliasMarker = body.getString("aliasMarker");
        String fullAlias = aliasParameterPrefix + aliasMarker;
        String domain = body.getString("domain");
        String version = body.getString("version");
        String type = body.getString("type");

        Future<JsonObject> addFuture = Future.future();
        addFuture.setHandler(addHandler -> {
            if(addHandler.failed()) {
                logger.error(message.correlationID(), message.origin(), 500, "Failed to add alias " + fullAlias + " for path " + path + " with notifications to " + domain + "." + version + "." + type + ": " + addHandler.cause());
                message.fail(500, "Unknown error occurred!");
                return;
            }
            JsonObject result = addHandler.result();
            if(result.getInteger("statusCode") != 200) {
                logger.error(message.correlationID(), message.origin(), result.getInteger("statusCode"), "Failed to add alias " + fullAlias + " for path " + path + " with notifications to " + domain + "." + version + "." + type +  ": " + result.getString("message"));
                message.fail(result.getInteger("statusCode"), result.getString("message"));
                return;
            }

            if(vertx.isClustered()) {
                sharedData.<String, JsonObject>getClusterWideMap(serviceName + "_publicFilesAliasesWithNotification", mapResult -> {
                    if (mapResult.failed()) {
                        logger.fatal(message.correlationID(), message.origin(), 500, "Failed to obtain map of public files aliases with notification", mapResult.cause());
                        message.fail("Unknown error occurred!");
                        return;
                    }
                    AsyncMap<String, JsonObject> map = mapResult.result();
                    map.put(fullAlias, body, putResult -> {
                        if (putResult.failed()) {
                            logger.fatal(message.correlationID(), message.origin(), 500, "Failed to persist map of public files aliases with notification", mapResult.cause());
                            message.fail("Unknown error occurred!");
                            return;
                        }

                        publish(serviceName, "1", "configHasChanged", new JsonObject()
                                .put("subject", "publicFilesAliasWithNotification")
                                .put("operation", "add")
                                .put("configuration", body)
                        );

                        logger.info(message.correlationID(), message.origin(), 201, "Added alias " + fullAlias + " for path " + path + " with notifications to " + domain + "." + version + "." + type);
                        message.reply(200);
                    });
                });
            }
            else {
                LocalMap<String, JsonObject> map = sharedData.getLocalMap(serviceName + "_publicFilesAliasesWithNotification");
                map.put(fullAlias, body);

                publish(serviceName, "1", "configHasChanged", new JsonObject()
                        .put("subject", "publicFilesAliasWithNotification")
                        .put("operation", "add")
                        .put("configuration", body)
                );

                logger.info(message.correlationID(), message.origin(), 201, "Added alias " + fullAlias + " for path " + path + " with notifications to " + domain + "." + version + "." + type);
                message.reply(200);
            }
        });
        addPublicFilesAliasWithNotification(path, aliasParameterPrefix, aliasMarker, fullAlias, domain, version, type, addFuture);
    }

    private void addPublicFilesAliasWithNotification(String path, String aliasParameterPrefix, String aliasMarker, String fullAlias, String notificationDomain, String notificationVersion, String notificationType, Future<JsonObject> future) {
        if(path.isEmpty() || !path.startsWith("/public/")) {
            future.complete(new JsonObject()
                    .put("statusCode", 400)
                    .put("message", "Invalid path: Must be non-empty and start with '/public/'"));
            return;
        }
        if(aliasParameterPrefix.isEmpty() || !aliasParameterPrefix.contains(":") || !aliasParameterPrefix.startsWith("/") || aliasParameterPrefix.startsWith("/public/") || aliasParameterPrefix.startsWith("/users/") || aliasParameterPrefix.startsWith("/eventbus/")) {
            future.complete(new JsonObject()
                    .put("statusCode", 400)
                    .put("message", "Invalid aliasParameterPrefix: Must be not-empty, contain a parameter specification (e.g. ':productid'), start with '/' and must not start with '/public/', '/eventbus/, or '/restricted/'"));
            return;
        }
        if(aliasMarker.isEmpty() || (aliasMarker.endsWith("/.*") && aliasMarker.length() == 3) || (aliasMarker.endsWith(".*") && aliasMarker.length() == 2)) {
            future.complete(new JsonObject()
                    .put("statusCode", 400)
                    .put("message", "Invalid aliasMarker: Must be not-empty and contain at least one marker which is not '/*' or '*'"));

            return;
        }

        if(publicFilesAliasMap.containsKey(fullAlias)) {
            // alias already configured
            future.complete(new JsonObject().put("statusCode", 200));
            return;
        }
        publicFilesAliasMap.put(fullAlias, path);

        final String pathPlaceholder = path.endsWith("/*")?  path.substring(0, path.length() - 1) : path;
        final String aliasMarkerPlaceholder = aliasMarker.endsWith("*")?  aliasMarker.substring(0, aliasMarker.length() - 1) : aliasMarker;

        router.route(fullAlias).handler(routingContext -> {
            String aliasPath = routingContext.normalisedPath();
            String reroutePath = pathPlaceholder + aliasPath.substring(aliasPath.indexOf(aliasMarkerPlaceholder) + aliasMarkerPlaceholder.length());


            // compile the data for the notification
            HttpServerRequest request = routingContext.request();
            JsonObject notificationData = new JsonObject()
                    .put("alias", aliasPath)
                    .put("path", reroutePath)
                    .put("remoteHost", request.remoteAddress().host())
                    .put("parameters", new JsonObject())
                    .put("headers", new JsonObject());

            request.headers().forEach(entry -> {
                notificationData.getJsonObject("headers").put(entry.getKey(), entry.getValue());
            });
            request.params().forEach(entry -> {
                notificationData.getJsonObject("parameters").put(entry.getKey(), entry.getValue());
            });

            routingContext.reroute(reroutePath);
            send(notificationDomain, notificationVersion, notificationType, "webUser", UUID.randomUUID().toString(), notificationData);
        });

        future.complete(new JsonObject().put("statusCode", 200));
    }

    @Processor(
            domain = "publicFiles",
            version = "1",
            type = "missingRegexAlias",
            description = "Setups an alias route/path with regex to a public resource location (alternative path for accessing via HTTPs)",
            enablingConditionKey = "isPublicStorageHTTPServerEnabled",
            requires = {
                    @Payload(key = "path", type = DataType.STRING, description = "Path without regex of the original public resource. Must be non-empty and start with '/public/'"),
                    @Payload(key = "aliasRegexPrefix", type = DataType.STRING, description = "Regex which is used as prefix to match requests. Must be not-empty, start with '/' and must not start with '/public/', '/eventbus/', or '/restricted/'"),
                    @Payload(key = "aliasMarker", type = DataType.STRING, description = "Alias path after the regex. Used for identifying the end of the regex and thus allowing for identifying the original public resource. Must be not-empty and contain at least one marker which is not only '/.*' or '*' E.g. '/something/.*' or 'filename.suffix'. Must be not-empty")
            }
    )
    public void processPublicFilesMissingRegexAlias(Message message) {
        JsonObject body = message.getBodyAsJsonObject();
        String path = body.getString("path");
        String aliasRegex = body.getString("aliasRegexPrefix");
        final String aliasMarker = body.getString("aliasMarker");
        String fullAliasRegex = aliasRegex + aliasMarker;

        Future<JsonObject> addFuture = Future.future();
        addFuture.setHandler(addHandler -> {
            if(addHandler.failed()) {
                logger.error(message.correlationID(), message.origin(), 500, "Failed to add regex alias " + fullAliasRegex + " for path " + path + ": " + addHandler.cause());
                message.fail(500, "Unknown error occurred!");
                return;
            }
            JsonObject result = addHandler.result();
            if(result.getInteger("statusCode") != 200) {
                logger.error(message.correlationID(), message.origin(),  result.getInteger("statusCode"), "Failed to add regex alias " + fullAliasRegex + " for path " + path + ": " + result.getString("message"));
                message.fail(result.getInteger("statusCode"), result.getString("message"));
                return;
            }

            if(vertx.isClustered()) {
                sharedData.<String, JsonObject>getClusterWideMap(serviceName + "_publicFilesRegexAliases", mapResult -> {
                    if (mapResult.failed()) {
                        logger.fatal(message.correlationID(), message.origin(), 500, "Failed to obtain map of public files regex aliases", mapResult.cause());
                        message.fail("Unknown error occurred!");
                        return;
                    }
                    AsyncMap<String, JsonObject> map = mapResult.result();
                    map.put(fullAliasRegex, body, putResult -> {
                        if (putResult.failed()) {
                            logger.fatal(message.correlationID(), message.origin(), 500, "Failed to persist map of public files regex aliases", mapResult.cause());
                            message.fail("Unknown error occurred!");
                            return;
                        }

                        publish(serviceName, "1", "configHasChanged", new JsonObject()
                                .put("subject", "publicFilesRegexAlias")
                                .put("operation", "add")
                                .put("configuration", body)
                        );

                        logger.info(message.correlationID(), message.origin(), 201, "Added regex alias " + fullAliasRegex + " for path " + path);

                        message.reply(200);
                    });
                });
            }
            else {
                LocalMap<String, JsonObject> map = sharedData.getLocalMap(serviceName + "_publicFilesRegexAliases");
                map.put(fullAliasRegex, body);

                publish(serviceName, "1", "configHasChanged", new JsonObject()
                        .put("subject", "publicFilesRegexAlias")
                        .put("operation", "add")
                        .put("configuration", body)
                );

                logger.info(message.correlationID(), message.origin(), 201, "Added regex alias " + fullAliasRegex + " for path " + path);

                message.reply(200);
            }
        });
        addPublicFilesRegexAlias(path, aliasRegex, aliasMarker, fullAliasRegex, addFuture);
    }

    private void addPublicFilesRegexAlias(String path, String aliasRegex, String aliasMarker, String fullAliasRegex, Future<JsonObject> future) {
        if(path.isEmpty() || !path.startsWith("/public/")) {
            future.complete(new JsonObject()
                .put("statusCode", 400)
                .put("message", "Invalid path: Must be non-empty and start with '/public/'"));
            return;
        }
        if(aliasRegex.isEmpty() || !aliasRegex.startsWith("/") || aliasRegex.startsWith("/public/") || aliasRegex.startsWith("/users/") || aliasRegex.startsWith("/eventbus/")) {
            future.complete(new JsonObject()
                    .put("statusCode", 400)
                    .put("message", "Invalid aliasRegex: Must be not-empty, start with '/' and must not start with '/public/', '/eventbus/, or '/users/'"));
            return;
        }
        if(aliasMarker.isEmpty() || (aliasMarker.endsWith("/.*") && aliasMarker.length() == 3) || (aliasMarker.endsWith(".*") && aliasMarker.length() == 2)) {
            future.complete(new JsonObject()
                    .put("statusCode", 400)
                    .put("message", "Invalid aliasMarker: Must be not-empty and contain at least one marker which is not '/*' or '*'"));
            return;
        }

        if(publicFilesAliasMap.containsKey(fullAliasRegex)) {
            // alias already configured
            future.complete(new JsonObject().put("statusCode", 200));
            return;
        }
        publicFilesAliasMap.put(fullAliasRegex, path);

        final String pathPlaceholder = path.endsWith("/*")?  path.substring(0, path.length() - 1) : path;
        final String aliasMarkerPlaceholder = aliasMarker.endsWith(".*")?  aliasMarker.substring(0, aliasMarker.length() - 2) : aliasMarker;

        router.routeWithRegex(fullAliasRegex).handler(routingContext -> {
            String aliasPath = routingContext.normalisedPath();
            String reroutePath = pathPlaceholder + aliasPath.substring(aliasPath.indexOf(aliasMarkerPlaceholder) + aliasMarkerPlaceholder.length());
            routingContext.reroute(reroutePath);
        });

        future.complete(new JsonObject().put("statusCode", 200));
    }

    private void loadConfig() {
        JsonObject config = config();

        if(!config.containsKey("port")) {
            config.put("port", 8080);
            logger.settings("port", 8080);
        }

        if(!config.containsKey("isSSLEnabled")) {
            config.put("isSSLEnabled", false);
            logger.settings("isSSLEnabled", false);
        }
        JsonObject ssl = config.getJsonObject("ssl");
        if(ssl == null) {
            config.put("ssl", ssl);
            logger.settings("ssl", ssl);
        }
        if(!ssl.containsKey("keystorePath")) {
            logger.settings("ssl.keystorePath", "config/services/bridgeWebService/sslKeyStore.jks");
            ssl.put("keystorePath", "config/services/bridgeWebService/sslKeyStore.jks");
        }
        if(!ssl.containsKey("keystorePassword")) {
            logger.settings("ssl.keystorePassword", "strongPW");
            ssl.put("keystorePassword", "strongPW");
        }

        JsonObject cors = config.getJsonObject("CORS");
        if(cors == null) {
            cors = new JsonObject();
            config.put("CORS", cors);
            logger.settings("CORS", cors);
        }
        if(!cors.containsKey("allowedOriginPattern")) {
            cors.put("allowedOriginPattern", "");
            logger.settings("CORS.allowedOriginPattern", "");
        }

        if(!config.containsKey("isWebSocketBridgeEnabled")) {
            config.put("isWebSocketBridgeEnabled", true);
            logger.settings("isWebSocketBridgeEnabled", true);
        }
        JsonObject webSocketBridge = config.getJsonObject("webSocketBridge");
        if(webSocketBridge == null) {
            webSocketBridge = new JsonObject();
            config.put("webSocketBridge", webSocketBridge);
            logger.settings("webSocketBridge", webSocketBridge);
        }
        if(!webSocketBridge.containsKey("heartbeatInterval")) {
            webSocketBridge.put("heartbeatInterval", 2000);
            logger.settings("webSocketBridge.heartbeatInterval", 2000);
        }
        if(!webSocketBridge.containsKey("replyTimeout")) {
            webSocketBridge.put("replyTimeout", 250000);
            logger.settings("webSocketBridge.replyTimeout", 250000);
        }

        if(!config.containsKey("isPublicStorageHTTPServerEnabled")) {
            config.put("isPublicStorageHTTPServerEnabled", false);
            logger.settings("isPublicStorageHTTPServerEnabled", false);
        }
        JsonObject publicStorageHTTPServer = config.getJsonObject("publicStorageHTTPServer");
        if(publicStorageHTTPServer == null) {
            publicStorageHTTPServer = new JsonObject();
            config.put("publicStorageHTTPServer", publicStorageHTTPServer);
            logger.settings("publicStorageHTTPServer", publicStorageHTTPServer);
        }
        if(!publicStorageHTTPServer.containsKey("isHTTP2Enabled")) {
            publicStorageHTTPServer.put("isHTTP2Enabled", false);
            logger.settings("publicStorageHTTPServer.isHTTP2Enabled", false);
        }
        if(!publicStorageHTTPServer.containsKey("idleTimeoutInSeconds")) {
            publicStorageHTTPServer.put("idleTimeoutInSeconds", 600);
            logger.settings("publicStorageHTTPServer.idleTimeoutInSeconds", 600);
        }
        if(!publicStorageHTTPServer.containsKey("isCachingEnabled")) {
            publicStorageHTTPServer.put("isCachingEnabled", false);
            logger.settings("publicStorageHTTPServer.isCachingEnabled", false);
        }
        JsonObject caching = publicStorageHTTPServer.getJsonObject("caching");
        if(caching == null) {
            caching = new JsonObject();
            publicStorageHTTPServer.put("caching", caching);
            logger.settings("publicStorageHTTPServer.caching", caching);
        }
        if(!caching.containsKey("maxAgeInSeconds")) {
            caching.put("maxAgeInSeconds", 86400);
            logger.settings("publicStorageHTTPServer.caching.maxAgeInSeconds", 86400);
        }
        if(!caching.containsKey("isVaryHeaderEnabled")) {
            caching.put("isVaryHeaderEnabled", true);
            logger.settings("publicStorageHTTPServer.caching.isVaryHeaderEnabled", true);
        }

        if(!config.containsKey("isClientSideLoggingEnabled")) {
            config.put("isClientSideLoggingEnabled", false);
            logger.settings("isClientSideLoggingEnabled", false);
        }
        JsonObject clientSideLogging = config.getJsonObject("clientSideLogging");
        if(clientSideLogging == null) {
            clientSideLogging = new JsonObject();
            config.put("clientSideLogging", clientSideLogging);
            logger.settings("clientSideLogging", clientSideLogging);
        }
        if(!clientSideLogging.containsKey("level")) {
            clientSideLogging.put("level", "ERROR");
            logger.settings("clientSideLogging.level", "ERROR");
        }

        if(!config.containsKey("areServicesAllowedToSetClientTraitConfigs")) {
            config.put("areServicesAllowedToSetClientTraitConfigs", false);
            logger.settings("areServicesAllowedToSetClientTraitConfigs", false);
        }

        JsonObject hashPolicy = config.getJsonObject("hashPolicy");
        if(hashPolicy == null) {
            hashPolicy = new JsonObject();
            config.put("hashPolicy", hashPolicy);
            logger.settings("hashPolicy", hashPolicy);
        }
        if(!hashPolicy.containsKey("factory")) {
            hashPolicy.put("factory", "PBKDF2WithHmacSHA512");
            logger.settings("hashPolicy.factory", "PBKDF2WithHmacSHA512");
        }
        if(!hashPolicy.containsKey("salt")) {
            hashPolicy.put("salt", "bridgeWeb");
            logger.settings("hashPolicy.salt", "bridgeWeb");
        }
        if(!hashPolicy.containsKey("iterations")) {
            hashPolicy.put("iterations", 10);
            logger.settings("hashPolicy.iterations", 10);
        }
        if(!hashPolicy.containsKey("length")) {
            hashPolicy.put("length", 256);
            logger.settings("hashPolicy.length", 256);
        }
    }

    @Override
    public void shutdown(Future<Object> shutdownFuture) {
        if(configCheckPeriodic != null) {
            vertx.cancelTimer(configCheckPeriodic);
        }
        if(httpServer != null) {
            httpServer.close(closeHandler -> {
                if(closeHandler.failed()) {
                    closeHandler.cause().printStackTrace();
                }
                shutdownFuture.complete();
            });
        }
        else {
            shutdownFuture.complete();
        }
    }
}

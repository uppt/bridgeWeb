window.uppuConfig = {
  "title": "UPPT App",
  "isLoginMandatory": true,
  "views": [
    {
      "name": "home",
      "title": "Home",
      "icon": "uppu-icons:home",
      "children": [
        {
          "name": "subhome",
          "title": "Sub Home",
          "icon": "uppu-icons:menu",
          "isLoginRequired": true,
          "onlyForRoles": []
        },
        {
          "name": "subhome2",
          "title": "Sub Home2",
          "icon": "uppu-icons:menu",
          "isLoginRequired": true,
          "onlyForRoles": [],
          "children": [
            {
              "name": "subsubhome",
              "title": "Sub Sub Home",
              "icon": "uppu-icons:menu",
              "isLoginRequired": true,
              "onlyForRoles": []
            },
            {
              "name": "subsubhome2",
              "title": "Sub Sub Home2",
              "icon": "uppu-icons:menu",
              "isLoginRequired": true,
              "onlyForRoles": []
            }
          ]
        }
      ]
    },
    {
      "name": "other1",
      "title": "Other1",
      "icon": "uppu-icons:menu"
    },
    {
      "name": "other2",
      "title": "Other2",
      "icon": "uppu-icons:menu",
      "children": [
        {
          "name": "subother",
          "title": "Sub Other",
          "icon": "uppu-icons:menu",
          "isLoginRequired": true,
          "onlyForRoles": []
        },
        {
          "name": "subhome2",
          "title": "Sub Other 2",
          "icon": "uppu-icons:menu",
          "isLoginRequired": true,
          "onlyForRoles": [],
          "children": [
            {
              "name": "subsubhome",
              "title": "Sub Sub Other",
              "icon": "uppu-icons:menu",
              "isLoginRequired": true,
              "onlyForRoles": []
            },
            {
              "name": "subsubhome2",
              "title": "Sub Sub Other 2",
              "icon": "uppu-icons:menu",
              "isLoginRequired": true,
              "onlyForRoles": []
            }
          ]
        }
      ]
    },
    {
      "name": "more",
      "title": "More",
      "icon": "uppu-icons:menu"
    }
  ]
};

(function(window) {
    'use strict';

    function initDataBus() {
        // private vars and functions
        var subscriptions = {};
        var localData = {"session": {"1": {}}, "config": {"1": {}}};
        var localDataOriginMap = {};
        var backendLoggingConfig = {
            isBackendLoggingEnabled: false,
            loggingLevel: 1
        }

        var eventBus = null;

        // get a value from the local/cached data
        var getLocal = function(domain, version, jsonPath) {
            if(!localData[domain][version]) {
                if(!localData[domain]) {
                    localData[domain] = {};
                }
                if(!localData[domain][version]) {
                    localData[domain][version] = {};
                }
            }
            return JSONPath(localData[domain][version], jsonPath, localDataOriginMap);
        };

        // set a value in the local/cached data
        // TODO: very difficult to read and not well performing. To be improved...
        // TODO: localDataOriginMap is a hack which requires a) a thought-through concept when which source is set b) normalization of paths (e.g. obj.["sth"] and obj.sth are not recognized as equal yet)
        var setLocal = function(domain, version, absolutePath, value, origin) {
            if(!absolutePath || !version || !domain) {
                return false;
            }
            if(absolutePath.startsWith("$.")) {
                absolutePath = absolutePath.substring(2);
            }
            // make sure domain.version is always an object. Otherwise version 1 and 1.1 will cause exceptions
            if(!localData[domain]) {
                localData[domain] = {};
            }
            if(!localData[domain][version]) {
                localData[domain][version] = {};
            }
            if(!origin) {
                origin = "unknown";
            }

            var tokens = absolutePath.split(".");
            var arrayPattern = /(?:\[(.+?)\])/g
            var numberPattern = /^\d+$/;
            var currentPosition = localData[domain][version];
            for(var i=0;i<tokens.length;i++) {
                var token = tokens[i];
                var matches = token.match(arrayPattern);
                if(matches !== null) {
                    matches.unshift(token.substring(0, token.indexOf("[")));
                    for(var a=1;a<matches.length;a++) {
                        matches[a] = matches[a].substring(1, matches[a].length-1).replace(/[\"']/g, "");
                        if(matches[a].match(numberPattern)) {
                            matches[a] = parseInt(matches[a]);
                        }
                    }
                    var previousPosition = null;
                    for(var a=0;a<matches.length;a++) {
                        if(!currentPosition[matches[a]]) {
                            if(typeof(matches[a+1]) === "number") {
                                currentPosition[matches[a]] = [];
                            }
                            else {
                                currentPosition[matches[a]] = {};
                            }
                        }
                        i=i+1;
                        tokens.splice(i, 0, matches[a]);
                        previousPosition = currentPosition;
                        currentPosition = currentPosition[matches[a]];
                    }
                    if(i === tokens.length -1) {
                        previousPosition[matches[matches.length-1]] = value;
                        localDataOriginMap[absolutePath] = origin;
                        return true;
                    }
                }
                else {
                    if(!currentPosition[token]) {
                        if(i === tokens.length -1) {
                            currentPosition[token] = value;
                            localDataOriginMap[absolutePath] = origin;
                            return true;
                        }
                        else {
                            currentPosition[token] = {};
                            currentPosition = currentPosition[token];
                        }
                    }
                    else if(i === tokens.length -1) {
                        currentPosition[token] = value;
                        localDataOriginMap[absolutePath] = origin;
                        return true;
                    }
                    else {
                        currentPosition = currentPosition[token];
                    }
                }
            }
            return false;
        };

        var forwardOperationToSolution = function(remoteOperation, domain, version, path, value, origin) {
            return new Promise(function(resolve, reject) {
                if((localData.session['1'].status !== "authenticated" && localData.session['1'].status !== "anonymous") || !eventBus) {
                    reject("Not connected! Session status: " + localData.session['1'].status);
                    return;
                }

                connectionHandlingImpl.getSessionToken()
                    .then(function(sessionToken) {
                        eventBus.send(window.dataBusSettings.routerDomain + "." + window.dataBusSettings.routerVersion + "." + window.dataBusSettings.routerType, {"requests": [{
                            "domain": domain,
                            "version": version,
                            "path": path,
                            "value": value !== null? value : "",
                            "operation": remoteOperation,
                            "origin": origin
                            }]}, {"sessionToken": sessionToken, "correlationID": generateID()}, function(error, reply) {
                                if(error != null || !reply.headers) {
                                    DataBus.logger.error("DataBus operation " + remoteOperation + " on " + path + " caused an error: " + error, reply.headers.correlationID? reply.headers.correlationID:"unknownCID");
                                    reject(error);
                                    return;
                                }
                                if(reply.headers.statusCode != 200) {
                                    DataBus.logger.error("[" + reply.headers.correlationID + "] DataBus operation " + remoteOperation + " on " + path + " caused an error: " + body.message, reply.headers.correlationID? reply.headers.correlationID:"unknownCID");
                                    reject(body.message);
                                    return;
                                }
                                reply.body.results[0].correlationID = reply.headers.correlationID;
                                resolve(reply.body.results[0]);
                        });
                    })
                    .catch(reject);
            });
        };

        // handle a databus get/set/delete operation
        var publishOrSend = function(operation, domain, version, path, jsonPath, value, origin, correlationID) {
            return new Promise(function(resolve, reject) {
                if(!origin) {
                    origin = "unknown";
                }
                if(!operation) {
                    reject("Must provide the operation get-/set-/del- Local/Global!");
                    return;
                }
                if(!domain || !version) {
                    reject("Must provide domain and version!");
                    return;
                }
                if(!correlationID) {
                    correlationID = generateID();
                }
                var observerNotifier = function(result) {
                    for(var index in result.paths) {
                        var path = result.paths[index];
                        var observerMessage = {"operation": result.operation, "paths": [path], "jsonPath": result.jsonPath, "values": [result.values[index]], "origins": [result.origins[index]], "domain": domain, "version": version, "correlationID": result.correlationID};
                        for(var id in subscriptions) {
                            if(subscriptions[id].domain === domain && subscriptions[id].version === version) {
                                if(subscriptions[id].absolute && path === subscriptions[id].absolute && typeof(subscriptions[id].onNext) === 'function') {
                                    try {
                                        subscriptions[id].onNext(observerMessage, subscriptions[id]);
                                    }
                                    catch(e) {
                                        DataBus.logger.error("[" + result.correlationID + "] Prefix callback " + id + " for " + path + " caused an exception: " + e);
                                    }
                                }
                                else if(subscriptions[id].prefix && path.startsWith(subscriptions[id].prefix) && typeof(subscriptions[id].onNext) === 'function') {
                                    try {
                                        subscriptions[id].onNext(observerMessage, subscriptions[id]);
                                    }
                                    catch(e) {
                                        DataBus.logger.error("[" + result.correlationID + "] Prefix callback " + id + " for " + path + " caused an exception: " + e);
                                    }
                                }
                                else if(subscriptions[id].pattern && path.match(subscriptions[id].pattern) !== null && typeof(subscriptions[id].onNext) === 'function') {
                                    try {
                                        subscriptions[id].onNext(observerMessage, subscriptions[id]);
                                    }
                                    catch(e) {
                                        DataBus.logger.error("[" + result.correlationID + "] Pattern callback " + id + " for " + path + " caused an exception: " + e);
                                    }
                                }
                            }
                        }
                    }
                };

                if(operation === "get" && (!eventBus || (localData.session['1'].status !== "authenticated" && localData.session['1'].status !== "anonymous"))) {
                    DataBus.logger.warn("[" + correlationID + "] Not connected, operation 'get' only performed on local cache!");
                    operation = "getLocal";
                }
                else if(operation === "set" && (!eventBus || localData.session['1'].status !== "authenticated")) {
                    DataBus.logger.warn("[" + correlationID + "] Not connected or not authenticated, operation 'set' only performed on local cache!");
                    operation = "setLocal";
                }
                else if(operation === "del" && (!eventBus || localData.session['1'].status !== "authenticated")) {
                    DataBus.logger.warn("[" + correlationID + "] Not connected or not authenticated, operation 'set' only performed on local cache!");
                    operation = "delLocal";
                }

                switch(operation) {
                    case "get":
                        // try local data first, only send to server if no local data available
                        var resultObject = getLocal(domain, version, jsonPath);
                        if(resultObject && resultObject.values && resultObject.paths && resultObject.values.length > 0) {
                            var result = {"operation": operation, "domain": domain, "version": version, "paths": resultObject.paths, "jsonPath": jsonPath, "origins": resultObject.origins, "correlationID": correlationID, "values": resultObject.values};
                            resolve(result);
                            return;
                        }
                        // else: go on to get remote
                    case "getRemote":
                        forwardOperationToSolution("GET", domain, version, jsonPath, value, origin)
                            .then(function(result) {
                                result.operation = operation;
                                result.jsonPath = jsonPath;

                                resolve(result);
                                // also store result locally and notify observers...
                                for(var i=0;i<result.paths.length;i++) {
                                    if(setLocal(domain, version, result.paths[i], result.values[i], result.origins[i])) {
                                        var cacheResult = {"operation": operation, "domain": domain, "version": version, "paths": [result.paths[i]], "jsonPath": "", "origins": [result.origins[i]], "correlationID": result.correlationID, "values": [result.values[i]]};
                                        observerNotifier(cacheResult);
                                    }
                                    else {
                                        DataBus.logger.error("Storing remotely retrieved value locally failed! " + domain +"."+ version+":"+  result.paths[i] + ": " + result.values[i], result.correlationID );
                                    }
                                }
                            })
                            .catch(function(error) {
                                reject(error);
                            });
                        break;
                    case "set":
                    case "setRemote":
                        forwardOperationToSolution("SET", domain, version, path, value, origin)
                            .then(function(result) {
                                result.operation = operation;
                                resolve(result);

                                // also store result locally and notify observers...
                                for(var i=0;i<result.paths.length;i++) {
                                    if(setLocal(domain, version, result.paths[i], result.values[i], origin)) {
                                        var cacheResult = {"operation": operation, "domain": domain, "version": version, "paths": [result.paths[i]], "jsonPath": "", "origins": [result.origins[i]], "correlationID": result.correlationID, "values": [result.values[i]]};
                                        observerNotifier(cacheResult);
                                    }
                                    else {
                                        DataBus.logger.error("Storing remotely set value locally failed! " + domain +"."+ version+":"+  result.paths[i] + ": " + result.values[i], result.correlationID);
                                    }
                                }
                            })
                            .catch(function(error) {
                                reject(error);
                            });
                        break;
                    case "del":
                    case "delRemote":
                        forwardOperationToSolution("DEL", domain, version, path, "", origin)
                        .then(function(result) {
                            result.operation = operation;
                            result.values[0] = null;
                            resolve(result);

                            // also store result locally and notify observers...
                            for(var i=0;i<result.paths.length;i++) {
                                if(setLocal(domain, version, result.paths[i], null, origin)) {
                                    var cacheResult = {"operation": operation, "domain": domain, "version": version, "paths": [result.paths[i]], "jsonPath": "", "origins": [result.origins[i]], "correlationID": result.correlationID, "values": [null]};
                                    observerNotifier(cacheResult);
                                }
                                else {
                                    DataBus.logger.error("Deleting (setting to null) remotely deleted value locally failed! " + domain +"."+ version+":"+  result.paths[i] + ": " + result.values[i], result.correlationID);
                                }
                            }
                        })
                        .catch(function(error) {
                            reject(error);
                        });
                        break;
                    case "getLocal":
                        var resultObject = getLocal(domain, version, jsonPath);
                        if(resultObject !== null) {
                            var result = {"operation": operation, "domain": domain, "version": version, "paths": resultObject.paths, "jsonPath": jsonPath, "origins": resultObject.origins, "correlationID": correlationID, "values": resultObject.values};
                            resolve(result);
                        }
                        else {
                            reject("[" + correlationID + "] getLocal failed! Check that correct jsonPath was provided!");
                        }
                        break;
                    case "setPush":
                    case "setLocal":
                        if(setLocal(domain, version, path, value, origin)) {
                            var result = {"operation": operation, "domain": domain, "version": version, "paths": [path], "jsonPath": "", "origins": [origin], "correlationID": correlationID, "values": [value]};
                            resolve(result);
                            observerNotifier(result);
                        }
                        else {
                            reject("[" + correlationID + "] Setting local data failed!");
                        }
                        break;
                    case "delLocal":
                        if(setLocal(domain, version, path, null, origin)) {
                            delete localDataOriginMap[path];
                            var result = {"operation": operation, "domain": domain, "version": version, "paths": [path], "jsonPath": "", "origins": [origin], "correlationID": generateID(), "values": [null]};
                            resolve(result);
                            observerNotifier(result);
                        }
                        else {
                            reject("[" + correlationID + "] Deleting (setting to null) local data failed!");
                        }
                        break;
                    default:
                        reject("Operation " + operation + " is not known/unsupported!");
                        break;
                }
            });
        };


        var loggerImpl = function(level, message, correlationID) {
            if(!correlationID) { correlationID = "unknownCID"; }

            var logMessage = message;

            switch(level) {
                case 1:
                    logMessage = "[" + correlationID + "] FATAL: " + logMessage;
                    break;
                case 2:
                    logMessage = "[" + correlationID + "] ERROR: " + logMessage;
                    break;
                case 3:
                    logMessage = "[" + correlationID + "] Warn: " + logMessage;
                    break;
                case 4:
                    logMessage = "[" + correlationID + "] Info: " + logMessage;
                    break;
                default:
                    level = 1;
                    logMessage = "[" + correlationID + "] UNKNOWN level: " + logMessage;
                    break;
            }

            if(console) {
                if(level == 1 && console.error) {
                    console.error(logMessage);
                }
                else if(level == 2 && console.error) {
                    console.error(logMessage);
                }
                else if(level == 3) {
                    console.warn(logMessage);
                }
                else {
                    console.log(logMessage);
                }
            }
            if(print) {
                print(logMessage);
            }
            else if(consoleLogInfo) {
                consoleLogInfo(logMessage);
            }

            // send log message to backend, if enabled and session available
            if(backendLoggingConfig.isBackendLoggingEnabled && (localData.session['1'].status == "authenticated" || localData.session['1'].status == "anonymous")) {
                connectionHandlingImpl.getSessionToken()
                     .then(function(sessionToken) {
                            eventBus.send("in.logging.1.log", {"message": logMessage, "level": level}, {"sessionToken": sessionToken, "correlationID": correlationID});
                        })
                     .catch(function(error) {});
            }
        };

        var connectionHandlingImpl = {
            getStoredSessionToken: function() {
                //TODO
                return "";
            },
            // setup anonymous session
            sessionMissing: function(correlationID) {
                return new Promise(function(resolve, reject) {
                    correlationID = correlationID? correlationID : generateID();

                    // TODO: check eventbus status

                    var headers = { correlationID: correlationID };
                    var payload = {};
                    var sessionContext = getDeviceInfos();
                    sessionContext.pathName = window.location.pathname;
                    sessionContext.routerDomain = window.dataBusSettings.routerDomain;
                    sessionContext.routerVersion = window.dataBusSettings.routerVersion;
                    sessionContext.routerType = window.dataBusSettings.routerType;
                    payload.sessionContext = sessionContext;

                    eventBus.send("in.sessions.1.sessionMissing", payload, headers, function(error, reply) {
                         if(error != null) {
                            DataBus.setLocal("session", "1", "status", "offline", "DataBus.sessionMissing");
                            reject(error.failureCode + ": Session setup caused an error: " + error, that.correlationID);
                            return;
                         }
                         if(reply.headers.statusCode != 200) {
                            DataBus.setLocal("session", "1", "status", "offline", "DataBus.sessionMissing");
                            reject(reply.headers.statusCode + ": Session setup caused an error: " + reply.body.message, that.correlationID);
                            return;
                         }

                          var userReply = reply.body.user;
                          var configReply = reply.body.config;
                          var sessionTokenReply = userReply.sessionToken;

                          eventBus.registerHandler(configReply.addressDomain + "." + configReply.addressVersion + "." + configReply.addressType, {"correlationID": correlationID, "sessionToken": sessionTokenReply}, function(error, message) {
                                 if(error) {
                                     DataBus.logger.error("Failed to receive a message from the session address: " + error, correlationID);
                                 }
                                 else if(message.headers.statusCode !== "200") {
                                     DataBus.logger.error("Received a push error (" + message.headers.statusCode + ") message: " + message.body.message, message.headers.correlationID? message.headers.correlationID : correlationID);
                                 }
                                 else if(!message.body.results) {
                                     DataBus.logger.error("Received an invalid push message: " + message.body.message, message.headers.correlationID? message.headers.correlationID : correlationID);
                                 }
                                 else {
                                     var results = message.body.results;
                                     for(var a=0;a<results.length;a++) {
                                         for(var i=0;i<results[a].paths.length;i++) {
                                             publishOrSend("setPush", results[a].domain, results[a].version, results[a].paths[i], "", results[a].values[i], results[a].origins[i], message.headers.correlationID);
                                         }
                                     }
                                 }
                            });

                          connectionHandlingImpl.setSessionToken(sessionTokenReply)
                              .then(function() {
                                    backendLoggingConfig.isBackendLoggingEnabled = configReply.isBackendLoggingEnabled;
                                    backendLoggingConfig.loggingLevel = configReply.backendLoggingLevel;
                                    DataBus.setLocal("session", "1", "addressDomain", configReply.addressDomain, "DataBus.sessionMissing");
                                    DataBus.setLocal("session", "1", "addressVersion", configReply.addressVersion, "DataBus.sessionMissing");
                                    DataBus.setLocal("session", "1", "addressType", configReply.addressType, "DataBus.sessionMissing");
                                    DataBus.setLocal("session", "1", "user", userReply, "DataBus.sessionMissing");
                                    DataBus.setLocal("session", "1", "traits", configReply.traits, "DataBus.sessionMissing");
                                    DataBus.setLocal("session", "1", "source", configReply.source, "DataBus.sessionMissing");
                                    DataBus.setLocal("session", "1", "status", "session", "DataBus.sessionMissing");
                                    resolve();
                              })
                              .catch(reject);
                    });
                });
            },
            // preflight login via email (check if email is known, caching application state)
            authViaEMailForeseeable: function(emailAddress, correlationID) {
                return new Promise(function(resolve, reject) {
                    if(!emailAddress) {
                        reject("Must provide emailAddress!");
                        return;
                    }

                    correlationID = correlationID? correlationID : generateID();

                    var headers = { correlationID: correlationID };
                    var payload = {authType: "email",
                                   authConfig: {
                                       "emailAddress": emailAddress,
                                   }};

                    eventBus.send("in.sessions.1.authenticationPreflight", payload, headers, function(error, reply) {
                         if(error != null) {
                            reject(error.failureCode + ": Authentication preflight caused an error: " + error, correlationID);
                            return;
                         }
                         if(reply.headers.statusCode != 202) {
                            reject(reply.headers.statusCode + ": Authentication preflight failed: " + reply.body.message, correlationID);
                            return;
                         }

                         resolve(emailAddress);
                     });
                });
            },
            authViaEmail: function(emailAddress, password, correlationID) {

            },
            getIsSaveSessionTokenAllowed: function() {
                return new Promise(function(resolve, reject) {
                    DataBus.getLocal("session", "1", "isSaveSessionTokenAllowed")
                        .then(function(saveSessionTokenAllowed) {
                            if(saveSessionTokenAllowed !== "undefined" && saveSessionTokenAllowed !== null) {
                                resolve(saveSessionTokenAllowed);
                            }
                            else {
                                // has not been set yet
                                // check if there is stored session token - if so, then it is allowed
                                resolve(localStorage && localStorage.getItem('sessionToken'));
                            }
                        })
                        .catch(reject);
                });
            },
            setIsSaveSessionTokenAllowed: function(isSaveSessionTokenAllowed) {
                return new Promise(function(resolve, reject) {
                     var saveSessionTokenAllowed = false;
                     if(isSaveSessionTokenAllowed) {
                        saveSessionTokenAllowed = isSaveSessionTokenAllowed;
                     }

                     DataBus.setLocal("session", "1", "isSaveSessionTokenAllowed", saveSessionTokenAllowed, "DataBus.setIsSaveSessionTokenAllowed")
                         .then(function() {
                             resolve(saveSessionTokenAllowed);
                         })
                         .catch(reject);
                });
            },
            getSessionToken: function() {
                return new Promise(function(resolve, reject) {
                     if(localStorage && localStorage.getItem('sessionToken')) {
                        resolve(window.atob(localStorage.getItem('sessionToken')));
                     }
                     else if(sessionStorage && sessionStorage.getItem('sessionToken')) {
                        resolve(window.atob(sessionStorage.getItem('sessionToken')));
                     }
                     else {
                        DataBus.getLocal("session", "1", "sessionToken")
                            .then(function(result) {
                                var sessionToken = result.values && result.values[0]? result.values[0] : "";
                                resolve(sessionToken);
                            })
                            .catch(reject);
                     }
                });
            },
            setSessionToken: function(sessionToken) {
                return new Promise(function(resolve, reject) {
                    if(!sessionToken  || sessionToken === "") {
                        if(localStorage) {
                            localStorage.setItem('sessionToken', null);
                        }
                        if(sessionStorage) {
                            sessionStorage.setItem('sessionToken', null);
                        }
                        DataBus.delLocal("session", "1", "sessionToken", "", "DataBus.setSessionToken")
                          .then(resolve)
                          .catch(reject);
                    }
                    else {
                        connectionHandlingImpl.getIsSaveSessionTokenAllowed()
                            .then(function(isSaveSessionTokenAllowed) {
                                if(isSaveSessionTokenAllowed) {
                                    if(localStorage) {
                                        localStorage.setItem('sessionToken', window.btoa(sessionToken));
                                    }
                                    else if(sessionStorage) {
                                        sessionStorage.setItem('sessionToken', window.btoa(sessionToken));
                                    }

                                    DataBus.setLocal("session", "1", "sessionToken", sessionToken, "DataBus.setSessionToken")
                                      .then(resolve);

                                }
                                else {
                                    if(sessionStorage) {
                                        sessionStorage.setItem('sessionToken', window.btoa(sessionToken));
                                    }
                                    DataBus.setLocal("session", "1", "sessionToken", sessionToken, "DataBus.setSessionToken")
                                       .then(resolve);

                                    //clean up previously stored session token
                                    if(localStorage) {
                                         localStorage.setItem('sessionToken', null);
                                    }
                                }
                            })
                            .catch(reject);
                        }
                });
            },
            sessionMissingOLD: function(correlationID) {
                return new Promise(function(resolve, reject) {
                    if(!correlationID) {
                        correlationID = generateID();
                    }

                    var createAnonymousSession = function() {
                        var headers = { correlationID: correlationID };
                        var payload = {};
                        var sessionContext = getDeviceInfos();
                        sessionContext.pathName = window.location.pathname;
                        sessionContext.routerDomain = window.dataBusSettings.routerDomain;
                        sessionContext.routerVersion = window.dataBusSettings.routerVersion;
                        sessionContext.routerType = window.dataBusSettings.routerType;
                        payload.sessionContext = sessionContext;

                        eventBus.send("in.sessions.1.sessionMissing", payload, headers, function(error, reply) {
                             if(error != null) {
                                DataBus.setLocal("session", "1", "status", "disconnected", "DataBus.sessionMissing");
                                reject(error.failureCode + ": Session setup caused an error: " + error, that.correlationID);
                                return;
                             }
                             if(reply.headers.statusCode != 200) {
                                DataBus.setLocal("session", "1", "status", "disconnected", "DataBus.sessionMissing");
                                reject(reply.headers.statusCode + ": Session setup caused an error: " + reply.body.message, that.correlationID);
                                return;
                             }

                              var userReply = reply.body.user;
                              var configReply = reply.body.config;
                              var sessionTokenReply = userReply.sessionToken;

                              eventBus.registerHandler(configReply.addressDomain + "." + configReply.addressVersion + "." + configReply.addressType, {"correlationID": correlationID, "sessionToken": sessionTokenReply}, function(error, message) {
                                     if(error) {
                                         DataBus.logger.error("Failed to receive a message from the session address: " + error, correlationID);
                                     }
                                     else if(message.headers.statusCode !== "200") {
                                         DataBus.logger.error("Received a push error (" + message.headers.statusCode + ") message: " + message.body.message, message.headers.correlationID? message.headers.correlationID : correlationID);
                                     }
                                     else if(!message.body.results) {
                                         DataBus.logger.error("Received an invalid push message: " + message.body.message, message.headers.correlationID? message.headers.correlationID : correlationID);
                                     }
                                     else {
                                         var results = message.body.results;
                                         for(var a=0;a<results.length;a++) {
                                             for(var i=0;i<results[a].paths.length;i++) {
                                                 publishOrSend("setPush", results[a].domain, results[a].version, results[a].paths[i], "", results[a].values[i], results[a].origins[i], message.headers.correlationID);
                                             }
                                         }
                                     }
                                });

                              connectionHandlingImpl.setSessionToken(sessionTokenReply)
                                  .then(function() {
                                        backendLoggingConfig.isBackendLoggingEnabled = configReply.isBackendLoggingEnabled;
                                        backendLoggingConfig.loggingLevel = configReply.backendLoggingLevel;
                                        DataBus.setLocal("session", "1", "addressDomain", configReply.addressDomain, "DataBus.sessionMissing");
                                        DataBus.setLocal("session", "1", "addressVersion", configReply.addressVersion, "DataBus.sessionMissing");
                                        DataBus.setLocal("session", "1", "addressType", configReply.addressType, "DataBus.sessionMissing");
                                        DataBus.setLocal("session", "1", "user", userReply, "DataBus.sessionMissing");
                                        DataBus.setLocal("session", "1", "traits", configReply.traits, "DataBus.sessionMissing");
                                        DataBus.setLocal("session", "1", "source", configReply.source, "DataBus.sessionMissing");
                                        DataBus.setLocal("session", "1", "status", "anonymous", "DataBus.sessionMissing")
                                            .then(function() {
                                                resolve({"user": userReply, "addressDomain": configReply.addressDomain, "addressVersion": configReply.addressVersion, "addressType": configReply.addressType, "status": "anonymous"});
                                            })
                                            .catch(function(error) {
                                                reject(error, that.correlationID);
                                            });
                                  })
                                  .catch(reject);
                        });
                    };

                    // make sure the event bus is set up
                    if(!eventBus || !eventBus.state ||  eventBus.state == 2 ||  eventBus.state == 3) {
                      var eventBusOptions = {
                          vertxbus_reconnect_attempts_max: Infinity, // Max reconnect attempts
                          vertxbus_reconnect_delay_min: 1000, // Initial delay (in ms) before first reconnect attempt
                          vertxbus_reconnect_delay_max: 15000, // Max delay (in ms) between reconnect attempts
                          vertxbus_reconnect_exponent: 2, // Exponential backoff factor
                          vertxbus_randomization_factor: 0.5 // Randomization factor between 0 and 1
                      };

                      eventBus = new EventBus(window.dataBusSettings.location + "/eventbus/websocket", eventBusOptions);
                      eventBus.enableReconnect(true);
                      eventBus.onopen = createAnonymousSession;
                      eventBus.onreconnect = createAnonymousSession;

                      eventBus.onclose = function(e) {
                          DataBus.getStatus()
                              .then(function(currentSessionStatus) {
                                  if("disconnected" !== currentSessionStatus && "DataBus.disconnect" !== e) {
                                    // this is an involuntary disconnect!
                                     DataBus.setLocal("session", "1", "status", "offline", "DataBus.onClose");
                                     DataBus.logger.warn("Session was disconnected involuntarily: " + currentSessionStatus + "-" + e);
                                  }
                                  else {
                                     DataBus.setLocal("session", "1", "status", "disconnected", "DataBus.onClose");
                                  }
                                });
                      };
                   }
                   else if(eventBus.state == 1) {
                       createAnonymousSession();
                   }
                   else {
                        DataBus.logger.warn("evenBus already connecting... not invoking getSessionConnection");
                   }
                });
            },
            authenticationMissing: function(email, password, correlationID) {
                return new Promise(function(resolve, reject) {
                    if(!correlationID) {
                        correlationID = generateID();
                    }
                    if(!email || !password) {
                        reject("Must provide email and password!", correlationID);
                        return;
                    }

                    DataBus.getStatus()
                        .then(function(currentSessionStatus) {
                            connectionHandlingImpl.getSessionToken()
                                .then(function(sessionToken) {
                                    if(currentSessionStatus != "anonymous" || !sessionToken) {
                                        reject("Must have an established session for authenticating it!", correlationID);
                                        return;
                                    }

                                    var headers = {
                                        correlationID: correlationID,
                                        sessionToken: sessionToken
                                    };
                                    var payload = {
                                        authType: "email",
                                        authConfig: {
                                            "email": email,
                                            "password": password
                                        }
                                    }

                                    eventBus.send("in.sessions.1.authenticationMissing", payload, headers, function(error, reply) {
                                         if(error != null) {
                                            reject(error.failureCode + ": Session authentication caused an error: " + error, that.correlationID);
                                            return;
                                         }
                                         if(reply.headers.statusCode != 200) {
                                            reject(reply.headers.statusCode + ": Session authentication caused an error: " + reply.body.message, that.correlationID);
                                            return;
                                         }

                                         var userReply = reply.body.user;

                                         connectionHandlingImpl.setSessionToken(sessionToken)
                                              .then(function() {
                                                    DataBus.setLocal("session", "1", "user", userReply, "DataBus.sessionAndAuthenticationMissing");
                                                    DataBus.setLocal("session", "1", "status", "authenticated", "DataBus.sessionAndAuthenticationMissing")
                                                        .then(function() {
                                                            resolve({"user": userReply, "status": "authenticated"});
                                                        })
                                                        .catch(function(error) {
                                                            reject(error, that.correlationID);
                                                        });
                                              })
                                              .catch(reject);
                                     });
                                })
                                .catch(reject);
                        })
                        .catch(function(error) {
                            reject("Failed to get current session status!");
                            return;
                        });
                });
            },
            sessionAndAuthenticationMissing: function(email, password, sessionToken, correlationID) {
                return new Promise(function(resolve, reject) {
                    if(!correlationID) {
                        correlationID = generateID();
                    }

                    if(!sessionToken && (!email || !password)) {
                        reject("Must provide either session token or email/password!", correlationID);
                        return;
                    }

                    var headers = { correlationID: correlationID };

                    var payload = {};
                    var sessionContext = getDeviceInfos();
                    sessionContext.pathName = window.location.pathname;
                    sessionContext.routerDomain = window.dataBusSettings.routerDomain;
                    sessionContext.routerVersion = window.dataBusSettings.routerVersion;
                    sessionContext.routerType = window.dataBusSettings.routerType;
                    payload.sessionContext = sessionContext;

                    if(email && password) {
                        payload.authType = "email";
                        payload.authConfig = {"email": email, "password": password};
                    }
                    else {
                        headers.sessionToken = sessionToken;
                    }


                    eventBus.send("in.sessions.1.sessionAndAuthenticationMissing", payload, headers, function(error, reply) {
                         if(error != null) {
                            reject(error.failureCode + ": Creating authenticated session caused an error: " + error, correlationID);
                            return;
                         }
                         if(reply.headers.statusCode != 200) {
                            reject(reply.headers.statusCode + ": Creating authenticated session caused an error: " + reply.body.message, correlationID);
                            return;
                         }

                         var userReply = reply.body.user;
                         var configReply = reply.body.config;
                         var sessionTokenReply = userReply.sessionToken;

                         eventBus.registerHandler(configReply.addressDomain + "." + configReply.addressVersion + "." + configReply.addressType, {"correlationID": correlationID, "sessionToken": sessionTokenReply}, function(error, message) {
                                if(error) {
                                    DataBus.logger.error("Failed to receive a message from the session address: " + error, correlationID);
                                }
                                else if(message.headers.statusCode !== "200") {
                                    DataBus.logger.error("Received a push error (" + message.headers.statusCode + ") message: " + message.body.message, message.headers.correlationID? message.headers.correlationID : correlationID);
                                }
                                else if(!message.body.results) {
                                    DataBus.logger.error("Received an invalid push message: " + message.body.message, message.headers.correlationID? message.headers.correlationID : correlationID);
                                }
                                else {
                                    var results = message.body.results;
                                    for(var a=0;a<results.length;a++) {
                                        for(var i=0;i<results[a].paths.length;i++) {
                                            publishOrSend("setPush", results[a].domain, results[a].version, results[a].paths[i], "", results[a].values[i], results[a].origins[i], message.headers.correlationID);
                                        }
                                    }
                                }
                           });

                         connectionHandlingImpl.setSessionToken(sessionTokenReply)
                             .then(function() {
                                   backendLoggingConfig.isBackendLoggingEnabled = configReply.isBackendLoggingEnabled;
                                   backendLoggingConfig.loggingLevel = configReply.backendLoggingLevel;
                                   DataBus.setLocal("session", "1", "addressType", configReply.addressType, "DataBus.sessionAndAuthenticationMissing");
                                   DataBus.setLocal("session", "1", "addressDomain", configReply.addressType, "DataBus.sessionAndAuthenticationMissing");
                                   DataBus.setLocal("session", "1", "addressVersion", configReply.addressType, "DataBus.sessionAndAuthenticationMissing");
                                   DataBus.setLocal("session", "1", "user", userReply, "DataBus.sessionAndAuthenticationMissing");
                                   DataBus.setLocal("session", "1", "traits", configReply.traits, "DataBus.sessionAndAuthenticationMissing");
                                   DataBus.setLocal("session", "1", "source", configReply.source, "DataBus.sessionAndAuthenticationMissing");
                                   DataBus.setLocal("session", "1", "status", "authenticated", "DataBus.sessionAndAuthenticationMissing")
                                       .then(function() {
                                           resolve({"user": userReply, "addressDomain": configReply.addressDomain, "addressVersion": configReply.addressVersion, "addressType": configReply.addressType, "status": "authenticated"});
                                       })
                                       .catch(function(error) {
                                           reject(error, that.correlationID);
                                       });
                             })
                             .catch(reject);
                    });

                });
            },
            getStatus: function() {
                return new Promise(function(resolve, reject) {
                    DataBus.getLocal("session", "1", "status")
                       .then(function(result) {
                            var currentSessionStatus = result.values && result.values[0]? result.values[0] : "uninitialized";
                            resolve(currentSessionStatus);
                          })
                       .catch(reject);
                  });
            },
            connect: function(correlationID, isSaveSessionTokenAllowed) {
                var that = this;
                that.correlationID = correlationID? correlationID : generateID();

                return new Promise(function (resolve, reject) {
                    if(window.ononline && !window.navigator.onLine) {
                        reject("Device is offline", correlationID);
                        return;
                    }

                    var getSessionConnection = function() {
                         DataBus.getStatus()
                              .then(function(currentSessionStatus) {

                                if(currentSessionStatus == "uninitialized" || currentSessionStatus == "disconnected" || currentSessionStatus == "offline") {
                                     // create anonymous session
                                     connectionHandlingImpl.sessionMissing(that.correlationID)
                                          .then(resolve)
                                          .catch(reject);
                                          //TODO: authenticate session, if possible
                                 }
                                 else if(currentSessionStatus == "authenticated") {
                                     DataBus.getLocal("session", "1", "$.", "DataBus.connect")
                                        .then(function(result) {
                                            var user = result.values[0].user;
                                            var addressDomain = result.values[0].addressDomain;
                                            var addressVersion = result.values[0].addressVersion;
                                            var addressType = result.values[0].addressType;
                                            var status = result.values[0].status;

                                            if(!user || !addressDomain || !addressVersion || !addressType || !status) {
                                                reject("Session is already marked as connected and authenticated but something is wrong. One of the following values is not set: user, addressDomain, addressVersion, addressType, status");
                                                return;
                                            }

                                            resolve({"user": user, "addressDomain": addressDomain, "addressVersion": addressVersion, "addressType": addressType, "status": status});
                                        })
                                        .catch(reject);
                                     return;
                                 }
                                 else if(currentSessionStatus == "anonymous") {
                                     if(that.email && that.password) {
                                         connectionHandlingImpl.authenticationMissing(that.email, that.password, that.correlationID)
                                             .then(function(result) {
                                                 that.email = null;
                                                 that.password = null;
                                                 resolve(result);
                                             })
                                             .catch(function(error) {
                                                 that.email = null;
                                                 that.password = null;
                                                 reject("Failed to upgrade anonymous session via email/password authentication!" + error, that.correlationID);
                                             });
                                     }
                                     else {
                                        reject("Can not upgrade anonymous session without email/password provided!");
                                     }
                                 }
                                 else {
                                     reject("Current session status '" + currentSessionStatus + "' is not known/supported!", that.correlationID);
                                 }
                              })
                              .catch(function(error) {
                                 reject("DataBus seems to be down!", that.correlationID);
                              });
                     };

                    getSessionConnection();
                });
            },
            connectOLD: function(email, password, correlationID, isSaveSessionTokenAllowed) {
                 var that = this;
                 that.email = email ? email : null;
                 that.password = password ? password : null;
                 that.correlationID = correlationID? correlationID : generateID();

                 return new Promise(function(resolve, reject) {
                     if(window.ononline && !window.navigator.onLine) {
                         reject("Device is offline", correlationID);
                         return;
                     }

                     var getSessionConnection = function() {
                         DataBus.getStatus()
                              .then(function(currentSessionStatus) {
                                 if(currentSessionStatus == "authenticated") {
                                     DataBus.getLocal("session", "1", "$.", "DataBus.connect")
                                        .then(function(result) {
                                            var user = result.values[0].user;
                                            var addressDomain = result.values[0].addressDomain;
                                            var addressVersion = result.values[0].addressVersion;
                                            var addressType = result.values[0].addressType;
                                            var status = result.values[0].status;

                                            if(!user || !addressDomain || !addressVersion || !addressType || !status) {
                                                reject("Session is already marked as connected and authenticated but something is wrong. One of the following values is not set: user, addressDomain, addressVersion, addressType, status");
                                                return;
                                            }

                                            resolve({"user": user, "addressDomain": addressDomain, "addressVersion": addressVersion, "addressType": addressType, "status": status});
                                        })
                                        .catch(reject);
                                     return;
                                 }
                                 else if(currentSessionStatus == "anonymous") {
                                     if(that.email && that.password) {
                                         connectionHandlingImpl.authenticationMissing(that.email, that.password, that.correlationID)
                                             .then(function(result) {
                                                 that.email = null;
                                                 that.password = null;
                                                 resolve(result);
                                             })
                                             .catch(function(error) {
                                                 that.email = null;
                                                 that.password = null;
                                                 reject("Failed to upgrade anonymous session via email/password authentication!" + error, that.correlationID);
                                             });
                                     }
                                     else {
                                        reject("Can not upgrade anonymous session without email/password provided!");
                                     }
                                 }
                                 else if(currentSessionStatus == "uninitialized" || currentSessionStatus == "disconnected" || currentSessionStatus == "offline") {
                                     if(that.email && that.password) {
                                        // perform session setup incl. authentication
                                        connectionHandlingImpl.sessionAndAuthenticationMissing(that.email, that.password, null, that.correlationID)
                                            .then(function(result) {
                                                resolve(result);
                                            })
                                            .catch(reject);
                                     }
                                     else {
                                         connectionHandlingImpl.getSessionToken()
                                             .then(function(sessionToken) {
                                                if(sessionToken) {
                                                    // perform a session handover
                                                    connectionHandlingImpl.sessionAndAuthenticationMissing(null, null, sessionToken, that.correlationID)
                                                        .then(function(result) {
                                                            resolve(result);
                                                        })
                                                        .catch(function(error) {
                                                            if(error && error.startsWith("404")) {
                                                                // create anonymous session as session token is not valid anymore
                                                                connectionHandlingImpl.sessionMissing(that.correlationID)
                                                                     .then(resolve)
                                                                     .catch(reject);
                                                            }
                                                            else {
                                                                reject(error);
                                                            }
                                                        });
                                                }
                                                else {
                                                    // create anonymous session
                                                    connectionHandlingImpl.sessionMissing(that.correlationID)
                                                         .then(resolve)
                                                         .catch(reject);
                                                }
                                             })
                                             .catch(reject);
                                     }
                                 }
                                 else {
                                     reject("Current session status '" + currentSessionStatus + "' is not known/supported!", that.correlationID);
                                 }
                              })
                              .catch(function(error) {
                                 reject("DataBus seems to be down!", that.correlationID);
                              });
                     };

                     var doConnect = function() {
                        if(!eventBus || !eventBus.state ||  eventBus.state == 2 ||  eventBus.state == 3) {
                          var eventBusOptions = {
                              vertxbus_reconnect_attempts_max: Infinity, // Max reconnect attempts
                              vertxbus_reconnect_delay_min: 1000, // Initial delay (in ms) before first reconnect attempt
                              vertxbus_reconnect_delay_max: 15000, // Max delay (in ms) between reconnect attempts
                              vertxbus_reconnect_exponent: 2, // Exponential backoff factor
                              vertxbus_randomization_factor: 0.5 // Randomization factor between 0 and 1
                          };

                          eventBus = new EventBus(window.dataBusSettings.location + "/eventbus/websocket", eventBusOptions);
                          eventBus.enableReconnect(true);
                          eventBus.onopen = getSessionConnection;
                          eventBus.onreconnect = getSessionConnection;

                          eventBus.onclose = function(e) {
                              DataBus.getStatus()
                                  .then(function(currentSessionStatus) {
                                      if("disconnected" !== currentSessionStatus && "DataBus.disconnect" !== e) {
                                        // this is an involuntary disconnect!
                                         DataBus.setLocal("session", "1", "status", "offline", "DataBus.onClose");
                                         DataBus.logger.warn("Session was disconnected involuntarily: " + currentSessionStatus + "-" + e);
                                      }
                                      else {
                                         DataBus.setLocal("session", "1", "status", "disconnected", "DataBus.onClose");
                                      }
                                    });
                          };
                       }
                       else if(eventBus.state == 1) {
                           getSessionConnection();
                       }
                       else {
                            DataBus.logger.warn("evenBus already connecting... not invoking getSessionConnection");
                       }
                     };

                     if(typeof isSaveSessionTokenAllowed !== 'undefined') {
                        // store the value
                        connectionHandlingImpl.setIsSaveSessionTokenAllowed(isSaveSessionTokenAllowed)
                            .then(function(result) {
                                    doConnect();
                                })
                            .catch(reject);
                     }
                     else {
                        doConnect();
                     }
                 });
             },
            disconnect: function() {
                return new Promise(function(resolve, reject) {
                    if(eventBus && eventBus.state == EventBus.OPEN) {
                        var headers = { correlationID: generateID() };

                        connectionHandlingImpl.getSessionToken()
                             .then(function(sessionToken) {
                                if(sessionToken) {
                                    headers.sessionToken = sessionToken;
                                }

                                eventBus.send("in.sessions.1.sessionEnded", {}, headers, function(error, reply) {
                                     if(error != null) {
                                        reject(error.failureCode + ": Closing session caused an error: " + error, correlationID);
                                        return;
                                     }
                                     if(reply.headers.statusCode != 200) {
                                        reject(reply.headers.statusCode + ": Closing session caused an error: " + reply.body.message, correlationID);
                                        return;
                                     }

                                     // delete local session token
                                     connectionHandlingImpl.setSessionToken("")
                                        .then(function() {
                                            DataBus.setLocal("session", "1", "status", "disconnected", "DataBus.disconnect")
                                                 .then(function(result) {
                                                     // close websocket
                                                     eventBus.close("DataBus.disconnect");
                                                     resolve();
                                                 })
                                                 .catch(function(error) {
                                                     reject(error);
                                                 });
                                        })
                                        .catch(reject);
                                 });
                            })
                            .catch(reject);
                    }
                    else {
                         // delete local session token
                         connectionHandlingImpl.setSessionToken("")
                            .then(function() {
                                DataBus.setLocal("session", "1", "status", "disconnected", "DataBus.disconnect")
                                     .then(function(result) {
                                         resolve();
                                     })
                                     .catch(function(error) {
                                         reject(error);
                                     });
                            })
                            .catch(reject);
                    }
                });
            }
        };

        var generateID = function() {
              return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (a, b) {
                return b = Math.random() * 16, (a == 'y' ? b & 3 | 8 : b | 0).toString(16);
           });
        };

        var getDeviceInfos = function() {
            var navConnection = navigator.connection || navigator.mozConnection || navigator.webkitConnection || {
                    type: "unknown",
                    effectiveType: "unknown",
                    downlink: "unknown"
                };
            var navOrientation = screen.orientation || screen.mozOrientation || screen.msOrientation || {
                   type: "unknown"
            };

            return {
                navigator: {
                    userAgent: navigator.userAgent,
                    doNotTrack: navigator.doNotTrack,
                    cookieEnabled: navigator.cookieEnabled,
                    language: navigator.language,
                    languages: navigator.languages,
                    timezoneOffset: new Date().getTimezoneOffset()
                },
                device: {
                    width: screen.width,
                    height: screen.height,
                    orientation: navOrientation.type,
                    devicePixelRatio: window.devicePixelRatio,
                    deviceResolutionWidth: screen.width * window.devicePixelRatio,
                    deviceResolutionHeight: screen.height * window.devicePixelRatio,
                    // touch is only a indicator - it does not work reliably
                    touch : !!(("ontouchstart" in window) || window.navigator && window.navigator.msPointerEnabled && window.MSGesture || window.DocumentTouch && document instanceof DocumentTouch),
                    oscpu: navigator.oscpu,
                    orientationChangeable : ("DeviceOrientationEvent" in window)
                },
                connection: {
                    type: navConnection.type,
                    effectiveType: navConnection.effectiveType,
                    downlink: navConnection.downlink
                }
            };
        }

        var DataBus = {
            // public attributes and methods
            logger: {
                    log: function(message, correlationID) { loggerImpl(4, message, correlationID); },
                    info: function(message, correlationID) { loggerImpl(4, message, correlationID); },
                    warn: function(message, correlationID) { loggerImpl(3, message, correlationID); },
                    error: function(message, correlationID) { loggerImpl(2, message, correlationID); },
                    fatal: function(message, correlationID) { loggerImpl(1, message, correlationID); }
                },
            subscribe: function(domain, version, pathSelector, isGetValueNow, onNext, onError) {
                if(typeof(isGetValueNow) === "function") {
                    onError = onNext;
                    onNext = isGetValueNow;
                    isGetValueNow = false;
                }
                if(!domain || !version || !pathSelector || pathSelector.length < 1) {
                    onError("Must provide domain, version, path selector!");
                }
                try {
                    var id = generateID();
                    var subscription = {
                        "id": id,
                        "domain": domain,
                        "version": version,
                        "onNext": onNext,
                        "onError": onError,
                        "unsubscribe": function() {
                            if(!id) {
                                return false;
                            }
                            return delete subscriptions[id];
                        }
                    };

                    if(typeof(pathSelector) === "string") {
                        if(pathSelector.endsWith(".*")) {
                            subscription.prefix = pathSelector.substring(0, pathSelector.length - 1);
                        }
                        else {
                            // TODO remove invalid characters (for absolute path)
                            subscription.absolute = pathSelector;
                        }

                        subscriptions[id] = subscription;
                        // retrieve current value
                        if(isGetValueNow) {
                            DataBus.get(domain, version, pathSelector, "subscription")
                                .then(function(result) {onNext(result, subscription);})
                                .catch(onError);
                            return subscription;
                        }
                    }
                    else if(pathSelector instanceof RegExp) {
                        if(isGetValueNow) {
                            // DOES NOT retrieve current value (that's a todo - walk current data and apply pattern matching efficiently)
                            onError("getValueNow is not supported for RegEx subscriptions!");
                            return;
                        }
                        subscription.pattern = pathSelector;
                        subscriptions[id] = subscription;
                        return subscription;
                    }
                }
                catch(error) {
                    onError(error);
                }
            },
            unsubscribe: function(subscriptionID) {
                if(!subscriptionID) {
                    return false;
                }
                return delete subscriptions[subscriptionID];
            },
            get: function(domain, version, jsonPath, origin) {
                return publishOrSend("get", domain, version, "", jsonPath, "", origin);
            },
            set: function(domain, version, absolutePath, value, origin) {
                return publishOrSend("set", domain, version, absolutePath, "", value, origin);
            },
            del: function(domain, version, absolutePath, origin) {
                return publishOrSend("del", domain, version, absolutePath, "", "", origin);
            },
            getRemote: function(domain, version, jsonPath, origin) {
                return publishOrSend("getRemote", domain, version, "", jsonPath, "", origin);
            },
            setRemote: function(domain, version, absolutePath, value, origin) {
                return publishOrSend("setRemote", domain, version, absolutePath, "", value, origin);
            },
            delRemote: function(domain, version, absolutePath, origin) {
                return publishOrSend("delRemote", domain, version, absolutePath, "", "", origin);
            },
            getLocal: function(domain, version, jsonPath, origin) {
                return publishOrSend("getLocal", domain, version, "", jsonPath, "", origin);
            },
            setLocal: function(domain, version, absolutePath, value, origin) {
                return publishOrSend("setLocal", domain, version, absolutePath, "", value, origin);
            },
            delLocal: function(domain, version, absolutePath, origin) {
                return publishOrSend("delLocal", domain, version, absolutePath, "", "", origin);
            },
            preflightLoginViaEmail: connectionHandlingImpl.authViaEMailForeseeable,
            loginViaEmail: connectionHandlingImpl.authViaEMail
            //connect: connectionHandlingImpl.connect,
            //disconnect: connectionHandlingImpl.disconnect,
            //getStatus: connectionHandlingImpl.getStatus
        };

        // check that all required settings are available
        // not provided via login function for easier (later) compatibility with OAuth procedures
        if(typeof(window.dataBusSettings) !== "object") {
           DataBus.logger.fatal("DataBus settings not set at global variable 'dataBusSettings'!");
           return null;
        }
        if(typeof(window.dataBusSettings.location) != "string") {
           DataBus.logger.fatal("UPPT location (URL) not set via global variable 'dataBusSettings.location'!");
           return null;
        }
        if(typeof(window.dataBusSettings.routerDomain) != "string") {
           DataBus.logger.fatal("UPPT routerDomain (EventBus Address for router/solution) not set via global variable 'dataBusSettings.routerDomain'!");
           return null;
        }
        if(typeof(window.dataBusSettings.routerVersion) != "string") {
           DataBus.logger.fatal("UPPT routerVersion (EventBus Address for router/solution) not set via global variable 'dataBusSettings.routerVersion'!");
           return null;
        }
        if(typeof(window.dataBusSettings.routerType) != "string") {
           DataBus.logger.fatal("UPPT routerType (EventBus Address for router/solution) not set via global variable 'dataBusSettings.routerType'!");
           return null;
        }

        // save all settings/configuration in local data
        for(var property in window.dataBusSettings) {
            localData.config["1"][property] = window.dataBusSettings[property];
        }

        // listen to session status changes andreact accordingly
        DataBus.subscribe("session", "1", "status", function(update) {

            DataBus.logger.info("Session status UPDATE: " + update.values[0]);

            if(update.values[0] === "online") {
                // websocket connection established, time to setup the session
                connectionHandlingImpl.sessionMissing()
                    .then(function() {})
                    .catch(function(error) {DataBus.logger.fatal(error);});
            }
            else if(update.values[0] === "session") {
                // anonymous session on server established, time to check if automatic authentication is possible
                var storedSessionToken = connectionHandlingImpl.getStoredSessionToken();

                if(storedSessionToken) {
                    // try a session handover
                    // TODO
                    DataBus.logger.info("Session status TOKEN: " + sessionToken);
                }
                else {
                    // keep the anonymous session and mark the session as established
                    DataBus.setLocal("session", "1", "status", "anonymous", "SessionStatusMonitor");
                }
            }
            else if(update.values[0] === "authenticating") {
                // waiting for authentication result...
            }
            else if(update.values[0] === "anonymous") {
                // anonymous session on server established
            }
            else if(update.values[0] === "authenticated") {
                // authenticated session on server established (logged in)
            }
            else if(update.values[0] === "disconnecting") {
                // process disconnect/log out requests by user
                DataBus.disconnect();
            }
            else if(update.values[0] === "closed") {
                // process closed websocket, might be involuntary disconnect
            }
            // TODO: offline/onclose
        });

        // create a (anonymous) session and keep it alive
        var eventBusOptions = {
              vertxbus_reconnect_attempts_max: Infinity, // Max reconnect attempts
              vertxbus_reconnect_delay_min: 1000, // Initial delay (in ms) before first reconnect attempt
              vertxbus_reconnect_delay_max: 15000, // Max delay (in ms) between reconnect attempts
              vertxbus_reconnect_exponent: 2, // Exponential backoff factor
              vertxbus_randomization_factor: 0.5 // Randomization factor between 0 and 1
          };

          eventBus = new EventBus(window.dataBusSettings.location + "/eventbus/websocket", eventBusOptions);
          eventBus.enableReconnect(true);
          eventBus.onopen = function() {
                DataBus.setLocal("session", "1", "status", "online", "eventBus.onOpen");
            };
          eventBus.onreconnect = function() {
                 DataBus.setLocal("session", "1", "status", "online", "eventBus.onReconnect");
             };
          eventBus.onclose = function(e) {
              DataBus.getStatus()
                  .then(function(currentSessionStatus) {
                      if("disconnected" !== currentSessionStatus && "DataBus.disconnect" !== e) {
                        // this is an involuntary disconnect!
                         DataBus.setLocal("session", "1", "status", "offline", "DataBus.onClose");
                         DataBus.logger.warn("Session was disconnected involuntarily: " + currentSessionStatus + "-" + e);
                      }
                      else {
                         DataBus.setLocal("session", "1", "status", "disconnected", "DataBus.onClose");
                      }
                    })
                    .catch(error);
          };


        return DataBus;
    };

    // define globally, if not yet available
    if(typeof(DataBus) === 'undefined') {
       window.DataBus = initDataBus();
    }
    else {
        DataBus.logger.warn("DataBus library already defined.");
    }
})(window);

/* Adopted from JSONPath 0.8.5 - XPath for JSON
 *
 * Copyright (c) 2007 Stefan Goessner (goessner.net)
 * modified by Malte Behrendt (dev@maltebehrendt.de)
 * Licensed under the MIT (MIT-LICENSE.txt) licence.
 *
 * Proposal of Chris Zyp goes into version 0.9.x
 * Issue 7 resolved
 */

function JSONPath(obj, expr, localDataOriginMap) {
   var P = {
      result: { "paths": [], "values": [], "origins": []},
      normalize: function(expr) {
         var subx = [];
         return expr.replace(/[\['](\??\(.*?\))[\]']|\['(.*?)'\]/g, function($0,$1,$2){return "[#"+(subx.push($1||$2)-1)+"]";})  /* http://code.google.com/p/jsonpath/issues/detail?id=4 */
                    .replace(/'?\.'?|\['?/g, ";")
                    .replace(/;;;|;;/g, ";..;")
                    .replace(/;$|'?\]|'$/g, "")
                    .replace(/#([0-9]+)/g, function($0,$1){return subx[$1];});
      },
      asPath: function(path) {
         var x = path.split(";"), p = "";
         for (var i=1,n=x.length; i<n; i++)
            p += /^[0-9*]+$/.test(x[i]) ? ("["+x[i]+"]") : ("."+x[i]+"");
         return p.substring(1);
      },
      store: function(p, v, o) {
        if(p) {
            var index = P.result.paths.length;
            P.result.paths[index] = P.asPath(p);
            P.result.values[index] = v;
            if(!o) {
                P.result.origins[index] = "unknown";
            }
            else {
                P.result.origins[index] = o;
            }
        }
        return !!p;
      },
      trace: function(expr, val, path, origin) {
         if (expr !== "") {
            var x = expr.split(";"), loc = x.shift();
            x = x.join(";");
            if (val && val.hasOwnProperty(loc))
               P.trace(x, val[loc], path + ";" + loc, localDataOriginMap[P.asPath(path)]? localDataOriginMap[P.asPath(path)]:origin);
            else if (loc === "*")
               P.walk(loc, x, val, path, function(m,l,x,v,p,o) { P.trace(m+";"+x,v,p,o); }, localDataOriginMap[P.asPath(path)]? localDataOriginMap[P.asPath(path)]:origin);
            else if (loc === "..") {
               P.trace(x, val, path, localDataOriginMap[P.asPath(path)]? localDataOriginMap[P.asPath(path)]:origin);
               P.walk(loc, x, val, path, function(m,l,x,v,p,o) { typeof v[m] === "object" && P.trace("..;"+x,v[m],p+";"+m, o); }, localDataOriginMap[P.asPath(path)]? localDataOriginMap[P.asPath(path)]:origin);
            }
            else if (/^\(.*?\)$/.test(loc)) // [(expr)]
               P.trace(P.eval(loc, val, path.substr(path.lastIndexOf(";")+1))+";"+x, val, path, localDataOriginMap[P.asPath(path)]? localDataOriginMap[P.asPath(path)]:origin);
            else if (/^\?\(.*?\)$/.test(loc)) // [?(expr)]
               P.walk(loc, x, val, path, function(m,l,x,v,p,o) { if (P.eval(l.replace(/^\?\((.*?)\)$/,"$1"), v instanceof Array ? v[m] : v, m)) P.trace(m+";"+x,v,p,o); }, localDataOriginMap[P.asPath(path)]? localDataOriginMap[P.asPath(path)]:origin); // issue 5 resolved
            else if (/^(-?[0-9]*):(-?[0-9]*):?([0-9]*)$/.test(loc)) // [start:end:step] python slice syntax
               P.slice(loc, x, val, path);
            else if (/,/.test(loc)) { // [name1,name2,...]
               for (var s=loc.split(/'?,'?/),i=0,n=s.length; i<n; i++)
                  P.trace(s[i]+";"+x, val, path, localDataOriginMap[P.asPath(path)]? localDataOriginMap[P.asPath(path)]:origin);
            }
         }
         else
            P.store(path, val, localDataOriginMap[P.asPath(path)]? localDataOriginMap[P.asPath(path)]:origin);
      },
      walk: function(loc, expr, val, path, f, origin) {
         if (val instanceof Array) {
            for (var i=0,n=val.length; i<n; i++)
               if (i in val)
                  f(i,loc,expr,val,path, origin);
         }
         else if (typeof val === "object") {
            for (var m in val)
               if (val.hasOwnProperty(m))
                  f(m,loc,expr,val,path, origin);
         }
      },
      slice: function(loc, expr, val, path, origin) {
         if (val instanceof Array) {
            var len=val.length, start=0, end=len, step=1;
            loc.replace(/^(-?[0-9]*):(-?[0-9]*):?(-?[0-9]*)$/g, function($0,$1,$2,$3){start=parseInt($1||start);end=parseInt($2||end);step=parseInt($3||step);});
            start = (start < 0) ? Math.max(0,start+len) : Math.min(len,start);
            end   = (end < 0)   ? Math.max(0,end+len)   : Math.min(len,end);
            for (var i=start; i<end; i+=step)
               P.trace(i+";"+expr, val, path, localDataOriginMap[P.asPath(path)]? localDataOriginMap[P.asPath(path)]:origin);
         }
      },
      eval: function(x, _v, _vname) {
         try { return $ && _v && eval(x.replace(/(^|[^\\])@/g, "$1_v").replace(/\\@/g, "@")); }  // issue 7 : resolved ..
         catch(e) { throw new SyntaxError("jsonPath: " + e.message + ": " + x.replace(/(^|[^\\])@/g, "$1_v").replace(/\\@/g, "@")); }  // issue 7 : resolved ..
      }
   };

   if(!localDataOriginMap) {
    localDataOriginMap = {};
   }

   var $ = obj;
   if (expr && obj) {
      P.trace(P.normalize(expr).replace(/^\$;?/,""), obj, "$", "unknown");  // issue 6 resolved
      return P.result;
   }
   else {
      return null;
   }
};
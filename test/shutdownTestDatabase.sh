if [ -f "test/arangoDB.pid" ]; then
    PID=`cat test/arangoDB.pid`
    kill $PID
    while ps -p $PID; do sleep 1;done;
    rm -f "test/arangoDB.pid"
fi